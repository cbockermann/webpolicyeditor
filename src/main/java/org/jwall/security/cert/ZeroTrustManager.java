package org.jwall.security.cert;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.logging.Logger;

import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class ZeroTrustManager 
	implements X509TrustManager, TrustManager 
{
	private static Logger log = Logger.getLogger( "ZeroTrustManager" );
	
	public void checkClientTrusted(X509Certificate[] chain, String authType)
			throws CertificateException {

		log.fine( "checkClientTrusted: \n");
		
		for( X509Certificate cert : chain ){
			log.fine("-------------------------------------------------------");
			log.fine( " SubjectDN = "+cert.getSubjectDN() );
			log.fine( " Issuer = " + cert.getIssuerDN() );
		}		
	}

	public void checkServerTrusted(X509Certificate[] chain, String authType)
			throws CertificateException {
		
		log.fine( "checkServerTrusted: \n");
		
		for( X509Certificate cert : chain ){
			log.fine("-------------------------------------------------------");
			log.fine( " SubjectDN = "+cert.getSubjectDN() );
			log.fine( " Issuer = " + cert.getIssuerDN() );
		}		
	}

	public X509Certificate[] getAcceptedIssuers() {
		return null;
	}
}
