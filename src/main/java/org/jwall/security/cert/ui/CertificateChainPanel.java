package org.jwall.security.cert.ui;

import java.awt.BorderLayout;
import java.security.cert.X509Certificate;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TreeSelectionListener;

public class CertificateChainPanel
    extends JPanel
{
    /** The unique class id */
    private static final long serialVersionUID = -1527670860194878151L;
    
    /** The chain to be visualized */
    X509Certificate[] chain;
    
    /** A simple tree-model of the chain */
    CertificateChainTreeModel treeModel;
    
    /** */
    JTree jtree;
    
    
    public CertificateChainPanel( X509Certificate[] chain ){
        this.chain = chain;
        
        treeModel = new CertificateChainTreeModel( chain );
        
        setLayout( new BorderLayout() );
        
        jtree = new JTree( treeModel );
        jtree.setBorder( new EmptyBorder( 5, 5, 5, 5 ) );
        jtree.setRowHeight( 30 );
        jtree.setCellRenderer( new CertificateCellRenderer() );
        JScrollPane p = new JScrollPane( jtree );
        jtree.setSelectionRow( jtree.getRowCount() );
        
        add( p, BorderLayout.CENTER );
    }
    
    public void addTreeSelectionListener( TreeSelectionListener l ){
        jtree.addTreeSelectionListener( l );
    }
    
    public void removeTreeSelectionListener( TreeSelectionListener l ){
        jtree.removeTreeSelectionListener( l );
    }
}