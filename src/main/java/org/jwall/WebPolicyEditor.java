package org.jwall;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.net.URL;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import org.jwall.app.Action;
import org.jwall.app.Application;
import org.jwall.app.ui.DisplayDialog;
import org.jwall.app.ui.SplashScreen;
import org.jwall.web.filter.ids.FilterSet;
import org.jwall.web.policy.WebPolicy;
import org.jwall.web.policy.compiler.PolicyCompilerImpl;
import org.jwall.web.policy.editor.AbstractPolicyModel;
import org.jwall.web.policy.editor.DocumentListener;
import org.jwall.web.policy.editor.PolicyEditorView;
import org.jwall.web.policy.editor.tree.PolicyTreeModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * This is a simple graphical editor for writing web-application policys using
 * the web policy language.
 * </p>
 * 
 * <ol>
 * <li>Version 0.2</li>
 * </ol>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class WebPolicyEditor extends Application implements ActionListener,
		DocumentListener {
	/** the version of the ProfileEditor */
	public final static String VERSION = "0.2";

	/** The SVN revision */
	public final static String REVISION = "$Revision: 11 $";

	/** The display-name of this application */
	public final static String NAME = "WebPolicyEditor";

	// export the version information
	static {
		System.setProperty("org.jwall.WebPolicyEditor.version", VERSION);
		System.setProperty("org.jwall.WebPolicyEditor.revision", REVISION);
	}

	/** A global logger for this class */
	public final static Logger log = LoggerFactory
			.getLogger(WebPolicyEditor.class);

	public final static boolean DEBUG = System
			.getProperty("org.jwall.WebPolicyEditor.DEBUG") != null;
	public final static boolean DEVEL = true; // ||
												// System.getProperty("org.jwall.WebPolicyEditor.DEVEL")
												// != null;

	public final static boolean BLOGIC = false;
	public final static String SLASH = System.getProperty("file.separator");
	public final static String PREF_DIR = System.getProperty("user.home")
			+ SLASH + ".jwall";
	public final static String PPREFIX = "org.jwall.web.policy.editor";

	/* The file currently associated with the loaded policy */
	File file;

	/* The policy-model currently being viewed/edited */
	AbstractPolicyModel rmodel;

	/* The main view which provides panels for all aspects of the policy-model */
	PolicyEditorView policyEditorView;

	/**
	 * Creates a new policy-editor instance.
	 * 
	 */
	public WebPolicyEditor() {
		super(NAME);

		URL url = FilterSet.class.getResource("/php-ids_default_filter.xml");
		try {
			if (url == null) {
				url = new URL("http://www.jwall.org/default_filter.xml");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		log.info("Using filters from " + url);
		System.setProperty(FilterSet.PROPERTY_DEFAULT_FILTER_URL, "" + url);
		System.setProperty(PolicyCompilerImpl.PROPERTY_COMPILER_XSLT_FILE,
				"/templates/WeakPositiveRules.xslt");

		SplashScreen splash = new SplashScreen();
		splash.setMessage(NAME + " " + VERSION);
		splash.setSubtitle("Copyright by Christian Bockermann <chris@jwall.org>");
		splash.setVisible(true);

		splash.setProgress(50);

		setupUI();

		splash.setProgress(100);
		try {
			Thread.sleep(100);
		} catch (Exception e) {
		}
		splash.setVisible(false);
	}

	private void setupUI() {

		policyEditorView = new PolicyEditorView(this);

		for (Action act : policyEditorView.getActions())
			registerAction(act);

		registerAction(new Action(this, Action.QUIT, "File/Quit", 100.0d,
				false, true, KeyStroke.getKeyStroke(KeyEvent.VK_Q, Toolkit
						.getDefaultToolkit().getMenuShortcutKeyMask())));// ActionEvent.CTRL_MASK
																			// )
																			// )
																			// );
		registerAction(new Action(this, Action.HELP, "Help/Help", 1.0d, false,
				true, KeyStroke.getKeyStroke("F1")));
		registerAction(new Action(this, Action.ABOUT, "Help/About", 2.0d,
				false, true));

		win.setVisible(true);
		win.setView(policyEditorView);
		win.getContentPane().validate();

		win.getStatusBar().setMessage("Press F1 for Help");

		try {

			loadProperties();

		} catch (Exception e) {
			JOptionPane.showMessageDialog(getWindow(),
					"<html>An error occured during startup: <p style=\"padding: 10px;\">"
							+ e.getMessage() + "<p>Using defaults.</html>");

			properties.put(PPREFIX + ".useExternalXSLT", "false");
		}
	}

	/**
	 * This creates a new instance of the editor which loads a policy from the
	 * specified file.
	 */
	public WebPolicyEditor(File f) {
		this();
		file = f;

		if (file != null && file.canRead())
			policyEditorView.loadPolicy(file);
	}

	/**
	 * <p>
	 * This method sets the policy that is currently being viewed/edited within
	 * the editor.
	 * </p>
	 * 
	 * @param policy
	 */
	public void setPolicy(WebPolicy policy) {
		rmodel = new PolicyTreeModel(policy);
		rmodel.addDocumentListener(this);
		policyEditorView.setProfile(policy);
		policyEditorView.validate();
		getWindow().getStatusBar().setMessage("Test");
		getAction(Action.SAVE_FILE).setEnabled(policy != null && file != null);
		getAction(Action.SAVE_AS_FILE).setEnabled(policy != null);
	}

	/**
	 * <p>
	 * The WebPolicyEditor acts as an action-listener for some global actions.
	 * All other action events are usually handled by the respective views, e.g.
	 * the PolicyEditorView.
	 * </p>
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {

		if (Action.HELP.equals(e.getActionCommand())) {
			URL url = WebPolicyEditor.class
					.getResource("/html/policy/help.html");
			WebPolicyEditor.showDialog(800, 500, url);
		}

		if (Action.ABOUT.equals(e.getActionCommand())) {

			try {

				URL url = WebPolicyEditor.class.getResource("/html/about.html");
				if (url != null) {

					DisplayDialog d = new DisplayDialog(url, false, 400, 300);
					d.setTitle("WebPolicyEditor Version " + VERSION + " ("
							+ REVISION + ")");
					d.setVisible(true);
					d.setResizable(false);

				} else
					WebPolicyEditor.showDialog(new URL(
							"http://www.jwall.org/web/policy/lang.jsp"));

			} catch (Exception ex) {
				URL url = WebPolicyEditor.class
						.getResource("/html/policy/lang.html");
				WebPolicyEditor.showDialog(800, 500, url);
			}
		}

		if (Action.QUIT.equals(e.getActionCommand())) {
			try {
				saveProperties();
			} catch (Exception ex) {
				log.error("Failed to save preferences: {}", ex.getMessage());
				JOptionPane
						.showMessageDialog(
								getWindow(),
								"<html>An error occured while saving your preferences: <p style=\"padding: 10px;\">"
										+ ex.getMessage()
										+ "<p>Your preferences have not been saved.</html>");
			}
			System.exit(0);
		}
	}

	/**
	 * @see org.jwall.web.policy.editor.DocumentListener#documentChanged(java.io.File)
	 */
	public void documentChanged(File doc) {
		if (file != null)
			win.setTitle(NAME + " :: " + file.getName() + "*");

		getAction(Action.SAVE_FILE).setEnabled(true);
	}

	/**
	 * @see org.jwall.web.policy.editor.DocumentListener#documentSaved(java.io.File)
	 */
	public void documentSaved(File doc) {
		win.setTitle(NAME + " :: " + file.getName());
		getAction(Action.SAVE_FILE).setEnabled(false);
	}

	public static void showDialog(URL url) {
		log.debug("Displaying url: {}", url);

		if (url == null)
			return;

		DisplayDialog.openURL(url.toString());
	}

	public static void showDialog(int width, int height, URL url) {
		if (url == null)
			return;

		DisplayDialog d = new DisplayDialog(url, true, width, height);
		d.setVisible(true);
		d.setResizable(false);
	}

	/**
	 * @see org.jwall.app.Application#getVersion()
	 */
	public String getVersion() {
		return VERSION;
	}

	/**
	 * @see org.jwall.app.Application#getUpdateURL()
	 */
	public String getUpdateURL() {
		return "http://www.jwall.org/releases?name=WebPolicyEditor";
	}

	/**
	 * @see org.jwall.app.Application#getPreferenceFileName()
	 */
	public String getPreferenceFileName() {
		return "editor.xml";
	}

	/**
	 * @see org.jwall.app.Application#getPreferenceDirectory()
	 */
	public String getPreferenceDirectory() {
		return System.getProperty("user.home") + SLASH + ".jwall";
	}

	/**
	 * @see org.jwall.app.Application#getResourceFiles()
	 */
	@Override
	public List<String> getResourceFiles() {
		List<String> l = super.getResourceFiles();
		l.add("/resources.xml");
		return l;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		for (String arg : args) {
			if (arg.equals("-v") || arg.equals("--version")) {
				log.info(VERSION);
				System.exit(0);
			}
		}

		File f = null;
		if (args.length > 0)
			f = new File(args[0]);

		new WebPolicyEditor(f);
	}
}