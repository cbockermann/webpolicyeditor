/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.app.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.jwall.app.Application;
import org.jwall.app.ui.CompletionTextField;

/**
 * <p>
 * This class implements a simple tree-cell editor which provides a name and a
 * value-field. The value-field is a smart text field, supporting completions. <br/>
 * It will return a <code>TableColumn</code> instance if the editing has been
 * finished by the user.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class TableColumnListCellEditor extends
		javax.swing.tree.DefaultTreeCellEditor {
	/** */
	protected static final long serialVersionUID = -5510958702224299085L;

	/* The text field for the name column */
	JTextField nameField = new JTextField(8);

	/* The list of completions offered to the user */
	List<String> completions = new LinkedList<String>();

	/* The text component for the variable column offering completions */
	CompletionTextField varField;

	/* A flag indicating whether input has been cancelled */
	boolean cancelled = false;

	KeyAdapter enterListener;

	/**
	 * Create a new instance for rendering elements of the given tree.
	 * 
	 * @param tree
	 *            The tree to provide editing for.
	 * @param renderer
	 *            The cell-renderer of the tree.
	 */
	public TableColumnListCellEditor(JTree tree,
			DefaultTreeCellRenderer renderer) {
		super(tree, renderer);

		//
		// create the adapter listening for the "ENTER" event
		//
		enterListener = new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {

					if (e.getSource() != varField
							|| varField.isSelectionFinished()) {
						cancelled = false;
						stopCellEditing();
					}
				}
			}
		};
	}

	/**
	 * This method allows setting the completions for the variable field.
	 * 
	 * @param completions
	 *            The set of possible completions.
	 */
	public void setVariableCompletions(List<String> completions) {
		this.completions = completions;
	}

	/**
	 * @see javax.swing.tree.DefaultTreeCellEditor#getCellEditorValue()
	 */
	@Override
	public Object getCellEditorValue() {
		if (cancelled)
			return null;

		return new TableColumn(varField.getText(), nameField.getText());
	}

	/**
	 * This method will create a JPanel instance with a border containing all
	 * neccessary input elements and two buttons for finishing and cancelling
	 * the user input.
	 * 
	 * @see javax.swing.tree.DefaultTreeCellEditor#getTreeCellEditorComponent(javax.swing.JTree,
	 *      java.lang.Object, boolean, boolean, boolean, int)
	 */
	@Override
	public Component getTreeCellEditorComponent(JTree tree, Object value,
			boolean isSelected, boolean expanded, boolean leaf, int row) {
		cancelled = false;

		JPanel c = new JPanel(new BorderLayout());

		JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		p.setAlignmentX(Component.LEFT_ALIGNMENT);
		p.setAlignmentY(Component.CENTER_ALIGNMENT);
		p.setBorder(new EmptyBorder(0, 0, 15, 0));
		p.setBackground(Color.LIGHT_GRAY);
		c.setBackground(Color.LIGHT_GRAY);
		c.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));

		JLabel icon = new JLabel(
				Application
						.getIcon("org.jwall.web.audit.viewer.table.TableColumn"));
		icon.setBorder(null);
		icon.setAlignmentX(Component.LEFT_ALIGNMENT);
		icon.setAlignmentY(Component.CENTER_ALIGNMENT);
		c.add(icon, BorderLayout.WEST);

		TableColumn column = (TableColumn) value;

		JLabel ln = new JLabel("Name: ");
		ln.setAlignmentY(Component.CENTER_ALIGNMENT);
		ln.setAlignmentX(Component.LEFT_ALIGNMENT);
		p.add(ln);

		nameField = new JTextField(8);
		nameField.setAlignmentY(Component.CENTER_ALIGNMENT);
		nameField.setAlignmentX(Component.LEFT_ALIGNMENT);
		nameField.setText(column.getColumnName());
		nameField.addKeyListener(enterListener);
		p.add(nameField);

		JLabel lv = new JLabel("Variable:");
		lv.setAlignmentY(Component.CENTER_ALIGNMENT);
		lv.setAlignmentX(Component.LEFT_ALIGNMENT);
		p.add(lv);

		varField = new CompletionTextField(completions);
		varField.setAlignmentY(Component.CENTER_ALIGNMENT);
		varField.setAlignmentX(Component.LEFT_ALIGNMENT);
		varField.setText(column.getVariable());
		varField.addKeyListener(enterListener);
		p.add(varField);

		JPanel b = new JPanel();
		b.setAlignmentY(Component.CENTER_ALIGNMENT);
		b.setAlignmentX(Component.LEFT_ALIGNMENT);
		b.setBorder(null);
		b.setOpaque(true);
		b.setBackground(Color.LIGHT_GRAY);

		JButton cancel = new JButton();
		cancel.setBorder(null);
		cancel.setBorderPainted(false);
		cancel.setBackground(Color.WHITE);
		cancel.setOpaque(true);
		cancel.setSize(new Dimension(8, 8));
		cancel.setIcon(Application
				.getIcon("org.jwall.web.audit.viewer.cancel-editing"));
		cancel.setContentAreaFilled(false);
		cancel.setAlignmentY(Component.CENTER_ALIGNMENT);
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelled = true;
				cancelCellEditing();
			}
		});

		JButton stop = new JButton();
		stop.setBorder(new EmptyBorder(4, 4, 4, 4));
		stop.setBorderPainted(false);
		stop.setOpaque(true);
		stop.setSize(new Dimension(10, 10));
		stop.setBackground(Color.WHITE);
		stop.setContentAreaFilled(false);
		stop.setIcon(Application
				.getIcon("org.jwall.web.audit.viewer.stop-editing"));
		stop.setAlignmentY(Component.CENTER_ALIGNMENT);
		stop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				stopCellEditing();
			}
		});

		b.add(stop);
		b.add(cancel);
		p.add(b);
		p.setPreferredSize(new Dimension(500, tree.getRowHeight()));

		c.add(p, BorderLayout.CENTER);
		return c;
	}

	public void close() {
		if (varField != null)
			varField.finish();
	}
}
