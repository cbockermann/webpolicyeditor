package org.jwall.app.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jwall.app.Application;


/**
 * 
 * @author Christian Bockermann <chris@jwall.org>
 * 
 */
public class ApplicationWindow
extends JFrame
implements WindowListener
{
	public final static long serialVersionUID = 123L;
	ToolBar toolbar;
	MenuBar menuBar;
	StatusBar statusBar;
	JPanel viewPanel = new JPanel();
	View currentView = null;
	
	public ApplicationWindow( Application app ){
		setTitle("..:: jwall.org :: " + app.getName() + " ::..");

		currentView = new View( app );

		menuBar = new MenuBar();
		
		statusBar = new StatusBar();
		
		Toolkit tk = Toolkit.getDefaultToolkit();
		this.setSize(new Dimension(1024,768));
		Dimension d = tk.getScreenSize();

		int x = (d.width - this.getWidth()) / 2;
		int y = (d.height - this.getHeight()) /2;

		this.setLocation(x, y);

		toolbar = new ToolBar();
		toolbar.setFloatable( false );
		getContentPane().add( toolbar, BorderLayout.NORTH );

		viewPanel.removeAll();
		viewPanel.setLayout( new BorderLayout() );
		viewPanel.add( currentView, BorderLayout.CENTER );

		getContentPane().add( viewPanel, BorderLayout.CENTER );

		getContentPane().add( statusBar, BorderLayout.SOUTH );
		
		addWindowListener(this);
		
		this.setJMenuBar( menuBar );
	}



	/* (non-Javadoc)
	 * @see java.awt.event.WindowListener#windowActivated(java.awt.event.WindowEvent)
	 */
	public void windowActivated(WindowEvent arg0)
	{
	}

	/* (non-Javadoc)
	 * @see java.awt.event.WindowListener#windowClosed(java.awt.event.WindowEvent)
	 */
	public void windowClosed(WindowEvent arg0)
	{
	}

	/* (non-Javadoc)
	 * @see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)
	 */
	public void windowClosing(WindowEvent arg0)
	{
		System.exit(0);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.WindowListener#windowDeactivated(java.awt.event.WindowEvent)
	 */
	public void windowDeactivated(WindowEvent arg0)
	{
	}

	/* (non-Javadoc)
	 * @see java.awt.event.WindowListener#windowDeiconified(java.awt.event.WindowEvent)
	 */
	public void windowDeiconified(WindowEvent arg0)
	{
	}

	/* (non-Javadoc)
	 * @see java.awt.event.WindowListener#windowIconified(java.awt.event.WindowEvent)
	 */
	public void windowIconified(WindowEvent arg0)
	{
	}

	/* (non-Javadoc)
	 * @see java.awt.event.WindowListener#windowOpened(java.awt.event.WindowEvent)
	 */
	public void windowOpened(WindowEvent arg0)
	{
	}

	public View getCurrentView(){
		return null;
	}
	
	public void setView( View v ){
		
		if( currentView == v )
			return;
	
		viewPanel.removeAll();
		currentView = v;
		viewPanel.add( currentView, BorderLayout.CENTER );
		viewPanel.revalidate();
		
		viewPanel.repaint();
		getContentPane().validate();
	}
	
	public ToolBar getToolBar(){
		return toolbar;
	}
	
	public MenuBar getMenu(){
		return this.menuBar;
	}
	
	public StatusBar getStatusBar(){
	    return statusBar;
	}
}