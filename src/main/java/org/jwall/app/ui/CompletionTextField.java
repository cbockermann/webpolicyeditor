package org.jwall.app.ui;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JTextField;


/**
 * 
 * This class implements a <i>smart</i> text field that is capable of offering 
 * a list of string-completions to the user.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class CompletionTextField
    extends JTextField
    implements FocusListener, KeyListener
{
    /** The unique class id */
    private static final long serialVersionUID = -6579604068593968248L;

    /** The list of completions */
    List<String> completions = new LinkedList<String>();

    /** The dialog showing the completions */
    SelectionDialog selectionDialog;

    /** A flag indicating whether the user finished his input, i.e. pressed RETURN. */
    boolean finishedSelection = false;

    
    /**
     * Create a new smart text-field instance with the given list of possible
     * string-completions.
     * 
     * @param selections The possible completions.
     */
    public CompletionTextField( List<String> selections ){
        this( 10, selections );
    }

    
    /**
     * Create a new text-field with the given size and the given list of completions.
     * 
     * @param size The size of the text-field.
     * @param selections The set of completions.
     */
    public CompletionTextField( int size, List<String> selections ){
        super( size );
        
        completions = selections;
        selectionDialog = new SelectionDialog( completions );
        selectionDialog.setFocusable( false );
        addFocusListener( this );
        addKeyListener( this );
        
        setFocusCycleRoot( true );
    }


    /**
     * Finishes the user input by closing the selection dialog.
     * 
     */
    public void finish(){
        finishedSelection = true;
        selectionDialog.finish();
    }


    /**
     * Returns whether the user has finished his selection.
     * 
     * @return <code>true</code>, if the user has finished his selection, i.e. hit RETURN.
     */
    public boolean isSelectionFinished(){
        return finishedSelection;
    }


    /**
     * @see java.awt.event.FocusListener#focusGained(java.awt.event.FocusEvent)
     */
    public void focusGained(FocusEvent e)
    {
        if( !selectionDialog.isFinished() ){
            if( ! selectionDialog.isShowing() && !selectionDialog.isVisible() ){
                //selectionDialog.setFocusable( false );
                selectionDialog.show( getLocationOnScreen().x, getLocationOnScreen().y + 22 );
            }
        } else {
            finishedSelection = false;
            selectionDialog.setFinished( false );
        }

    }

    
    /**
     * @see java.awt.event.FocusListener#focusLost(java.awt.event.FocusEvent)
     */
    public void focusLost(FocusEvent e)
    {
        if( selectionDialog.isShowing() ){
            selectionDialog.setVisible( false );
        }
    }


    /**
     * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
     */
    public void keyPressed(KeyEvent e)
    {
        if( e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_ESCAPE ){
            if( !selectionDialog.isFinished() ){
                selectionDialog.finish();
                setCaretPosition( getText().length() );
            } else {
                // 
                // this is the final ENTER key signaling that user input is finished 
                //
                finishedSelection = true;
            }
        }

        if( e.getKeyCode() == KeyEvent.VK_DOWN ){
            selectionDialog.selectNext();
            setText( "" + selectionDialog.getSelectedValue() );
            setCaretPosition( getText().length() );
            finishedSelection = false;
        }

        if( e.getKeyCode() == KeyEvent.VK_UP ){
            selectionDialog.selectPrevious();
            setText( "" + selectionDialog.getSelectedValue() );
            setCaretPosition( getText().length() );
            finishedSelection = false;
        }
        
        if( !finishedSelection && getText().length() == 0 ){
            if( ! selectionDialog.isShowing() )
                selectionDialog.show( getLocationOnScreen().x, getLocationOnScreen().y + 22 );
        }
    }


    /**
     * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
     */
    public void keyReleased(KeyEvent e)
    {
        if( finishedSelection )
            return;

        if( e.getKeyCode() != KeyEvent.VK_DOWN && e.getKeyCode() != KeyEvent.VK_UP && !selectionDialog.isFinished() ){

            if( !selectionDialog.isShowing() && getText() != null && getText().length() > 0 ){
                selectionDialog.show( getLocationOnScreen().x, getLocationOnScreen().y + 22 );
            }

            if( selectionDialog.isShowing() ){
                selectionDialog.selectByPrefix( getText() );
            }
        }
    }

    
    /**
     * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
     */
    public void keyTyped(KeyEvent e)
    {
    }
}