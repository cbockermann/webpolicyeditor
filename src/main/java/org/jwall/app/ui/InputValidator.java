package org.jwall.app.ui;

public interface InputValidator
{
    public boolean validateInput() throws UserError;
}
