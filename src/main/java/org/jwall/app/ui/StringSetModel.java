package org.jwall.app.ui;

import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.DefaultComboBoxModel;


/**
 * This class implements a simple container for holding strings. It is used
 * to allow for the use of interactive (editable) Combo boxes.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 */
public class StringSetModel
    extends DefaultComboBoxModel
{

    private static final long serialVersionUID = -5002961331574719687L;
    
    /** The index of the currently selected item */
    int selected = -1;
    
    /** The list of items to be displayed */
    LinkedList<String> items = new LinkedList<String>();

    
    /**
     * This creates a new instance of this class by loading a list of values
     * from the system property given by <code>propertyName</code>. The values
     * are expected to be separated by colons. 
     * 
     * @param propertyName The name of the property from which to extract the values.
     */
    public StringSetModel( String propertyName ){
        String list = System.getProperty( propertyName );
        if( list != null ){
            String[] itemlist = list.split(":");
            for( String str : itemlist )
                items.add( str );
        }
    }

    
    /**
     * This constructor creates a new instance of this model which is filled by
     * the given String items.
     * 
     * @param itemList The list of initial items.
     */
    public StringSetModel( String[] itemList ){
        for( String s : itemList )
            items.add( s );
    }

    
    /**
     * This method adds a new item to the list and notifies the parent combobox
     * object. The item is then selected.
     * If the item is already contained within the list, it only gets selected.
     * 
     * @param item The item to be added.
     */
    public void add( String item ){
        if(! items.contains( item ) ){
            items.add( item );
        }
        selected = items.indexOf( item );
        this.fireIntervalAdded( this, selected, selected);
    }

    
    /**
     * @see javax.swing.DefaultComboBoxModel#getSelectedItem()
     */
    public Object getSelectedItem()
    {
        if( selected < 0 || selected > items.size() )
            return null;

        return items.get( selected );
    }

    
    /**
     * @see javax.swing.DefaultComboBoxModel#setSelectedItem(java.lang.Object)
     */
    public void setSelectedItem(Object anItem)
    {
        selected = items.indexOf( anItem );
    }

    
    /**
     * @see javax.swing.DefaultComboBoxModel#getElementAt(int)
     */
    public Object getElementAt(int index)
    {
        return items.get( index );
    }


    /**
     * @see javax.swing.DefaultComboBoxModel#getSize()
     */
    public int getSize()
    {
        return items.size();
    }

    
    /**
     * This method stores the values of the combobox model to the system property given
     * by <code>propertyName</code>. The values are seperated by colons.
     * 
     * @param propertyName The name of the property which is used to store the item list.
     */
    public void saveListToSystemProperties( String propertyName ){
        StringBuffer s = new StringBuffer();
        Iterator<String> it = items.iterator();
        while( it.hasNext() ){
            String str = it.next();
            s.append( str );
            if( it.hasNext() )
                s.append(":");
        }
        System.setProperty( propertyName, s.toString() );
    }
}