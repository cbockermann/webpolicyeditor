package org.jwall.app.ui;

import java.awt.event.ActionEvent;
import java.util.List;


/**
 * 
 * A handler which provides a few actions on a specific type of objects.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface ActionHandler<E>
{
    
    /**
     * Return a list of action commands, provided by this handler.
     * 
     * @return The list of actions provided by this handler.
     */
    public List<String> getActionCommands();
    
    
    /**
     * Checks whether the handler handles the given event.
     * 
     * @param e
     * @return
     */
    public boolean handlesEvent( ActionEvent e );
    
    
    /**
     * Delegates the action and the object on which the event will be acted on to the handler.
     * 
     * @param e The event.
     * @param object The object to be handled.
     */
    public void dispatchEvent( ActionEvent e, E object );
}