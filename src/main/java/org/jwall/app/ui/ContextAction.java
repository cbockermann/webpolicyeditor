package org.jwall.app.ui;

import java.awt.event.ActionEvent;


/**
 * 
 * This interface defines a context action, i.e. an action that can be fired within
 * a specific context, such as a collection of objects. 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 * @param <E>
 */
public interface ContextAction<E>
{

    /**
     * Return a descriptive name for this action.
     * 
     * @return The action's name.
     */
    public String getName();
    
    
    /**
     * This method is called when the action has been selected.
     * 
     * @param e The event that fired this action (usually a MouseEvent).
     * @param object The object, defining the context in which this action has been fired.
     */
    public void actionPerformed( ActionEvent e, E object );
}
