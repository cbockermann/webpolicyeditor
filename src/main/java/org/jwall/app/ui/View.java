package org.jwall.app.ui;

import java.util.LinkedList;
import java.util.List;

import javax.swing.JPanel;

import org.jwall.app.Action;
import org.jwall.app.Application;

public class View
extends JPanel 
{
	public final static long serialVersionUID = 4353534L;
	
	Application app = null;
	protected String name = "";
	
	protected LinkedList<Action> actions = new LinkedList<Action>();
	
	
	public View( Application app ){
		this.app = app;
	}
	
	public Action getAction( String cmd ){
		if( app == null )
			return null;
		
		return app.getAction( cmd );
	}
	
	public Application getApplication(){
		return app;
	}
	
	public String getName(){
		return name;
	}
	
	public List<Action> getActions(){
	    return actions;
	}
	
	public void viewSelected(){
	}
}