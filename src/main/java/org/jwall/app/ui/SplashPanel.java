package org.jwall.app.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.net.URL;
import java.util.LinkedList;

public class SplashPanel extends ImagePanel {
	public final static long serialVersionUID = 435345L;
	Color c = Color.WHITE;
	LinkedList<SplashMessage> msgs = new LinkedList<SplashMessage>();

	
	
	public SplashPanel( URL url, int x, int y, int w, int h){
		super( url, x, y, w, h );
	}
	
	
	public void setMessage( int x, int y, String s ){
		msgs.add( new SplashMessage( x, y, s ) );
		repaint();
	}
	
	public void addMessage( SplashMessage m ){
	    msgs.add( m );
	}
	
	public void setMessageColor( Color col ){
		c = col;
	}


	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		for( SplashMessage m : msgs ){

		    if( m.color != null )
			    g.setColor( m.color );
		    else
		        g.setColor( c );

		    if( m.font != null )
			    g.setFont( m.font );
		    
			g.drawString( m.msg, m.x, m.y );
		}
	}
}
