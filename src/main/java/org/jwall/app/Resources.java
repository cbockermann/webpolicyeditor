package org.jwall.app;

import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.ImageIcon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("Resources")
public class Resources {

	static Logger log = LoggerFactory.getLogger(Resources.class);

	@XStreamImplicit
	private TreeSet<Resource> resources = new TreeSet<Resource>();

	public Set<Resource> getResources() {
		return resources;
	}

	public Resource getActionById(String id) {
		for (Resource act : resources) {
			if (act.getId().equals(id))
				return act;
		}

		return null;
	}

	public static XStream getXStream() {
		XStream xs = new XStream();
		xs.processAnnotations(Resources.class);
		xs.processAnnotations(Resource.class);
		// xs.processAnnotations( Action.class );
		return xs;
	}

	public void load(String url) {
		if (url == null)
			return;

		InputStream in = Resources.class.getResourceAsStream(url);
		if (in == null)
			return;

		Resources res = (Resources) getXStream().fromXML(in);
		resources.addAll(res.getResources());
	}

	public ImageIcon getIcon(Action act) {
		return getIcon(act.getCommand());
	}

	public ImageIcon getIcon(String id) {
		log.debug("Looking for icon for ID '{}'", id);
		Resource r = this.getActionById(id);
		if (r == null) {
			log.error("No resource found for id '{}'!", id);
			return new ImageIcon();
		}

		String resource = r.getIcon();

		if (!resource.startsWith("/"))
			resource = "/icons/16x16/" + resource;
		// System.out.println( "Looking up icon for " + id + " => " + resource
		// );
		URL url = Application.class.getResource(resource);
		// System.out.println( "\t" + resource + " => " + url );

		if (url == null) {
			url = Application.class.getResource("/icons/16x16/stock_left.png");
		}

		if (url == null)
			return new ImageIcon();

		return new ImageIcon(url);
	}

	public ImageIcon getLargeIcon(String id) {
		Resource r = this.getActionById(id);

		if (r == null)
			return new ImageIcon();

		String resource = r.getIcon();
		if (!resource.startsWith("/"))
			resource = "/icons/24x24/" + resource;

		URL url = Application.class.getResource(resource);

		if (url == null) {
			url = Application.class.getResource("/icons/24x24/folder.png");
		}

		if (url == null) {
			System.out.println("URL for largeIcon with id " + id + " is null!");
		}

		if (url == null) {
			System.err.println("Unable to locate image for resource by id \""
					+ id + "\"");
			return new ImageIcon();
		}
		return new ImageIcon(url);
	}

	public static void main(String[] args) {

		try {

			InputStream in = Resources.class
					.getResourceAsStream("/org/jwall/app/icons.properties");
			Properties icons = new Properties();
			icons.load(in);

			in = Resources.class
					.getResourceAsStream("/org/jwall/app/messages.properties");
			Properties msgs = new Properties();
			msgs.load(in);

			Resources res = new Resources();

			Enumeration<Object> en = icons.keys();
			while (en.hasMoreElements()) {
				String k = (String) en.nextElement();

				Resource a = new Resource(k);
				a.setIcon(icons.getProperty(k));

				if (msgs.get(k) != null) {
					String label = (String) msgs.get(k);
					a.setLabel(label);
				}

				res.getResources().add(a);
			}

			System.out.println(Resources.getXStream().toXML(res));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}