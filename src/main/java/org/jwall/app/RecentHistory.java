package org.jwall.app;

import java.util.LinkedList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;


/**
 * 
 * This class implements a recently-used history of objects, e.g. Files.
 * Basically it is simply used to wrap a list of recently used items and make
 * this history persistent using xstream.
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
@XStreamAlias("History")
public class RecentHistory<T>
{
    /** This attribute just describes a specific history list using a name.  */
    @XStreamAsAttribute
    String name;
    
    @XStreamImplicit
    LinkedList<T> lr;

    
    /**
     * Create a new RecentHistory which will be associated with the given name.
     * 
     * @param propertyName The propertyName of this history.
     */
    public RecentHistory( String propertyName ){
        this.name = propertyName;
        lr = new LinkedList<T>();
    }
    
    
    public void add( T item ){
        if( lr.contains( item ) )
            lr.remove( item );
        
        lr.add( item );
    }
    
    public T getLast(){
        if( lr.isEmpty() )
            return null;
        
        return lr.getLast();
    }
}