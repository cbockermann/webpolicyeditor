package org.jwall.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.jwall.app.ui.ApplicationWindow;
import org.jwall.app.ui.View;


/**
 * 
 * This abstract implementation of the module interface provides the action
 * registration part, i.e. a list-implementation of the set of actions provided.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 */
@SuppressWarnings("static-access")
public abstract class AbstractModule implements ActionListener, Module
{
    protected LinkedList<Action> actions = new LinkedList<Action>();
    protected String name;
    protected Application app;
    
    public AbstractModule(Application viewer, String name){
        this.app = viewer;
        this.name = name;
    }
    
    public void setApplication( Application a ){
    	app = a;
    }
    
    /* (non-Javadoc)
     * @see org.jwall.app.Module#getName()
     */
    public String getName(){
        return name;
    }
    
    /* (non-Javadoc)
     * @see org.jwall.app.Module#cleanUp()
     */
    public void cleanUp()
    {
    }

    /* (non-Javadoc)
     * @see org.jwall.app.Module#getAction(java.lang.String)
     */
    public Action getAction(String cmd)
    {
        Iterator<Action> it = getActions().iterator();
        while(it.hasNext()){
            Action a = it.next();
            if(a.getCommand().equals(cmd))
                return a;
        }

        return null;
    }

    /* (non-Javadoc)
     * @see org.jwall.app.Module#getActions()
     */
    public List<Action> getActions()
    {
        return actions;
    }
    
    /* (non-Javadoc)
     * @see org.jwall.app.Module#actionPerformed(java.awt.event.ActionEvent)
     */
    public abstract void actionPerformed(ActionEvent arg0);
    
    
    protected void add(Action act){
        getActions().add(act);
    }
    
    /* (non-Javadoc)
     * @see org.jwall.app.Module#getApplication()
     */
    public Application getApplication(){
        return app;
    }
    
    /* (non-Javadoc)
     * @see org.jwall.app.Module#getWindow()
     */
    public ApplicationWindow getWindow(){
        return getApplication().getWindow();
    }
    
    /* (non-Javadoc)
     * @see org.jwall.app.Module#getView()
     */
    public View getView(){
        return null;
    }
}