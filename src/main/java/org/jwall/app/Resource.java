package org.jwall.app;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("Resource")
public class Resource implements Comparable<Resource> {
	@XStreamAlias("id")
	@XStreamAsAttribute
	private String id = "";

	@XStreamAlias("Label")
	private String label = "";

	@XStreamAlias("Tooltip")
	private String tooltip = "";

	@XStreamAlias("Icon")
	private String icon = "";

	public Resource() {

	}

	public Resource(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the tooltip
	 */
	public String getTooltip() {
		return tooltip;
	}

	/**
	 * @param tooltip
	 *            the tooltip to set
	 */
	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

	/**
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @param icon
	 *            the icon to set
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int compareTo(Resource other) {
		return id.compareTo(other.id);
	}
}