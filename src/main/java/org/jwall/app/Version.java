package org.jwall.app;


/**
 * 
 * This class implements some version information of this java library. It
 * can be invoked to print out the version to the standard output.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class Version
{
    public final static String VERSION = "0.2";

    static {
        System.setProperty( "org.jwall.app.version", VERSION );
    }
    
    /**
     * Return the version number as string.
     * @return
     */
    public static String getVersion(){
        return VERSION;
    }
    
    
    /**
     * Return the name of the component to which this version
     * information belongs.
     * 
     * @return The name of the software component.
     */
    public static String getComponentName(){
        return "org.jwall.app";
    }
    
    
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        
        for( String arg : args ){
            if( arg.equalsIgnoreCase( "-v" ) || arg.equalsIgnoreCase( "--version" ) ){
                System.out.print( getComponentName() + " " );
                break;
            }
        }
        
        System.out.print( getVersion() );
    }
}