package org.jwall.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
@XStreamAlias("Action")
public class Action
extends AbstractAction
implements Comparable<Action>
{
    private static final long serialVersionUID = 5826594554252052501L;

    private static Logger log = Logger.getLogger("Action");
	
	public final static String NEW_FILE = "org.jwall.app.file.new";
	public final static String LOAD_FILE = "org.jwall.app.file.open";
	public final static String SAVE_FILE = "org.jwall.app.file.save";
	public final static String SAVE_AS_FILE = "org.jwall.app.file.save.as";
	
	public final static String QUIT = "org.jwall.app.file.quit";
	public final static String ABOUT = "org.jwall.app.help.about";
	public final static String HELP = "org.jwall.app.help.help";
	
	protected LinkedList<ActionListener> listener = new LinkedList<ActionListener>();
	protected String command;
	protected double menuIndex = -1;
	protected String menuLabel = null;
	protected boolean toolbar = false;
	protected KeyStroke keyStroke = null;

	
	public Action( String cmd, String menu, double idx ){
	    command = cmd;
	    menuLabel = menu;
	    menuIndex = idx;

	    putValue( Action.ACTION_COMMAND_KEY, cmd );
	    putValue( Action.SMALL_ICON, Application.getIcon( cmd ) );
	}
	
	/**
	 * 
	 * @param l
	 * @param cmd
	 * @param menu
	 */
	public Action(ActionListener l, String cmd, String menu, double idx){
	    this( cmd, menu, idx );
	    addActionListener( l );
	}

	public Action( ActionListener l, String cmd, String ml, double idx, boolean tool ){
		this( l, cmd, ml, idx, tool, true );
		if( ! tool )
		    if( Application.getMessage( cmd ) != null )
		        putValue( Action.NAME, Application.getMessage( cmd ) );
	}

	public Action( ActionListener l, String cmd, String ml, double idx, boolean tool, boolean enabled ){
		this( l, cmd, ml, idx);
		toolbar = tool;
		setEnabled( enabled );
	}

	public Action( ActionListener l, String cmd, String ml, double idx, boolean tool, boolean enabled, KeyStroke ks ){
	    this( l, cmd, ml, idx, tool, enabled );
	    keyStroke = ks;
	}

	public String getCommand(){
		return command;
	}

	public void setMenuLabel( String l ){
		menuLabel = l;
	}

	public String getMenu(){
		if( menuLabel == null )
			return null;

		int idx = menuLabel.indexOf(".");
		if( idx < 0 )
			return menuLabel;
		else
			return menuLabel.substring(0, idx );
	}

	public double getMenuIndex(){
		return menuIndex;
	}

	public String getMenuLabel(){
		return menuLabel;
	}

	public boolean showInToolbar(){
		return toolbar;
	}

	public boolean handles( ActionEvent e ){
		return command.equals( e.getActionCommand() );
	}

	public void actionPerformed(ActionEvent e)
	{
		if( listener == null )
			return;

		if( handles( e ) ){
		    for( ActionListener l : listener )
		        if( l != null )
		            l.actionPerformed( e );
		} else
			log.finest( "Action["+command+"] not responsible for event " + e );
	}

	public int compareTo( Action act ){
		if( getMenuIndex() == act.getMenuIndex() )
			return command.compareTo( act.getCommand() );

		if( getMenuIndex() < act.getMenuIndex() )
			return -1;
		else
			return 1;
	}
	
	public KeyStroke getKeyStroke(){
	    return keyStroke;
	}

	public Action setKeyStroke( KeyStroke ks ){
	    keyStroke = ks;
	    return this;
	}
	
	public List<ActionListener> getActionListener(){
		return listener;
	}
	
	public void addActionListener( ActionListener l ){
	    if( l != null && !listener.contains( l ))
	        listener.add( l );
	}
	
	public void removeActionListener( ActionListener l ){
	    if( l != null && listener.contains( l ) )
	        listener.remove( l );
	}
}