package org.jwall.app;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.jwall.app.ui.BugReporterDialog;

/**
 * 
 * This class will create a report of the exception that is to be handled and
 * display that report to the user. The user is asked if he is willing to submit
 * the information presented in the report to the jwall.org site.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class BugReporter implements ExceptionHandler {

	static Logger log = Logger.getLogger("BugReporter");

	public final static String VERSION = "$Revision:45 $";

	/**
	 * Create a new bug reporter.
	 */
	public BugReporter() {

	}

	/**
	 * In this method we create the stacktrace-report and open the dialog to
	 * display it to the user.
	 * 
	 * @see org.jwall.app.ExceptionHandler#handleException(java.lang.Exception)
	 */
	public void handleException(Exception e) {
		Map<String, String> reportData = new HashMap<String, String>();

		reportData.put("Version Information", collectVersionInformation());
		reportData.put("Stack Trace", collectStackTrace(e));

		BugReporterDialog d = new BugReporterDialog(reportData);

		d.setVisible(true);

		Message m = new Message();
		m.setMessage(d.getComment());

		Map<String, String> attachments = d.getReport();

		for (String key : attachments.keySet())
			m.addAttachment(key, attachments.get(key));

		if (d.getUserChoice() == BugReporterDialog.SEND_REPORT) {
			log.fine("Sending bug-report...");
			log.fine(m.toXML());
			this.sendReport(m);
		} else {
			log.fine("Sending of BugReport cancelled...");
		}
	}

	public String collectStackTrace(Exception e) {
		StringWriter str = new StringWriter();
		PrintWriter out = new PrintWriter(str);
		e.printStackTrace(out);

		out.flush();
		out.close();

		return str.toString();
	}

	public String collectVersionInformation() {
		StringBuffer s = new StringBuffer();

		for (Object o : System.getProperties().keySet()) {

			String key = o.toString();
			if (key.indexOf("os.") >= 0 || key.endsWith(".version"))
				s.append(key + " = " + System.getProperty(key) + "\n");
		}

		s.append("org.jwall.app.BugReporter.version = " + VERSION);

		return s.toString();
	}

	public void sendReport(Message m) {
		try {

			URL url = new URL("http://www.jwall.org/bug-report");
			URLConnection con = url.openConnection();

			con.setDoInput(true);
			con.setDoOutput(true);

			PrintStream out = new PrintStream(con.getOutputStream());
			out.println(m.toXML());

			out.flush();
			out.close();

			if (con.getDoInput()) {
				BufferedReader r = new BufferedReader(new InputStreamReader(
						con.getInputStream()));
				StringBuffer s = new StringBuffer();
				String line = r.readLine();
				while (line != null) {
					s.append(line + "\n");
					line = r.readLine();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String args[]) {
		BugReporter r = new BugReporter();

		try {
			System.out.println(args[2]);
		} catch (Exception e) {
			r.handleException(e);
		}

		System.exit(0);
	}
}