package org.jwall.app.list;


import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;
import java.util.logging.Logger;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreeCellRenderer;

import org.jwall.app.Action;


public class AbstractListView<T>
extends JPanel
implements KeyListener, MouseListener
{
    private static final long serialVersionUID = 6488498199842843197L;
    protected static Logger log = Logger.getLogger( "AbstractListView" );
    protected JTree list;
    protected AbstractListModel<T> lm;
    protected JTextField search = new JTextField();
    int keyPressed = 0;
    protected ListFilter<T> filter;
    public TreeCellEditor editor = null;
    LinkedList<Action> actions = new LinkedList<Action>();
    
    public AbstractListView( AbstractListModel<T> listModel, TreeCellRenderer renderer, ListFilter<T> listFilter )
    {
        lm = listModel;
        filter = listFilter;
        
        lm.setFilter( filter );
        list = new JTree( lm );
        list.setRootVisible( false );
        list.setCellRenderer( renderer );
        
        setLayout( new BorderLayout() );
        JScrollPane sp = new JScrollPane( list );
        setBorder( new EmptyBorder( 2,2,2,2 ) );
        add( sp, BorderLayout.CENTER );

        GridBagLayout g = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();

        c.gridy = 0;
        c.ipadx = 4;
        c.anchor = GridBagConstraints.WEST;

        JPanel p = new JPanel( g );

        //JButton add = new JButton("");
        //add.setIcon( Application.getIcon( "/icons/16x16" ));

        //c.gridx = 0;
        //g.setConstraints( add, c ); 
        //p.add( add );
        
        JLabel sl = new JLabel( "Search:" );
        c.gridx = 0;
        //c.weightx = 1.0d;
        g.setConstraints( sl, c );

        p.add( sl );

        c.ipadx = 0;
        c.gridx = 1;
        c.weightx = 1.0d;
        c.fill = GridBagConstraints.HORIZONTAL;
        g.setConstraints( search, c );
        p.add( search );


        search.setFont( search.getFont().deriveFont( Font.BOLD ) );
        search.addKeyListener( this );
        p.setBorder( new EmptyBorder( 4, 2, 2, 0 ) );
        add( p, BorderLayout.NORTH );
    }

    public AbstractListModel<T> getListModel(){
        return lm;
    }
    
    public void keyPressed(KeyEvent e)
    {
        keyPressed = e.getKeyCode();
    }

    public void keyReleased(KeyEvent e)
    {
        if( keyPressed == KeyEvent.VK_ESCAPE && keyPressed == e.getKeyCode() ){
            filter.setFilterExpression( null );
            lm.doFilter();
            search.setText("");
        }
    }

    public void keyTyped(KeyEvent e)
    {
        //if( e.getSource() == this.list && !list.isSelectionEmpty() ){
            
        //}
        
        if( e.getSource() == search && search.getText() != null && search.getText().length() > 0 ){
            String q = search.getText() + e.getKeyChar();
            log.fine("Searching for ParameterType: \"" + q + "\"");

            if( filter != null ){
                filter.setFilterExpression( ".*" + q + ".*" );
                lm.doFilter();
            }
        } else {
            if( filter != null ){
                filter.setFilterExpression( null );
                lm.doFilter();
            }
        }
            
    }

    public void setCellEditor( TreeCellEditor editor ){
        this.editor = editor;
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent e)
    {
    }
    
    
    

    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent e)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent e)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent e)
    {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    public void mouseReleased(MouseEvent e)
    {
        // TODO Auto-generated method stub
        
    }
}