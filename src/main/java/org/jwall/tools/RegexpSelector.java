package org.jwall.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.Vector;

import javax.swing.ImageIcon;

import org.jwall.web.policy.editor.regexp.RegexpSelectionDialog;

public class RegexpSelector
{
    public static ImageIcon ICON_YES;
    public static ImageIcon ICON_NO;

    public static ImageIcon loadIcon( String path ) throws Exception {
        URL url = RegexpSelector.class.getResource( path );

        if( url == null ){
            url = RegexpSelector.class.getResource( path );
        }

        if( url == null )
            return null;

        return new ImageIcon(url);
    }


    public static void main( String args[] ){
        Vector<String> samples = new Vector<String>();
        
        try {
            ICON_NO =  loadIcon( "/icons/16x16/no.png");
            ICON_YES =  loadIcon( "/icons/16x16/yes.png");
            
            if( args.length > 0){
                File sf = new File( args[0] );
                if( sf.canRead() ){
                    
                    BufferedReader r = new BufferedReader( new FileReader( sf ) );
                    String line = r.readLine();
                    while( line != null ){
                        samples.add( line );
                        line = r.readLine();
                    }

                    r.close();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        RegexpSelectionDialog d = new RegexpSelectionDialog( samples );
        d.setVisible( true );
        System.exit(0);
    }
}
