package org.jwall.web.policy.editor.filter;

import java.awt.Component;
import java.util.Iterator;

import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.jwall.WebPolicyEditor;
import org.jwall.web.filter.ids.FilterRule;

public class FilterTreeCellRenderer extends DefaultTreeCellRenderer {
	private static final long serialVersionUID = 5268775699708228304L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.swing.tree.DefaultTreeCellRenderer#getTreeCellRendererComponent
	 * (javax.swing.JTree, java.lang.Object, boolean, boolean, boolean, int,
	 * boolean)
	 */
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean sel, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {
		super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf,
				row, hasFocus);

		if (value == null) {
			return this;
		}

		if (!(value instanceof FilterRule))
			return this;

		FilterRule rule = (FilterRule) value;

		setBorder(new EmptyBorder(4, 4, 4, 4));

		StringBuffer s = new StringBuffer("<html>Rule: <code>"
				+ HTMLEntityEncode(rule.getRegex()) + "</code><br>");

		s.append("Id: <b>" + rule.getId() + "</b>, ");
		s.append("Tags: ");
		Iterator<String> it = rule.getTags().iterator();
		while (it.hasNext()) {
			s.append("<b>" + it.next() + "</b>");
			if (it.hasNext())
				s.append(", ");
		}

		s.append("</html>");
		setText(s.toString());
		setToolTipText(rule.getDescription());
		setIcon(WebPolicyEditor
				.getIcon("org.jwall.web.filter.ids.IncludeFilterNode"));

		return this;

	}

	public static String HTMLEntityEncode(String s) {
		StringBuffer buf = new StringBuffer();
		int len = (s == null ? -1 : s.length());

		for (int i = 0; i < len; i++) {
			char c = s.charAt(i);
			if (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c >= '0'
					&& c <= '9') {
				buf.append(c);
			} else {
				buf.append("&#" + (int) c + ";");
			}
		}
		return buf.toString();
	}
}
