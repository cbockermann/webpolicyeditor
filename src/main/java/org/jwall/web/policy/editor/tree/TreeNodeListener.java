package org.jwall.web.policy.editor.tree;

import java.awt.event.ActionEvent;

import org.jwall.web.policy.AbstractTreeNode;

/**
 * 
 * 
 * @author Christian Bockermann <chris@jwall.org>
 *
 */
public interface TreeNodeListener
{
    public void actionPerformed(ActionEvent e, AbstractTreeNode node);
}
