package org.jwall.web.policy.editor.filter;

import org.jwall.app.list.ListFilter;
import org.jwall.web.filter.ids.FilterRule;


/**
 * 
 * This is a simple filter implementation for filtering FilterRule instances. Basically
 * it simply matches all FilterRules which contain a tag that matches a given regular
 * expression. 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class FilterRuleListFilter
    implements ListFilter<FilterRule>
{
    String regex = null;
    
    public FilterRuleListFilter(){
    }
    
    public void setFilterExpression( String exp ){
        regex = exp;
    }

    public boolean matches( FilterRule object)
    {
        if( regex == null )
            return true;
        
        if( object.getId() != null && object.getId().matches( regex ) )
        	return true;
        
        for( String tag : object.getTags() )
            if( tag.matches( regex ) )
                return true;
        
        return false;
    }
}