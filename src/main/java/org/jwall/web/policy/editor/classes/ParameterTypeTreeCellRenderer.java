package org.jwall.web.policy.editor.classes;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.jwall.WebPolicyEditor;
import org.jwall.web.policy.abstraction.ParameterType;

public class ParameterTypeTreeCellRenderer
extends DefaultTreeCellRenderer
{
    private static final long serialVersionUID = 5268775699708228304L;
    public final static Color EVEN_COLOR = new Color( 240, 255, 255 );

    private double size = 10.0;
    
    
    
    /* (non-Javadoc)
     * @see javax.swing.tree.DefaultTreeCellRenderer#getTreeCellRendererComponent(javax.swing.JTree, java.lang.Object, boolean, boolean, boolean, int, boolean)
     */
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value,
            boolean sel, boolean expanded, boolean leaf, int row,
            boolean hasFocus)
    {
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        
        if(! ( value instanceof ParameterType ) )
            return this;
        
        if( size < 0 ){
            setFont( getFont().deriveFont( 9.0f ) );
        } else {
            setFont( getFont().deriveFont( 11.0f ) );
            setBorder( new EmptyBorder( 4,4,4,4 ) );
        }

        if( ! sel && ! hasFocus ){
            if( row % 2 == 0 )
                setBackground( EVEN_COLOR );                
        }
        
        ParameterType p = (ParameterType) value;
        
        StringBuffer s = new StringBuffer("<html><b>"+p.getName()+"</b><br>");

        s.append("" + p.getRegexp() + "");

        s.append("</html>");
        setText( s.toString() );
        setIcon( WebPolicyEditor.getIcon(p) );
        this.setIconTextGap( 10 );
        if( p.getComment() != null )
            setToolTipText( p.getComment() );

        return this;

    }
}