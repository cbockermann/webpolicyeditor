package org.jwall.web.policy.editor.classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.jwall.app.list.AbstractListModel;
import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.abstraction.ParameterType;

public class ParameterTypeListAction
    extends ListAction
{
    private static final long serialVersionUID = 5188917979807578434L;
    
    public final static String LIST_ADD_PARAMETER_TYPE = "org.jwall.web.policy.list.add_type";
    public final static String LIST_EDIT_PARAMETER_TYPE = "org.jwall.web.policy.list.edit_type";
    public final static String LIST_DELETE_PARAMETER_TYPE = "org.jwall.web.policy.list.delete_type";

    protected ActionListener listener;
    
    public ParameterTypeListAction( String cmd, AbstractListModel<ParameterType> tree, ActionListener listener ){
        super( cmd, tree );
        this.listener = listener;
    }
    
    public ParameterTypeListAction( String cmd, AbstractListModel<ParameterType> tree, ActionListener listnr, String mpath ){
        super( cmd, tree );
        listener = listnr;
        menuPath = mpath.split("/");
    }
    
    
    public void actionPerformed( ActionEvent e ){
        listener.actionPerformed( e );
    }
    
    public String[] getMenuPath(){
        return menuPath;
    }

    /* (non-Javadoc)
     * @see org.jwall.web.profile.editor.classes.ListAction#enabledForNode(org.jwall.web.profile.TreeNode)
     */
    @Override
    public boolean enabledForNode(TreeNode node)
    {
        return showForNode( node );
    }

    /* (non-Javadoc)
     * @see org.jwall.web.profile.editor.classes.ListAction#showForNode(org.jwall.web.profile.TreeNode)
     */
    @Override
    public boolean showForNode(TreeNode node)
    {
        
        if( this.getCommand() == LIST_ADD_PARAMETER_TYPE )
            return true;
        
        if( getCommand() == LIST_DELETE_PARAMETER_TYPE )
            return node != null;
        
        if( getCommand() == LIST_EDIT_PARAMETER_TYPE )
            return node != null;
            
        return true;
    }
}
