package org.jwall.web.policy.editor.blogic;

import javax.swing.JPanel;

import org.jwall.web.policy.editor.AbstractPolicyModel;


/**
 * 
 * This class implements a simple view of the business-logic of a given
 * rule-tree model.
 * 
 * @author Christian Bockermann <chris@jwall.org>
 *
 */
public class BusinessLogicView extends JPanel {
	public final static long serialVersionUID = 4345341L;
	
	AbstractPolicyModel model;
	
	
	/**
	 * 
	 * Creates a new view of the business-logic as defined by the token-elements
	 * of the given rule-tree model.
	 * 
	 * @param rtm The rule-tree model that the view is displaying.
	 */
	public BusinessLogicView( AbstractPolicyModel rtm ){
		
		model = rtm;
	}
	
	/*
	public List<SessionStateNode> getStates(){
		
		return null;
	}
	*/
}
