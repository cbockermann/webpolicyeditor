package org.jwall.web.policy.editor.regexp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;

import org.jwall.tools.RegexpSelector;
import org.jwall.app.ui.Dialog;

/**
 * 
 * This dialog asks the user for a regular expression and lists all resources that
 * match the entered expression.
 * 
 * @author chris@jwall.org
 *
 */
public class RegexpSelectionDialog 
extends Dialog 
implements ActionListener, KeyListener
{
    private static final long serialVersionUID = -6224742131102753287L;

    private JButton ok,cancel;
	private JList list;
	private DefaultListModel lm = new DefaultListModel();
	private JTextField re = new JTextField(30);
	private Border borderOk = re.getBorder();
	String exp = "";
	JTextField newVal = new JTextField(30);
	HighlightListCellRenderer renderer = new HighlightListCellRenderer( null );
	
	/**
	 * This initializes a new merge-dialog. The selection only affects the child-nodes
	 * of the given parent.
	 * 
	 * @param parent The node which's children shall be merged.
	 */
	public RegexpSelectionDialog(){
		setTitle("Select Regular Expression");
		setModal( true );
		getContentPane().setLayout( new BorderLayout() );

		JPanel p = new JPanel( new FlowLayout() );
		p.add( new JLabel("Regular Expression: ") );
		re = new JTextField(30);
		re.addKeyListener( this );
		p.add( re );
		
		JPanel ct = new JPanel( new BorderLayout() );
		ct.add( p, BorderLayout.NORTH );
		//getContentPane().add( p, BorderLayout.NORTH );
		lm.clear();
		list = new JList(lm);
		list.setCellRenderer( renderer );
		
		ct.add( new JLabel( "Sample values:" ) );
		
		JScrollPane sp = new JScrollPane(list);
		//getContentPane().add( sp, BorderLayout.CENTER );
		ct.add( sp, BorderLayout.CENTER );
		
		JPanel input = new JPanel( new FlowLayout( FlowLayout.LEFT ) );
		input.add( new JLabel("Add sample: ") );
		input.add( newVal );
		newVal.addKeyListener( this );
		ct.add( input, BorderLayout.SOUTH );
		
		getContentPane().add( ct, BorderLayout.CENTER );
		
		JPanel buttons = new JPanel( new FlowLayout() );
		ok = new JButton("Ok");
		ok.addActionListener( this );
		buttons.add( ok );

		cancel = new JButton("Cancel");
		cancel.addActionListener( this );

		buttons.add( cancel );
		getContentPane().add( buttons, BorderLayout.SOUTH );

		pack();
		center();
	}

	
	public RegexpSelectionDialog( Collection<String> samples ){
	    this();
	    for( String s: samples )
	        lm.addElement( s );
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e){

		if( e.getSource() == ok )
			ok();
		
		if( e.getSource() == cancel )
			cancel();
	}
	
	public void ok(){
		setVisible( false );
	}
	
	public void cancel(){
		exp = null;
		setVisible( false );		
	}
	
	public String getSelectedExpression(){
	    return exp;
	}

	/**
	 * This method compiles the entered regular expression and updates the list of resources
	 * that match this expression. If the text-field contains only a partial entered expression,
	 * the list is not updated.
	 */
	private void updateList(){
		exp = re.getText();
		
		try {
		    if( exp.startsWith("!") ){
		        renderer.setNegate( true );
		        exp.substring( 1 );
		    } else
		        renderer.setNegate( false );
			Pattern.compile( exp );
		} catch (Exception e){
			re.setBorder( BorderFactory.createLineBorder( Color.RED ) );
			return;
		}
		
		re.setBorder( this.borderOk );
		renderer.setRegex( exp );
		list.repaint();
	}

	
	/**
	 * Returns the expression entered by the user.
	 * 
	 * @return A (possibly only partly entered) regular expression 
	 */
	public String getRegularExpression(){
		return exp;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	public void keyPressed(KeyEvent e) {
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	public void keyReleased(KeyEvent e) {
		if( e.getKeyCode() == KeyEvent.VK_ESCAPE )
			cancel();
		
		if( !newVal.getText().trim().equals("") && e.getSource() == newVal && e.getKeyCode() == 10 ){
		    lm.addElement( newVal.getText() );
		    newVal.setText("");
		}
		updateList();
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	public void keyTyped(KeyEvent e) {
	}
	
	class HighlightListCellRenderer extends DefaultListCellRenderer {

	    public final static long serialVersionUID = 234324L;
	    String regex;
	    boolean neg = false;
	    
	    public HighlightListCellRenderer( String regex ){
	        this.regex = regex;
	    }
	    
	    public void setRegex( String r ){
	        regex = r;
	        /*
	        if( ! r.startsWith("^") )
	            regex = ".*" + r;
	        
	        if( ! r.endsWith( "$") )
	            regex = regex + ".*";
	         */
	    }
	    
	    public void setNegate( boolean b ){
	        neg = b;
	    }

	    @Override
	    public Component getListCellRendererComponent(JList arg0, Object arg1, int arg2, boolean arg3, boolean arg4) {
	        super.getListCellRendererComponent(arg0, arg1, arg2, arg3, arg4);

	        setText( arg1.toString() );
	        
	        if( arg2 % 2 > 0 )
	            setBackground( new Color( 0xf0, 0xf0, 0xf0 ) );

	        Pattern p = Pattern.compile( regex );
	        Matcher m = p.matcher( arg1.toString() );
	        
	        if( regex != null && (! m.find() ) ){
	            if( RegexpSelector.ICON_NO != null )
	                setIcon( RegexpSelector.ICON_NO );
	            setForeground( Color.RED );
	        } else {
                if( RegexpSelector.ICON_NO != null )
                    setIcon( RegexpSelector.ICON_YES );
	        }
	        
	        return this;
	    }
	}
}