package org.jwall.web.policy.editor.tree;

import java.awt.event.ActionEvent;

import org.jwall.web.policy.AbstractTreeNode;
import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.editor.AbstractPolicyModel;


/**
 * 
 * This class implements an abstract action that might be applied to a tree. Instances
 * of this class can be used to create menu-items. 
 * 
 * This class also provides methods for context sensitivity.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public abstract class AbstractTreeAction 
extends org.jwall.app.Action 
implements TreeNodeListener
{
    public final static long serialVersionUID = 423544597654645L;

    public final static String TREE_DELETE_NODE = "org.jwall.web.policy.editor.tree.delete";
    public final static String TREE_INSERT_NODE = "org.jwall.web.policy.editor.tree.new";
    public final static String TREE_EDIT_NODE = "org.jwall.web.policy.editor.tree.edit";

    public final static String TREE_CANCEL_EDITING = "org.jwall.web.policy.editor.tree.edit.cancel";
    public final static String TREE_STOP_EDITING = "org.jwall.web.policy.editor.tree.edit.stop";

    public final static String TREE_MERGE_BY_RE = "org.jwall.web.policy.editor.tree.merge";

    public final static String TREE_INSERT_RESOURCE = "org.jwall.web.policy.editor.tree.add.resource";
    public final static String TREE_INSERT_METHOD = "org.jwall.web.policy.editor.tree.add.method";
    public final static String TREE_INSERT_PARAMETER = "org.jwall.web.policy.editor.tree.add.parameter";
    public final static String TREE_INSERT_HEADER = "org.jwall.web.policy.editor.tree.add.header";
    public final static String TREE_INSERT_COOKIE = "org.jwall.web.policy.editor.tree.add.cookie";

    public final static String TREE_INSERT_LIMIT = "org.jwall.web.policy.editor.tree.add.limit";
    public final static String TREE_INSERT_FILTER_TAGS = "org.jwall.web.policy.editor.tree.add.filter_tags";
    public final static String TREE_INSERT_RESOURCE_CLASS = "org.jwall.web.policy.editor.classes.add.resource";
    
    public final static String TREE_INSERT_CREATETOKEN = "org.jwall.web.policy.editor.tree.add.createtoken";
    public final static String TREE_INSERT_CHECKTOKEN = "org.jwall.web.policy.editor.tree.add.checktoken";

    public final static String TREE_INSERT_METAINF = "org.jwall.web.policy.editor.tree.add.metainf";
    public final static String TREE_INSERT_METAINF_AUTHOR = "org.jwall.web.policy.editor.tree.add.metainfauthor";

    public final static String TREE_RESOURCE_WIZZARD = "org.jwall.web.policy.editor.resource-wizzard";
    
    private AbstractPolicyModel tree;

    String[] menuPath = new String[0];
    
    
    
    public AbstractTreeAction( String cmd, AbstractPolicyModel tree ){
        super( cmd, "", 10.0d );
        this.tree = tree;
    }

    public void setResourceTreeModel( AbstractPolicyModel rtm ){
        tree = rtm;
    }

    public AbstractPolicyModel getResourceTreeModel(){
        return tree;
    }

    
    /**
     * This method is called to check whether this instance is to be displayed for
     * the node given.
     * 
     * @param node The node which defines the &quot;context&quot;.
     * @return <code>true</code>, if this instance is to be displayed for that node.
     */
    public boolean showForNode( TreeNode node ){
        
        if( TREE_INSERT_PARAMETER.equals( getCommand() ) )
            return node.allowsChild( TreeNode.METHOD_NODE );
        
        if( TREE_INSERT_COOKIE.equals( getCommand() ) )
            return node.allowsChild( TreeNode.METHOD_NODE );
        
        if( TREE_INSERT_HEADER.equals( getCommand() ) )
            return node.allowsChild( TreeNode.METHOD_NODE );
        
        if( TREE_INSERT_LIMIT.equals( getCommand() ) )
            return node.allowsChild( TreeNode.LIMIT_NODE );
        
        return true;
    }

    
    /**
     * This method is called to check whether this instance is to be enabled for
     * the node given.
     * 
     * @param node The node which defines the &quot;context&quot;.
     * @return <code>true</code>, if this instance is to be enabled for that node.
     */
    public boolean enabledForNode( TreeNode node ){
        return false;
    }

    public String[] getMenuPath(){
        return menuPath;
    }

    
    /**
     * @see org.jwall.web.policy.editor.tree.TreeNodeListener#actionPerformed(java.awt.event.ActionEvent, org.jwall.web.policy.AbstractTreeNode)
     */
    public abstract void actionPerformed( ActionEvent evt, AbstractTreeNode node );
}