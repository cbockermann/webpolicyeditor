package org.jwall.web.policy.editor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.SortedSet;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellEditor;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.jwall.WebPolicyEditor;
import org.jwall.app.ui.SelectionDialog;
import org.jwall.app.ui.SelectionListener;
import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.WebPolicy;
import org.jwall.web.policy.abstraction.ParameterType;
import org.jwall.web.policy.editor.classes.ParameterTypeListCellRenderer;
import org.jwall.web.policy.editor.tree.AbstractTreeAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * This abstract class provides the basic functions for a cell-editor for cells
 * of a policy tree.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public abstract class AbstractPolicyTreeCellEditor extends
		DefaultTreeCellEditor implements KeyListener, FocusListener,
		SelectionListener {
	/* The global logger for this class */
	static Logger log = LoggerFactory
			.getLogger(AbstractPolicyTreeCellEditor.class);

	protected int nodeType = 0;

	protected JTextField name = new JTextField("", 10);
	protected JTextField value = new JTextField("", 15);
	protected JTextField value2 = new JTextField("", 5);
	protected JCheckBox required = new JCheckBox("required");

	protected String[] scopes = { "any", "header", "body" };
	protected JComboBox scope = new JComboBox(scopes);
	protected TreeNode node = null;

	protected boolean cancelled = false;
	protected WebPolicy profile;

	protected String oldValue = "";

	protected JPanel p;
	protected Font f;

	protected SelectionDialog selectionDialog = null;

	public AbstractPolicyTreeCellEditor(JTree tree,
			DefaultTreeCellRenderer renderer, WebPolicy profile) {
		super(tree, renderer);
		this.profile = profile;
	}

	public abstract void createUIComponents(TreeNode node);

	/**
	 * @see javax.swing.tree.DefaultTreeCellEditor#getTreeCellEditorComponent(javax.swing.JTree,
	 *      java.lang.Object, boolean, boolean, boolean, int)
	 */
	@Override
	public Component getTreeCellEditorComponent(JTree arg0, Object arg1,
			boolean arg2, boolean arg3, boolean arg4, int arg5) {
		Component c = super.getTreeCellEditorComponent(arg0, arg1, arg2, arg3,
				arg4, arg5);
		cancelled = false;

		if (arg1 instanceof TreeNode) {
			node = (TreeNode) arg1;
			f = new Font("Sansserif", Font.PLAIN, 11);

			p = new JPanel(new FlowLayout());
			p.setBackground(Color.LIGHT_GRAY);
			p.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
			p.add(new JLabel(AbstractPolicyTreeRenderer.getIcon(node)));
			nodeType = node.getType();

			createUIComponents(node);

			JPanel b = new JPanel();
			b.setAlignmentY(Component.CENTER_ALIGNMENT);
			b.setBorder(null);
			// b.setLayout( new GridLayout( 2 , 1) );
			b.setOpaque(true);
			b.setBackground(Color.LIGHT_GRAY);

			JButton cancel = new JButton();
			cancel.setBorder(null);
			cancel.setBackground(Color.WHITE);
			cancel.setOpaque(true);
			cancel.setSize(new Dimension(10, 10));
			cancel.setActionCommand(AbstractTreeAction.TREE_STOP_EDITING);
			cancel.setIcon(AbstractPolicyTreeRenderer
					.getIcon(AbstractTreeAction.TREE_CANCEL_EDITING));
			cancel.setContentAreaFilled(false);
			cancel.setAlignmentY(Component.CENTER_ALIGNMENT);
			cancel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cancelCellEditing();
				}
			});

			JButton stop = new JButton();
			stop.setBorder(null);
			stop.setBorderPainted(false);
			stop.setOpaque(true);
			stop.setSize(new Dimension(10, 10));
			stop.setBackground(Color.WHITE);
			stop.setContentAreaFilled(false);
			stop.setIcon(AbstractPolicyTreeRenderer
					.getIcon(AbstractTreeAction.TREE_STOP_EDITING));
			stop.setAlignmentY(Component.CENTER_ALIGNMENT);
			stop.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					stopCellEditing();
				}
			});

			b.add(stop);
			b.add(cancel);
			p.add(b);

			return p;
		}
		c.addKeyListener(this);
		return c;
	}

	public void keyPressed(KeyEvent arg0) {
		if (arg0.getKeyCode() == KeyEvent.VK_ESCAPE)
			cancelled = true;
	}

	public void keyReleased(KeyEvent arg0) {
	}

	@Override
	public void cancelCellEditing() {
		cancelled = true;
		super.cancelCellEditing();

		if (selectionDialog != null) {
			selectionDialog.cancel();
			selectionDialog = null;
		}
	}

	@Override
	public boolean stopCellEditing() {
		boolean stopped = super.stopCellEditing();

		if (stopped && selectionDialog != null)
			selectionDialog.finish();

		selectionDialog = null;
		return stopped;
	}

	public void keyTyped(KeyEvent arg0) {

		log.debug("KeyEvent: {}", arg0);

		if (arg0.getKeyCode() == KeyEvent.VK_ESCAPE) {
			cancelled = true;
			this.cancelCellEditing();
		}

		if (arg0.getKeyChar() == KeyEvent.VK_ENTER)
			this.stopCellEditing();

		if (node != null
				&& (node.getType() == TreeNode.PARAMETER_NODE
						|| node.getType() == TreeNode.HEADER_NODE || node
						.getType() == TreeNode.COOKIE_NODE)) {
			;
		} else
			return;

		// if( arg0.getKeyChar() == KeyEvent.VK_ESCAPE &&
		// value.getText().equals("")){
		// if( this.value.getText() != null && value.getText().equals( "$" ) ){
		log.debug("value.getText() = {}", value.getText());
		oldValue = value.getText();
		if (arg0.getKeyChar() == '$'
				&& (value.getText().equals("") || value.getSelectedText()
						.equals(value.getText()))) {

			SortedSet<ParameterType> types = profile.getParameterTypes();
			int i = 0;
			String[] names = new String[types.size()];
			for (ParameterType tp : types)
				names[i++] = tp.getName();

			Point p = value.getLocationOnScreen();

			selectionDialog = new SelectionDialog(types);
			selectionDialog.setCellRenderer(new ParameterTypeListCellRenderer(
					-1));
			selectionDialog.addSelectionListener(this);
			selectionDialog.show(p.x, p.y + value.getHeight());

			value.addKeyListener(selectionDialog);
		}
	}

	public void selectionFinished(Object selected, SelectionDialog dialog) {

		if (selected != null && selected instanceof ParameterType) {
			ParameterType pt = (ParameterType) selected;
			if (!cancelled)
				value.setText("${" + pt.getName() + "}");
			else
				value.setText(oldValue);
		} else
			value.setText(oldValue);

		if (dialog != null) {
			dialog.setVisible(false);
			if (selected == null)
				dialog.cancel();
			else
				dialog.finish();
		}
	}

	public void selectionChanged(Object selected, SelectionDialog dialog) {
		if (selected != null) {
			ParameterType pt = (ParameterType) selected;
			value.setText("${" + pt.getName() + "}");
		}
	}

	/**
	 * @see java.awt.event.FocusListener#focusGained(java.awt.event.FocusEvent)
	 */
	public void focusLost(FocusEvent e) {
		log.debug("FocusEvent: {}", e);
		WebPolicyEditor.getWindow().getStatusBar().setMessage("");

	}

	/**
	 * @see java.awt.event.FocusListener#focusLost(java.awt.event.FocusEvent)
	 */
	public void focusGained(FocusEvent e) {
		if (e.getSource() == value) {
			value.selectAll();
		}

		if (node != null
				&& e.getSource() == value
				&& (node.getType() == TreeNode.HEADER_NODE || node.getType() == TreeNode.PARAMETER_NODE)) {
			WebPolicyEditor
					.getWindow()
					.getStatusBar()
					.setMessage(
							"<html>Enter <b>$</b> to select a predefined Parameter-Type!</html>");
		}
	}
}