package org.jwall.web.policy.editor.tree;

import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import org.jwall.web.policy.AbstractTreeNode;
import org.jwall.web.policy.Resource;
import org.jwall.web.policy.SyntaxException;
import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.editor.AbstractPolicyModel;

public class ResourceTreeMerger
extends AbstractTreeAction
{
	public final static long serialVersionUID = 4235354L;
	public final static String COMMAND = "org.jwall.web.policy.editor.tree.merge";

	public ResourceTreeMerger( AbstractPolicyModel tree ){
		super( COMMAND, tree );
	}
	
	public boolean showForNode( TreeNode node ){
		return node.getType() == TreeNode.RESOURCE_NODE;
	}
	
	public boolean enabledForNode( TreeNode node ){
		return node.getType() == TreeNode.RESOURCE_NODE;
	}
	
	public void actionPerformed( ActionEvent evt, AbstractTreeNode node ){
		
		AbstractPolicyModel rtm = getResourceTreeModel();
		if( rtm == null )
			return;
		
		ResourceTreeMergeDialog rtmd = new ResourceTreeMergeDialog( node.getParent() );
		rtmd.setVisible( true );
		
		String exp = rtmd.getRegularExpression();
		if( exp == null )
			return;
		
		try {
			TreeNode parent = node.getParent();
			mergeResources( parent, exp );
			rtm.notify( parent );
			return;
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}
	
	public String[] getMenuPath(){
	    return new String[0];
	}
	
	public void mergeResources( TreeNode node, String exp ) throws SyntaxException {
		
		Resource newRes = new Resource( exp );
		
		Vector<Resource> res = new Vector<Resource>();
		for( TreeNode ch : node.children() ){
			if( ch.getType() == TreeNode.RESOURCE_NODE ){
				Resource r = (Resource) ch;
				if( r.getName().matches( exp ) ){
					res.add( r);
					newRes.children().addAll( r.children() );
					//mergeChildren( newRes, r.children() );
					//for( TreeNode c : r.children() )
					//	merge( newRes.children(), c );
				}
			}
		}
		
		node.children().removeAll( res );
		node.add( newRes );
	}
	
	
	public void merge( TreeNode dst, TreeNode src )
		throws SyntaxException
	{
		if( ! dst.matches( src ) )
			return;
		
		for( TreeNode ch : src.children() ){
			TreeNode dch = getAppopriateChild( dst, ch );
			if( dch == null ){
				dch = ch.copy();
			} else {
				merge( dch, ch );
			}
			dst.add( dch );
		}
	}
	
	public void mergeChildren( TreeNode dst, Collection<AbstractTreeNode> childs )
		throws SyntaxException
	{
		if( dst.children().size() == 0 ){
			for( TreeNode ch : childs )
				dst.children().add( ch.copy() );
			
			return;
		}
		
		Vector<TreeNode> newChilds = new Vector<TreeNode>();
		for( TreeNode ch : dst.children() ){
			
			for( TreeNode c : childs ){
				if( ch.matches( c ) ){
					merge( ch, c );
				} else
					newChilds.add( c.copy() );
			}
		}
		
		dst.children().addAll( newChilds );
	}
	
	
	public void merge( Collection<TreeNode> nodes, TreeNode node ) throws SyntaxException{
		TreeNode dst = null;
		
		Iterator<TreeNode> it = nodes.iterator();
		while( dst == null && it.hasNext() ){
			dst = it.next();
			if(! dst.matches( node ) )
				dst = null;
		}
		
		if( dst == null ){
			nodes.add( node.copy() );
		} else {
			merge( dst, node );
		}
	}
	
	
	public TreeNode getAppopriateChild( TreeNode parent, TreeNode template ){
		for( TreeNode ch : parent.children() ){
			if( ch.getType() == template.getType() && ch.getName().equals( template.getName() ) )
				return ch;
		}
		
		return null;
	}
}
