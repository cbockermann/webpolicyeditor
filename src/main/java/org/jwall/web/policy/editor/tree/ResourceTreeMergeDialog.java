package org.jwall.web.policy.editor.tree;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Collection;
import java.util.Vector;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;

import org.jwall.app.ui.Dialog;
import org.jwall.web.policy.AbstractTreeNode;
import org.jwall.web.policy.Resource;
import org.jwall.web.policy.TreeNode;

/**
 * 
 * This dialog asks the user for a regular expression and lists all resources that
 * match the entered expression.
 * 
 * @author chris@jwall.org
 *
 */
public class ResourceTreeMergeDialog 
extends Dialog 
implements ActionListener, KeyListener
{
    private static final long serialVersionUID = -6224742131102753287L;

    private JButton ok,cancel;
	private JList list;
	private DefaultListModel lm = new DefaultListModel();
	private JTextField re = new JTextField(30);
	private Border borderOk = re.getBorder();
	private TreeNode node;
	Vector<AbstractTreeNode> resources = new Vector<AbstractTreeNode>();
	String exp = "";
	
	
	/**
	 * This initializes a new merge-dialog. The selection only affects the child-nodes
	 * of the given parent.
	 * 
	 * @param parent The node which's children shall be merged.
	 */
	public ResourceTreeMergeDialog( TreeNode parent ){
		setTitle("Merge Resources by Regular Expression");
		setModal( true );
		node = parent;
		getContentPane().setLayout( new BorderLayout() );


		JPanel p = new JPanel( new FlowLayout() );
		p.add( new JLabel("Regular Expression: ") );
		re = new JTextField(30);
		re.addKeyListener( this );
		p.add( re );
		getContentPane().add( p, BorderLayout.NORTH );
		lm.clear();
		list = new JList(lm);
		list.setCellRenderer( new ResourceListCellRenderer() );
		
		JScrollPane sp = new JScrollPane(list);
		getContentPane().add( sp, BorderLayout.CENTER );

		JPanel buttons = new JPanel( new FlowLayout() );
		ok = new JButton("Ok");
		ok.addActionListener( this );
		buttons.add( ok );

		cancel = new JButton("Cancel");
		cancel.addActionListener( this );

		buttons.add( cancel );
		getContentPane().add( buttons, BorderLayout.SOUTH );

		pack();
		center();
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e){

		if( e.getSource() == ok )
			ok();
		
		if( e.getSource() == cancel )
			cancel();
	}
	
	public void ok(){
		setVisible( false );
	}
	
	public void cancel(){
		exp = null;
		resources = null;
		setVisible( false );		
	}

	/**
	 * This method compiles the entered regular expression and updates the list of resources
	 * that match this expression. If the text-field contains only a partial entered expression,
	 * the list is not updated.
	 */
	private void updateList(){
		exp = re.getText();
		
		try {
			Pattern.compile( exp );
		} catch (Exception e){
			re.setBorder( BorderFactory.createLineBorder( Color.RED ) );
			return;
		}
		
		re.setBorder( this.borderOk );
		
		resources = new Vector<AbstractTreeNode>();
		for( TreeNode ch : node.children() ){
			if( ch.getType() == TreeNode.RESOURCE_NODE ){
				
				Resource res = (Resource) ch;
				if( res.getName().matches(exp) ){
					resources.add( res );
				}
			}
		}
		
		lm.clear();
		for( TreeNode n : resources )
			lm.addElement( n );
	}

	
	/**
	 * Returns the expression entered by the user.
	 * 
	 * @return A (possibly only partly entered) regular expression 
	 */
	public String getRegularExpression(){
		return exp;
	}
	
	
	/**
	 * This method returns the set of resources that are selected by the
	 * given regular expression.
	 * 
	 * @return The set of resources that match the expression.
	 */
	public Collection<AbstractTreeNode> getSelectedTreeNodes(){
		return resources;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	public void keyPressed(KeyEvent e) {
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	public void keyReleased(KeyEvent e) {
		updateList();
		
		if( e.getKeyCode() == KeyEvent.VK_ESCAPE )
			cancel();
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	public void keyTyped(KeyEvent e) {
	}
}