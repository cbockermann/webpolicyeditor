package org.jwall.web.policy.editor.filter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;

import org.jwall.app.list.AbstractListView;
import org.jwall.security.cert.ZeroTrustManager;
import org.jwall.web.filter.ids.FilterRule;
import org.jwall.web.filter.ids.FilterSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * This class implements a simple view for displaying a list of filter rules.
 * 
 * @author Christian bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class FilterListView extends AbstractListView<FilterRule> {
	private static final long serialVersionUID = -4533024573466707379L;

	static Logger logger = LoggerFactory.getLogger(FilterListView.class);

	public FilterListView() {
		super(new FilterListModel(), new FilterTreeCellRenderer(),
				new FilterRuleListFilter());

		loadRules();
	}

	public void loadRules() {
		InputStream in = null;

		logger.info("Trying to load filter-rules");

		try {
			try {
				TrustManager[] zeroTrust = new TrustManager[] { new ZeroTrustManager() };

				SSLContext ctx = SSLContext.getInstance("SSL");
				ctx.init(null, zeroTrust, new java.security.SecureRandom());
				HttpsURLConnection.setDefaultSSLSocketFactory(ctx
						.getSocketFactory());
				HttpsURLConnection
						.setDefaultHostnameVerifier(new HostnameVerifier() {
							public boolean verify(String host,
									SSLSession session) {
								return true;
							}
						});

				String filterUrl = System
						.getProperty(FilterSet.PROPERTY_DEFAULT_FILTER_URL);
				if (filterUrl != null) {
					URL url = new URL(filterUrl);
					in = url.openStream();
				} else {
					in = FilterListView.class
							.getResourceAsStream("/php-ids_default_filter.xml");
				}

			} catch (Exception e) {
				// e.printStackTrace();
				try {
					in = FilterListView.class
							.getResourceAsStream("/php-ids_default_filter.xml");
					if (in == null)
						logger.info("Filter descriptions not found!");
				} catch (Exception ex) {
					// ex.printStackTrace();
				}

				if (in == null)
					in = new FileInputStream(new File(
							System.getProperty("user.home") + File.separator
									+ "php-ids_default_filter.xml"));
				else
					logger.error("Unable to open php-ids_default_filter.xml file!");
			}

			BufferedReader r = new BufferedReader(new InputStreamReader(in));
			File tmp = File.createTempFile("conv", "tmp");
			PrintStream out = new PrintStream(new FileOutputStream(tmp));
			String line = null;
			do {
				line = r.readLine();
				if (line != null && !(line.indexOf("tags>") > 0))
					out.println(line);
			} while (line != null);
			out.flush();

			loadRules(new FileInputStream(tmp));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void loadRules(URL url) throws Exception {
		loadRules(url.openStream());
	}

	public void loadRules(InputStream in) throws Exception {
		FilterSet fs = FilterSet.load(in);
		getListModel().setContents(fs.getFilterRules());
		in.close();
	}
}