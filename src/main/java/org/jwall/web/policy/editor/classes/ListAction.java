package org.jwall.web.policy.editor.classes;

import java.awt.event.ActionEvent;

import org.jwall.app.Action;
import org.jwall.app.list.AbstractListModel;
import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.abstraction.ParameterType;

public abstract class ListAction
    extends Action
{
    public final static long serialVersionUID = 423544597654645L;

    protected AbstractListModel<ParameterType> list;
    protected String[] menuPath = new String[0];

    
    public ListAction( String cmd, AbstractListModel<ParameterType> tree ){
        super( cmd, "", 10.0d );
        list = tree;
    }
    

    public void setListModel( AbstractListModel<ParameterType >rtm ){
        list = rtm;
    }

    public AbstractListModel<ParameterType> getListModel(){
        return list;
    }

    
    /**
     * This method is called to check whether this instance is to be displayed for
     * the node given.
     * 
     * @param node The node which defines the &quot;context&quot;.
     * @return <code>true</code>, if this instance is to be displayed for that node.
     */
    public abstract boolean showForNode( TreeNode node );

    
    /**
     * This method is called to check whether this instance is to be enabled for
     * the node given.
     * 
     * @param node The node which defines the &quot;context&quot;.
     * @return <code>true</code>, if this instance is to be enabled for that node.
     */
    public abstract boolean enabledForNode( TreeNode node );
    
    public abstract String[] getMenuPath();
    
    
    /**
     * @see org.jwall.web.policy.editor.tree.TreeNodeListener#actionPerformed(java.awt.event.ActionEvent, org.jwall.web.policy.AbstractTreeNode)
     */
    public abstract void actionPerformed( ActionEvent evt );    
}