package org.jwall.web.policy.editor;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.TreeModel;

import org.jwall.web.policy.editor.tree.AbstractTreeAction;

public abstract class AbstractPolicyTreeView
    extends JPanel
    implements TreeView<TreeModel>, MouseListener, KeyListener
{
    /** The unique class ID */
    private static final long serialVersionUID = -1894487625299420780L;
    
    /** This is the list of registered tree actions. */
    protected LinkedList<AbstractTreeAction> treeActions = new LinkedList<AbstractTreeAction>();

    
    /**
     * 
     * 
     */
    public AbstractPolicyTreeView(){
        super( new BorderLayout() );
        treeActions = new LinkedList<AbstractTreeAction>();
        
        this.addKeyListener( this );
    }
    
    
    /**
     * @see org.jwall.web.policy.editor.TreeView#getTreeActions()
     */
    public List<AbstractTreeAction> getTreeActions()
    {
        return treeActions;
    }

    
    /**
     * @see org.jwall.web.policy.editor.TreeView#registerTreeAction(org.jwall.web.policy.editor.tree.AbstractTreeAction)
     */
    public void registerTreeAction(AbstractTreeAction act)
    {
        treeActions.add( act );
    }

    
    /**
     * @see org.jwall.web.policy.editor.TreeView#unregisterTreeAction(org.jwall.web.policy.editor.tree.AbstractTreeAction)
     */
    public void unregisterTreeAction(AbstractTreeAction act)
    {
        treeActions.remove( act );
    }

    
    /**
     * This method returns the model that is displayed by the
     * instance of the view.
     * 
     * @return The tree model.
     */
    public abstract TreeModel getTreeModel();
    
    
    /**
     * This method returns the visualization component (usually an instance
     * of the JTree class) that is used to display the tree.
     * 
     * @return The UI component that displays the tree.
     */
    public abstract JTree getTreeComponent();


    /**
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent e)
    {
    }


    /**
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent e)
    {
    }


    /**
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent e)
    {
    }


    /**
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent e)
    {
    }


    /**
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    public void mouseReleased(MouseEvent e)
    {
    }
    
    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyTyped(KeyEvent e) {
        System.err.println("AbstractTreeView: keyTyped :: " + e );
    }
}