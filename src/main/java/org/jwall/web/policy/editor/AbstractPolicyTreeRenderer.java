package org.jwall.web.policy.editor;

import javax.swing.ImageIcon;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.jwall.app.Application;
import org.jwall.web.policy.Resource;
import org.jwall.web.policy.TreeNode;

/**
 * 
 * 
 * @author Christian Bockermann <chris@jwall.org>
 */
public abstract class AbstractPolicyTreeRenderer
    extends DefaultTreeCellRenderer
{
    public final static long serialVersionUID = 123L;

    public final static String FOLDER_ICON = "mimetype.folder";
    public final static String FILE_ICON = "mimetype.file";
    public final static String HTML_FILE_ICON = "mimetype.html";
    public final static String PHP_FILE_ICON = "mimetype.php";
    public final static String PERL_FILE_ICON = "mimetype.pl";
    public final static String PYTHON_FILE_ICON = "mimetype.py";
    public final static String CGI_FILE_ICON = "mimetype.cgi";
    public final static String JSP_FILE_ICON = "mimetype.jsp";
    public final static String GIF_FILE_ICON = "mimetype.gif";
    public final static String JPG_FILE_ICON = "mimetype.jpg";
    public final static String PNG_FILE_ICON = "mimetype.png";
    public final static String PDF_FILE_ICON = "mimetype.pdf";
    public final static String PS_FILE_ICON = "mimetype.ps";
    public final static String CSS_FILE_ICON = "mimetype.css";

    public final static String MOVIE_FILE_ICON = "mimetype-movie";
    public final static String ARCHIV_FILE_ICON = "mimetype.archiv";
    
    public final static String METHOD_ICON = "org.jwall.web.policy.Method";
    public final static String CREATE_TOKEN_ICON = "org.jwall.web.policy.CreateToken";
    public final static String CHECK_TOKEN_ICON = "org.jwall.web.policy.CheckToken";
    public final static String PARAMETER_ICON = "org.jwall.web.policy.Parameter";
    public final static String PROFILE_ICON = "org.jwall.web.policy.Profile";
    public final static String SESSION_TYPE_ICON = "org.jwall.web.policy.SessionType";
    
    public final static String META_INF_ICON = "gnome-status.png";
    public final static String META_AUTHOR_ICON = "user-info.png";
    public final static String META_VERSION_ICON = "gnome-status.png";
    
    public final static String RESOURCE_CLASSES = "org.jwall.web.policy.ResourceClasses";
    public final static String RESOURCE_CLASS_ICON = "org.jwall.web.policy.ResourceClass";
    public final static String PARAMETER_CLASSES = "org.jwall.web.policy.ParameterTypes";
    public final static String PARAMETER_CLASS_ICON = "org.jwall.web.policy.ParameterType";
    
    public TreeNode dragSource = null;
    
    
    public static ImageIcon getIcon(String s){
    	return Application.getIcon( s );
    }
    
    
    public static ImageIcon getIcon( Resource r ){
    	String pe = r.getName();
    	
        if( pe != null && pe.lastIndexOf(".") > 0 ){
            String ext = pe.substring( pe.lastIndexOf(".") + 1 );
            
            if( ext.matches("php(3|4)?") )
                return getIcon( PHP_FILE_ICON );
            
            if("pl".equals(ext))
                return getIcon( PERL_FILE_ICON );

            if("py".equals(ext) || ".py".matches( ext ) )
                return getIcon( PYTHON_FILE_ICON );
            
            if("jsp".equals(ext) || "do".equals(ext) || "action".equals(ext) || ".jsp".matches( ext ) )
                return getIcon( JSP_FILE_ICON );
            
            if("png".equals(ext) || ".png".matches( ext ) )
                return getIcon( PNG_FILE_ICON );
            
            if( ext.matches("(jpe?g|ico)") || ".jpeg".matches( ext ) || ".jpg".matches( ext ) )
                return getIcon( JPG_FILE_ICON );
            
            if( "gif".equals(ext) || ".gif".matches( ext ) )
            	return getIcon( GIF_FILE_ICON );
            
            if("pdf".equals(ext) || ".pdf".matches( ext ) )
            	return getIcon( PDF_FILE_ICON );
            
            if( "ps".equals( ext ) || ".ps".matches( ext ) )
            	return getIcon( PS_FILE_ICON );
            
            if( "css".equals( ext ) )
                return getIcon( CSS_FILE_ICON );
            
            if( "html".equals( ext) || "htm".equals( ext ))
            	return getIcon( HTML_FILE_ICON );
            
            if( ext.matches("(avi|mpg|mpeg|swf|wmv)") )
            	return getIcon( MOVIE_FILE_ICON );
            
            if( ext.matches("(zip|gz|bz|bz2)") )
            	return getIcon( ARCHIV_FILE_ICON );
        }
        
        return getIcon( FOLDER_ICON );
    }
    
    public static ImageIcon getIcon( TreeNode arg1 ){
        if( arg1.getType() == TreeNode.METHOD_NODE )
            return getIcon( METHOD_ICON );
        
        if( arg1.getType() == TreeNode.CHECK_TOKEN_NODE )
            return getIcon( CHECK_TOKEN_ICON );
        
        
        if( arg1.getType() == TreeNode.CREATE_TOKEN_NODE ){
        	return getIcon( CREATE_TOKEN_ICON );
        }
        
        if( arg1.getType() == TreeNode.META_INF_NODE ){
        	return getIcon( META_INF_ICON );
        }
        
        if( arg1.getType() == TreeNode.META_AUTHOR_NODE ){
        	return getIcon( META_AUTHOR_ICON );
        }
        
        if( arg1.getType() == TreeNode.META_VERSION_NODE ){
        	return getIcon( META_VERSION_ICON );
        }
        
        if( arg1.getType() == TreeNode.POLICY_NODE ){
        	return getIcon( PROFILE_ICON );
        }
        
        if( arg1.getType() == TreeNode.SESSION_TRACKING_NODE ){
            return getIcon( SESSION_TYPE_ICON );
        }
        
        if( arg1.getType() == TreeNode.PARAMETER_NODE ){
        	return getIcon( PARAMETER_ICON );
        }
        
        if( arg1.getType() == TreeNode.RESOURCE_CLASS ){
            return getIcon( RESOURCE_CLASS_ICON );
        }
        
        
        String pe = arg1.toString();
        
        if( pe != null && pe.lastIndexOf(".") > 0 ){
            String ext = pe.substring( pe.lastIndexOf(".") + 1 );
            
            
            if("php".equals(ext))
                return getIcon( PHP_FILE_ICON );
            
            if("pl".equals(ext))
                return getIcon( PERL_FILE_ICON );

            if("py".equals(ext))
                return getIcon( PYTHON_FILE_ICON );
            
            if("jsp".equals(ext))
                return getIcon( JSP_FILE_ICON );
            
            if("png".equals(ext))
                return getIcon( PNG_FILE_ICON );
            
            if("jpg".equals(ext))
                return getIcon( JPG_FILE_ICON );
            
            if("gif".equals(ext))
            	return getIcon( GIF_FILE_ICON );
            
            if("pdf".equals(ext))
            	return getIcon( PDF_FILE_ICON );
            
        } else
        	return getIcon( FOLDER_ICON );
        
        return getIcon( FILE_ICON );
    }
}
