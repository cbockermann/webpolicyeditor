package org.jwall.web.policy.editor.tree;

import org.jwall.web.policy.TreeNode;

public class ResourceTreePathFilter
    implements TreeNodeFilter
{
    String exp;
    
    
    public ResourceTreePathFilter(){
        exp = null;
    }
    
    public ResourceTreePathFilter( String exp ){
        this.exp = exp;
    }
    

    public void setFilterExpression( String exp ){
        
    }
 
    
    public static String getPath( TreeNode res ){
        TreeNode cur = res;
        StringBuffer s = new StringBuffer();
        
        while( cur != null ){
            if( cur.getName().startsWith( "/" ) || ( s.length() > 0 && s.charAt( 0 ) == '/' ) )
                s.insert( 0, cur.getName() );
            else
                s.insert( 0, "/" + cur.getName() );

            cur = cur.getParent();
        }
        
        String p = s.toString();
        
        while( p.indexOf("//") >= 0 )
            p = p.replaceAll( "//", "/" );
        
        return p;
    }
    
    
    public boolean matches( TreeNode node )
    {

        if( exp == null || ( node.getType() != TreeNode.RESOURCE_NODE && node.getType() != TreeNode.CONTEXT_NODE ) )
            return true;
        
        String path = getPath( node );

        // the "real match"
        //
        if( path.startsWith( exp ) )
            return true;

        // in case we are in an intermediary node and one of the children matches
        // the this nodes needs to be displayed as well
        //
        for( TreeNode ch : node.children() )
            if( ch.getType() == TreeNode.RESOURCE_NODE && matches( ch ) )
                return true;
        
        return false;
    }
}
