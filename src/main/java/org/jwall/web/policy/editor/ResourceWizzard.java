package org.jwall.web.policy.editor;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.jwall.app.ui.Dialog;

public class ResourceWizzard
    extends Dialog
{
    private static final long serialVersionUID = -8365099958923488574L;

    public ResourceWizzard(){
        setTitle( "New Resource ");
        getContentPane().setLayout( new BorderLayout() );
        
        JPanel p = new JPanel( null );
        
        
        getContentPane().add( p, BorderLayout.CENTER );
        
        JPanel b = new JPanel( new FlowLayout( FlowLayout.RIGHT ) );
        b.add( new JButton( "Ok" ) );
        b.add( new JButton( "Cancel" ) );
        
        getContentPane().add( b, BorderLayout.SOUTH );
    }
    
    
    public static void main( String args[] ){
        ResourceWizzard rw = new ResourceWizzard();
        rw.setVisible( true );
    }
}
