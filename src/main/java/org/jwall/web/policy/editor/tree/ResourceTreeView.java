package org.jwall.web.policy.editor.tree;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.jwall.WebPolicyEditor;
import org.jwall.web.filter.ids.IncludeFilterNode;
import org.jwall.web.policy.AbstractTreeNode;
import org.jwall.web.policy.Cookie;
import org.jwall.web.policy.Header;
import org.jwall.web.policy.Method;
import org.jwall.web.policy.Parameter;
import org.jwall.web.policy.Resource;
import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.WebPolicy;
import org.jwall.web.policy.editor.AbstractPolicyTreeView;
import org.jwall.web.policy.editor.DefaultPolicyTreePopupMenu;
import org.jwall.web.policy.editor.MouseUtils;
import org.jwall.web.policy.editor.TreeView;
import org.jwall.web.policy.ext.CheckToken;
import org.jwall.web.policy.ext.CreateToken;
import org.jwall.web.policy.ext.Limit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * This class implements the tree-view of the application and allows for
 * modifying or removing existing resource and adding new ones.
 * 
 * @author Christian Bockermann <chris@jwall.org>
 * 
 */
public class ResourceTreeView extends AbstractPolicyTreeView implements
		TreeView<TreeModel>, TreeSelectionListener, TreeModelListener,
		TreeNodeListener {
	/* The unique class ID */
	private static final long serialVersionUID = -7048956720896393185L;

	/* The logger for this class */
	Logger log = LoggerFactory.getLogger(ResourceTreeView.class);

	JTree jtree;
	JPanel treePanel = new JPanel(new BorderLayout());
	JTextField search = new JTextField(10);
	JButton newButton;
	PolicyTreeModel rmodel;
	MouseEvent firstMouseEvent = null;
	ResourceTreeCellRenderer cellRenderer = new ResourceTreeCellRenderer();
	List<TreeSelectionListener> externalListener = new LinkedList<TreeSelectionListener>();
	List<ResourceTreeSelectionListener> rtsListener = new LinkedList<ResourceTreeSelectionListener>();

	/**
     * 
     * 
     */
	public ResourceTreeView() {
		setLayout(new BorderLayout());
		JScrollPane sp = new JScrollPane(treePanel);
		sp.setWheelScrollingEnabled(true);
		sp.getVerticalScrollBar().setUnitIncrement(15);

		JPanel s = new JPanel(new FlowLayout(FlowLayout.LEFT));

		s.add(search);

		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_INSERT_RESOURCE, this,
				"Insert/Resource"));
		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_INSERT_METHOD, this, "Insert/Method"));
		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_INSERT_PARAMETER, this,
				"Insert/Parameter"));
		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_INSERT_HEADER, this, "Insert/Header"));
		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_INSERT_COOKIE, this, "Insert/Cookie"));
		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_INSERT_LIMIT, this, "Insert/Limit"));
		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_INSERT_FILTER_TAGS, this,
				"Insert/Filter Rules"));
		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_INSERT_CREATETOKEN, this,
				"Insert/Create-Token"));
		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_INSERT_CHECKTOKEN, this,
				"Insert/Check-Token"));

		registerTreeAction(new ResourceTreeMerger(rmodel));

		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_EDIT_NODE, this));
		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_DELETE_NODE, this));

		GridBagLayout g = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.ipadx = 4;
		c.anchor = GridBagConstraints.WEST;

		JPanel p = new JPanel(g);
		newButton = new JButton(new DefaultTreeAction(
				AbstractTreeAction.TREE_RESOURCE_WIZZARD, this));
		newButton.setIcon(WebPolicyEditor
				.getIcon(AbstractTreeAction.TREE_RESOURCE_WIZZARD));
		newButton.setBorder(new EmptyBorder(0, 2, 2, 4));
		g.setConstraints(newButton, c);

		p.add(newButton);

		c.gridx = 1;
		JLabel sl = new JLabel("Search:");

		g.setConstraints(sl, c);

		p.add(sl);

		c.ipadx = 0;
		c.gridx = 2;
		c.weightx = 1.0d;
		c.fill = GridBagConstraints.HORIZONTAL;
		g.setConstraints(search, c);
		p.add(search);

		search.setFont(search.getFont().deriveFont(Font.BOLD));
		search.addKeyListener(this);
		p.setBorder(new EmptyBorder(4, 2, 2, 0));

		add(p, BorderLayout.NORTH);
		add(sp, BorderLayout.CENTER);

		setPreferredSize(new Dimension(500, 400));
	}

	public void setProfile(WebPolicy p) {
		setRuleTreeModel(new PolicyTreeModel(p));
	}

	/**
	 * 
	 * @param model
	 */
	private void setRuleTreeModel(PolicyTreeModel model) {
		log.debug("Setting RuleTreeView to model {}", model);

		treePanel.removeAll();

		rmodel = model;
		rmodel.addTreeModelListener(this);
		jtree = new JTree(rmodel);
		jtree.setLargeModel(true);
		jtree.setEditable(rmodel.isEditable());

		for (AbstractTreeAction act : treeActions)
			act.setResourceTreeModel(rmodel);

		jtree.setDragEnabled(false);
		jtree.putClientProperty("JTree.lineStyle", "Angled");
		jtree.setAlignmentX(Component.LEFT_ALIGNMENT);
		jtree.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		jtree.setCellRenderer(cellRenderer);
		jtree.setRowHeight(34);
		jtree.setEditable(true);

		jtree.addTreeSelectionListener(this);
		for (TreeSelectionListener ext : externalListener)
			jtree.addTreeSelectionListener(ext);

		jtree.addMouseListener(this);
		jtree.setBorder(new EmptyBorder(5, 5, 5, 5));
		jtree.setCellEditor(new ResourceTreeCellEditor(jtree,
				new DefaultTreeCellRenderer(), rmodel.getProfile()));
		jtree.setVisible(true);

		jtree.validate();

		treePanel.add(jtree, BorderLayout.CENTER);
		treePanel.validate();
		this.revalidate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.swing.event.TreeSelectionListener#valueChanged(javax.swing.event
	 * .TreeSelectionEvent)
	 */
	public void valueChanged(TreeSelectionEvent e) {
		log.debug(e.toString());
		if (e.getSource() == this.jtree) {
			if (jtree.getSelectionCount() == 0)
				WebPolicyEditor.getWindow().getStatusBar().setMessage("");
			else {

				TreeNode tn = (TreeNode) jtree.getSelectionPath()
						.getLastPathComponent();
				for (ResourceTreeSelectionListener rts : rtsListener)
					rts.treeNodeSelected(tn, this.rmodel.getProfile());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	public void mouseClicked(MouseEvent e) {
		if (e.getSource() == jtree && jtree.getSelectionCount() > 0) {
			WebPolicyEditor.getWindow().getStatusBar()
					.setMessage("Press F2 to edit");
		} else
			WebPolicyEditor.getWindow().getStatusBar().setMessage("");

		evaluatePopup(e);
	}

	private void evaluatePopup(MouseEvent e) {

		if (e.getSource() == jtree
				&& (MouseUtils.isRightClick(e) || e.isPopupTrigger())) {
			// (e.getModifiers() == MouseEvent.BUTTON3 || ( e.getButton() ==
			// MouseEvent.BUTTON1 && e.getModifiersEx() ==
			// MouseEvent.CTRL_DOWN_MASK) )){

			AbstractTreeNode node = (AbstractTreeNode) jtree
					.getLastSelectedPathComponent();
			if (node != null) {
				DefaultPolicyTreePopupMenu p = new DefaultPolicyTreePopupMenu(
						this, node);

				p.show(jtree, e.getX(), e.getY());
				jtree.validate();
				jtree.repaint();
				jtree.treeDidChange();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
	 */
	public void mousePressed(MouseEvent e) {
		evaluatePopup(e);

		if (e.getSource() == jtree && e.getButton() == MouseEvent.BUTTON1) {
			log.debug("Preparing for DnD");

			this.firstMouseEvent = e;
			e.consume();
		}
	}

	public void keyReleased(KeyEvent e) {
		if (e.getSource() == search) {
			if (e.getKeyChar() == KeyEvent.VK_ESCAPE) {
				search.setText("");
				rmodel.setFilter(null);
			} else {
				String path = search.getText();
				if (path != null && path.length() > 0) {
					rmodel.setFilter(new ResourceTreePathFilter(path));
				}
			}
		}
	}

	public void keyTyped(KeyEvent e) {

		if (jtree.getSelectionPath() == null) {
			log.debug("nothing selected in the tree...");
			return;
		}

		if (rmodel.isEditable() && e.getKeyCode() == KeyEvent.VK_F2) {
			if (jtree.getSelectionPath() != null) {
				TreeNode node = (TreeNode) jtree.getSelectionPath()
						.getLastPathComponent();
				if (node.isInherited()) {

					int r = JOptionPane
							.showConfirmDialog(
									this,
									"You are about to edit an inherited node. Your changes will affect all resources, which inherit this element.",
									"Editing Abstraction",
									JOptionPane.YES_NO_OPTION);
					if (r == JOptionPane.YES_OPTION)
						jtree.startEditingAtPath(jtree.getSelectionPath());
				}
			}
		}

		log.debug("keyChar = " + ((int) e.getKeyChar()));

		if ((e.getKeyCode() == KeyEvent.VK_BACK_SPACE || e.getKeyChar() == 127 || e
				.getKeyCode() == KeyEvent.VK_DELETE)) {

			log.debug("DEL/BACKSPACE-Key hit!");
			rmodel.deleteNode((TreeNode) jtree.getSelectionPath()
					.getLastPathComponent());
		}
	}

	public void treeNodesChanged(TreeModelEvent arg0) {
		log.debug("treeNodesChanged: " + arg0);
		jtree.treeDidChange();
		jtree.revalidate();
	}

	public void treeNodesInserted(TreeModelEvent arg0) {
		log.debug("treeNodesInserted: " + arg0);

		if (jtree.isCollapsed(arg0.getTreePath()))
			jtree.expandPath(arg0.getTreePath());
		jtree.revalidate();
		jtree.treeDidChange();
	}

	public void treeNodesRemoved(TreeModelEvent arg0) {
		log.debug("treeNodesRemoved: " + arg0);
		jtree.treeDidChange();
		jtree.revalidate();
	}

	public void treeStructureChanged(TreeModelEvent arg0) {
		log.debug("treeStructureChanged: " + arg0);
		jtree.treeDidChange();
		jtree.revalidate();
	}

	public void actionPerformed(ActionEvent e, AbstractTreeNode node) {
		try {
			log.debug("RuleTreeModel.actionPerformed:: e: "
					+ e.getActionCommand() + ", node=" + node);

			if (AbstractTreeAction.TREE_INSERT_PARAMETER == e
					.getActionCommand()) {
				Parameter param = new Parameter();
				param.setName("p1");
				param.setRegexp(".*");
				param.setRequired(false);
				node.add(param);
				rmodel.notify(node);

				jtree.startEditingAtPath(new TreePath(PolicyTreeModel
						.getPath(param)));
			}

			if (AbstractTreeAction.TREE_INSERT_HEADER == e.getActionCommand()) {
				Header header = new Header();
				header.setName("h1");
				header.setRegexp(".{0,255}");
				header.setRequired(false);
				node.add(header);
				rmodel.notify(node);

				jtree.startEditingAtPath(new TreePath(PolicyTreeModel
						.getPath(header)));
			}

			if (AbstractTreeAction.TREE_INSERT_COOKIE == e.getActionCommand()) {
				Cookie cookie = new Cookie();
				cookie.setName("c1");
				cookie.setRegexp(".{0,255}");
				cookie.setRequired(false);
				node.add(cookie);
				rmodel.notify(node);

				jtree.startEditingAtPath(new TreePath(PolicyTreeModel
						.getPath(cookie)));
			}

			if (AbstractTreeAction.TREE_INSERT_RESOURCE == e.getActionCommand()) {

				log.debug("Inserting new location...");

				Resource res = new Resource("Loc" + Resource.LAST_ID++);
				node.add(res);
				rmodel.notify(node);

				if (node.getParent() != null)
					rmodel.notify(node.getParent());

				jtree.startEditingAtPath(new TreePath(PolicyTreeModel
						.getPath(res)));
			}

			if (AbstractTreeAction.TREE_INSERT_METHOD == e.getActionCommand()) {
				log.debug("Inserting method...");

				String newName = null;
				String[] names = new String[] { "GET", "POST", "HEAD",
						"OPTIONS", "PUT", "DELETE" };

				for (String name : names) {
					if (!this.containsMethodWithName(node.children(), name)) {
						newName = name;
						break;
					}
				}

				if (newName == null) {
					int i = 0;
					newName = "GET-" + i;
					while (this
							.containsMethodWithName(node.children(), newName)) {
						i++;
						newName = "GET-" + i;
					}
				}

				Method method = new Method(newName);
				node.add(method);
				rmodel.notify(node);

				jtree.startEditingAtPath(new TreePath(PolicyTreeModel
						.getPath(method)));
			}

			if (AbstractTreeAction.TREE_INSERT_CHECKTOKEN == e
					.getActionCommand()) {
				log.debug("Inserting check-token node...");
				CheckToken ct = new CheckToken();

				node.add(ct);
				rmodel.notify(node);

				jtree.startEditingAtPath(new TreePath(PolicyTreeModel
						.getPath(ct)));
			}

			if (AbstractTreeAction.TREE_EDIT_NODE == e.getActionCommand()) {
				jtree.startEditingAtPath(jtree.getSelectionPath());
			}

			if (AbstractTreeAction.TREE_INSERT_CREATETOKEN == e
					.getActionCommand()) {
				log.debug("Inserting create-token node...");
				CreateToken ct = new CreateToken();

				node.add(ct);
				rmodel.notify(node);

				jtree.startEditingAtPath(new TreePath(PolicyTreeModel
						.getPath(ct)));
			}

			if (AbstractTreeAction.TREE_DELETE_NODE == e.getActionCommand()) {
				TreeNode parent = node.getParent();
				if (parent == null)
					return;
				parent.remove(node);
				rmodel.notify(parent);
			}

			if (AbstractTreeAction.TREE_INSERT_LIMIT == e.getActionCommand()) {
				Limit limit = new Limit();
				limit.setBy("ip");
				limit.setLimit("10");

				node.add(limit);
				rmodel.notify(node);
				jtree.startEditingAtPath(new TreePath(PolicyTreeModel
						.getPath(limit)));
			}

			if (AbstractTreeAction.TREE_MERGE_BY_RE == e.getActionCommand()) {

				ResourceTreeMergeDialog d = new ResourceTreeMergeDialog(node);
				d.setVisible(true);
			}

			if (AbstractTreeAction.TREE_RESOURCE_WIZZARD == e
					.getActionCommand()) {

				log.error("Resource-Wizzard has not been implemented, yet!");
				//
				// TODO: Here we need to create a modal dialog running through
				// some steps
				// of resource creation and if the wizzard-dialog returns a node
				// (i.e.
				// the user has created a node) then add it to the mode.
				// If the wizzard returns null (i.e. the wizzard was cancelled
				// then
				// there is nothing todo :-))
				//
			}

			if (AbstractTreeAction.TREE_INSERT_FILTER_TAGS == e
					.getActionCommand()) {
				IncludeFilterNode inc = new IncludeFilterNode();
				node.add(inc);
				rmodel.notify(node);

				Object[] path = PolicyTreeModel.getPath(inc);
				this.jtree.startEditingAtPath(new TreePath(path));
			}

		} catch (Exception se) {
			se.printStackTrace();
		}
	}

	public boolean containsMethodWithName(Collection<TreeNode> nodes,
			String name) {

		for (TreeNode node : nodes) {

			if (node.getType() == TreeNode.METHOD_NODE) {

				Method m = (Method) node;
				if (m.getValue().equals(name))
					return true;
			}
		}

		return false;
	}

	public JTree getTreeComponent() {
		return jtree;
	}

	public TreeModel getTreeModel() {
		return rmodel;
	}

	public void addTreeSelectionListener(TreeSelectionListener tsl) {
		jtree.addTreeSelectionListener(tsl);
		externalListener.add(tsl);
	}

	public void removeTreeSelectionListener(TreeSelectionListener tsl) {
		jtree.removeTreeSelectionListener(tsl);
		externalListener.remove(tsl);
	}

	public void addResourceTreeSelectionListener(
			ResourceTreeSelectionListener rtsl) {
		rtsListener.add(rtsl);
	}

	public void removeResourceTreeSelectionListener(
			ResourceTreeSelectionListener rtsl) {
		rtsListener.remove(rtsl);
	}
}