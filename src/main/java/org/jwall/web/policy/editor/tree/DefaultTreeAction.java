package org.jwall.web.policy.editor.tree;

import java.awt.event.ActionEvent;

import org.jwall.web.policy.AbstractTreeNode;
import org.jwall.web.policy.TreeNode;


/**
 * 
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class DefaultTreeAction extends AbstractTreeAction {
	public final static long serialVersionUID = 435354353442342L;
	private TreeNodeListener listener;
	String[] menuPath = new String[0];
	
	
	/**
	 * 
	 * @param cmd
	 * @param tnl
	 */
	public DefaultTreeAction( String cmd, TreeNodeListener tnl ){
		super(cmd, null);
		listener = tnl;
		menuPath = null;
	}
	
	public DefaultTreeAction( String cmd, TreeNodeListener tnl, String menuPath ){
	    this( cmd, tnl );
	    this.menuPath = menuPath.split("/");
	}
	
	

	/**
	 * @see org.jwall.web.policy.editor.tree.AbstractTreeAction#showForNode(org.jwall.web.policy.AbstractTreeNode)
	 */
	public boolean showForNode( TreeNode node ){
	    
	    // headers are only checked regarding a method
	    //
	    if( TREE_INSERT_HEADER.equals( getCommand() ) )
	        return node != null && node.allowsChild( TreeNode.HEADER_NODE );
	    
	    // cookies are only added to methods
	    //
	    if( TREE_INSERT_COOKIE.equals( getCommand() ) )
	        return node != null && node.allowsChild( TreeNode.COOKIE_NODE );

	    // parameters can only be added to method-nodes or other parameters
	    //
        if( TREE_INSERT_PARAMETER.equals( getCommand() ) )
            return node != null && node.allowsChild( TreeNode.PARAMETER_NODE );
        
        // Methods can only be inserted into resources, resource-classes or the root-context
        //
        if( TREE_INSERT_METHOD.equals( getCommand() ) )
            return node != null && node.allowsChild( TreeNode.METHOD_NODE ); // || node.allowsChild( TreeNode.RESOURCE_CLASS ) || node.allowsChild( TreeNode.CONTEXT_NODE ) );
        
        // resources can only be added to other resources or the root-context
        //
        if( TREE_INSERT_RESOURCE.equals( getCommand() ) )
            return node != null && node.allowsChild( TreeNode.RESOURCE_NODE ); 

        if( TREE_INSERT_CREATETOKEN.equals( getCommand() ) )
            return node != null && node.allowsChild( TreeNode.CREATE_TOKEN_NODE );
        
        if( TREE_INSERT_CHECKTOKEN.equals( getCommand() ) )
            return node != null && node.allowsChild( TreeNode.CHECK_TOKEN_NODE );
        
        if( TREE_INSERT_FILTER_TAGS.equals( getCommand() ) )
            return node != null && node.allowsChild( TreeNode.INCLUDE_FILTER_NODE );
        
        if( TREE_INSERT_RESOURCE_CLASS.equals( getCommand() ) )
            return node == null || node.allowsChild( TreeNode.RESOURCE_CLASS );

        
        // the root-context and resource-classes nodes must not be deleted
        //
        if( TREE_DELETE_NODE.equals( getCommand() ) )
            return node != null;
        
        // since the resource-classes node is only an implicit node it cannot be deleted
        //
        if( TREE_EDIT_NODE.equals( getCommand() ) )
            return node != null;
        
        return true;
	}

	
	/**
	 * @see org.jwall.web.policy.editor.tree.AbstractTreeAction#enabledForNode(org.jwall.web.policy.AbstractTreeNode)
	 */
	public boolean enabledForNode( TreeNode node ){
		return true;
	}
	

	/**
	 * @see org.jwall.web.policy.editor.tree.AbstractTreeAction#getMenuPath()
	 */
	public String[] getMenuPath(){
	    return menuPath;
	}
	

	/**
	 * @see org.jwall.web.policy.editor.tree.AbstractTreeAction#actionPerformed(java.awt.event.ActionEvent, org.jwall.web.policy.AbstractTreeNode)
	 */
	@Override
	public void actionPerformed(ActionEvent evt, AbstractTreeNode node) {
	    listener.actionPerformed(evt, node);
	}
}