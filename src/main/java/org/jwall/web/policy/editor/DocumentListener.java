package org.jwall.web.policy.editor;

import java.io.File;


/**
 * 
 * This interface implements methods that are called whenever a document
 * is changed or saved.
 * 
 * @author Christian Bockermann <chris@jwall.org>
 *
 */
public interface DocumentListener
{
    /**
     * 
     * 
     * @param doc
     */
    public void documentChanged( File doc );
    
    
    /**
     * 
     * 
     * @param doc
     */
    public void documentSaved( File doc );
}
