package org.jwall.web.policy.editor.classes;

import org.jwall.app.list.ListFilter;
import org.jwall.web.policy.abstraction.ParameterType;

public class ParameterTypeListFilter
    implements ListFilter<ParameterType>
{
    String regex = null;
    

    public void setFilterExpression( String exp ){
        regex = exp;
    }

    public boolean matches( ParameterType object)
    {
        if( regex == null )
            return true;

        if( object.getName().matches( regex ) || object.getComment() != null && object.getComment().matches( regex ) )
            return true;
        
        if( object.getName().contains( regex ) || (object.getComment() != null && object.getComment().contains( regex ) ) )
            return true;
        
        return false;
    }
}
