package org.jwall.web.policy.editor.classes;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.logging.Logger;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TreeModelListener;

import org.jwall.web.filter.ids.IncludeFilterNode;
import org.jwall.web.policy.AbstractTreeNode;
import org.jwall.web.policy.Method;
import org.jwall.web.policy.Parameter;
import org.jwall.web.policy.Resource;
import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.WebPolicy;
import org.jwall.web.policy.abstraction.ResourceClass;
import org.jwall.web.policy.editor.AbstractPolicyTreeView;
import org.jwall.web.policy.editor.DefaultPolicyTreePopupMenu;
import org.jwall.web.policy.editor.tree.AbstractTreeAction;
import org.jwall.web.policy.editor.tree.DefaultTreeAction;
import org.jwall.web.policy.editor.tree.ResourceTreeMergeDialog;
import org.jwall.web.policy.editor.tree.TreeNodeListener;

/**
 * 
 * The class tree view represents a visual component that displays the
 * user-defined resource and parameter classes of a profile. Additionally it
 * allows for the modification of existing and creation of new classes within
 * the profile.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class ClassTreeView extends AbstractPolicyTreeView implements
		TreeNodeListener {
	private static final long serialVersionUID = -4300738605621105594L;
	Logger log = Logger.getLogger("ClassTreeView");

	/** The actual visualization component. */
	JTree ctree = new JTree();

	/** This is the model that is to be visualized. */
	ClassTreeModel treeModel;
	ClassTreeCellRenderer cellRenderer = new ClassTreeCellRenderer();

	/**
	 * This constructor will instantiate a new empty class tree view.
	 * 
	 */
	public ClassTreeView() {

		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_INSERT_RESOURCE_CLASS, this,
				"Insert/Resource Class"));
		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_INSERT_RESOURCE, this,
				"Insert/Resource"));
		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_INSERT_METHOD, this, "Insert/Method"));
		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_INSERT_PARAMETER, this,
				"Insert/Parameter"));
		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_INSERT_FILTER_TAGS, this,
				"Insert/Filters"));
		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_EDIT_NODE, this));
		registerTreeAction(new DefaultTreeAction(
				AbstractTreeAction.TREE_DELETE_NODE, this));
	}

	/**
	 * This constructor will create a new class tree view that initially
	 * displays the given class tree.
	 * 
	 * @param ctm
	 *            The class tree structure that is to be displayed.
	 */
	public ClassTreeView(ClassTreeModel ctm) {
		this();
		setTreeModel(ctm);
	}

	public void setProfile(WebPolicy profile) {
		setTreeModel(new ClassTreeModel(profile));
	}

	/**
	 * This method sets the class tree that is to be displayed within the tree
	 * view.
	 * 
	 * @param ctm
	 *            A class tree object.
	 */
	private void setTreeModel(ClassTreeModel ctm) {
		treeModel = ctm;

		ctree = new JTree(treeModel);
		ctree.setDragEnabled(false);
		// ctree.setFont( new Font("Monospaced", Font.PLAIN, 9 ) );
		ctree.putClientProperty("JTree.lineStyle", "Angled");
		ctree.setAlignmentX(Component.LEFT_ALIGNMENT);
		ctree.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));
		ctree.setCellRenderer(cellRenderer);
		ctree.setRowHeight(30);
		ctree.setEditable(true);
		// ctree.addTreeSelectionListener(this);
		ctree.addMouseListener(this);
		ctree.addKeyListener(this);
		ctree.setBorder(new EmptyBorder(5, 5, 5, 5));
		ctree.setCellEditor(new ClassTreeCellEditor(ctree, cellRenderer, ctm
				.getProfile()));
		ctree.setVisible(true);

		ctree.setRootVisible(true);

		removeAll();
		// setBorder( new EmptyBorder( 4, 4, 4, 4 ) );
		setBorder(null);
		JLabel l = new JLabel("Predefined Resources/Types");

		JPanel ctrl = new JPanel(new FlowLayout());
		ctrl.add(l);
		// add( ctrl , BorderLayout.NORTH );
		add(new JScrollPane(ctree), BorderLayout.CENTER);
		validate();
	}

	/**
	 * This method returns the class tree that is currently displayed by this
	 * view object.
	 * 
	 * @return The displayed class tree.
	 */
	public ClassTreeModel getTreeModel() {
		return treeModel;
	}

	public JTree getTreeComponent() {
		return ctree;
	}

	/**
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	public void mouseClicked(MouseEvent e) {
		// log.warning( e.toString() );
		if (e.getSource() == ctree) {
			log.finer("  Mouse-Click on ctree: " + e);
		}

		if (e.getButton() == MouseEvent.BUTTON3
				|| (e.getButton() == MouseEvent.BUTTON1 && e.getModifiers() == MouseEvent.CTRL_DOWN_MASK))
			log.finer("  Mouse-Click with button3!");

		if (e.getSource() == ctree
				&& (e.getButton() == MouseEvent.BUTTON3 || (e.getButton() == MouseEvent.BUTTON1 && e
						.getModifiersEx() == MouseEvent.CTRL_DOWN_MASK))) {
			log.warning(e.toString());

			AbstractTreeNode node = (AbstractTreeNode) ctree
					.getLastSelectedPathComponent();
			if (node == null)
				node = (AbstractTreeNode) this.treeModel.getRoot();

			DefaultPolicyTreePopupMenu p = new DefaultPolicyTreePopupMenu(this,
					node);
			p.show(ctree, e.getX(), e.getY());
			ctree.validate();
			ctree.repaint();
			ctree.treeDidChange();

		} else {
			// log.warning("Missing conditions!");
		}
	}

	public void keyTyped(KeyEvent e) {
		// log.warning(""+e);
		if (ctree.getSelectionPath() == null) {
			log.warning("nothing selected in the tree...");
			return;
		}

		if (ctree.isEditable() && e.getKeyCode() == KeyEvent.VK_F2) {
			ctree.startEditingAtPath(ctree.getSelectionPath());
		}

		log.finest("keyChar = " + ((int) e.getKeyChar()));

		if ((e.getKeyCode() == KeyEvent.VK_BACK_SPACE || e.getKeyChar() == 127 || e
				.getKeyCode() == KeyEvent.VK_DELETE)) {

			log.finest("DEL/BACKSPACE-Key hit!");
			treeModel.deleteNode((TreeNode) ctree.getSelectionPath()
					.getLastPathComponent());
		}
	}

	public void actionPerformed(ActionEvent e, AbstractTreeNode node) {
		try {
			log.warning("ClassTreeView.actionPerformed:: e: "
					+ e.getActionCommand() + ", node=" + node);

			if (AbstractTreeAction.TREE_INSERT_PARAMETER == e
					.getActionCommand()) {
				Parameter param = new Parameter();
				param.setName("p1");
				param.setRegexp(".*");
				param.setRequired(false);
				node.add(param);
				treeModel.notify(node);
			}

			if (AbstractTreeAction.TREE_INSERT_RESOURCE == e.getActionCommand()) {

				log.finest("Inserting new location...");

				Resource res = new Resource("Loc" + Resource.LAST_ID++);
				node.add(res);
				treeModel.notify(node);

				if (node.getParent() != null)
					treeModel.notify(node.getParent());
			}

			if (AbstractTreeAction.TREE_INSERT_RESOURCE_CLASS == e
					.getActionCommand()) {

				log.finest("Inserting new resource-class");
				ResourceClass rc = new ResourceClass("rc-0");
				node.add(rc);
				treeModel.getProfile().add(rc);
				treeModel.notify(node);

				if (node.getParent() != null)
					treeModel.notify(node.getParent());
			}

			if (AbstractTreeAction.TREE_INSERT_METHOD == e.getActionCommand()) {
				log.finest("Inserting method...");

				Method method = new Method("GET");
				for (TreeNode ch : node.children()) {

					if (ch instanceof Method) {

						Method other = (Method) ch;
						if (other.getValue() == "GET")
							method = new Method("POST");

					}

				}

				node.add(method);
				treeModel.notify(node);
			}

			if (AbstractTreeAction.TREE_INSERT_CHECKTOKEN == e
					.getActionCommand()) {
				treeModel.notify(node);
			}

			if (AbstractTreeAction.TREE_INSERT_CREATETOKEN == e
					.getActionCommand()) {
				treeModel.notify(node);
			}

			if (AbstractTreeAction.TREE_DELETE_NODE == e.getActionCommand()) {
				TreeNode parent = node.getParent();
				if (parent == null)
					return;
				parent.remove(node);
				treeModel.notify(parent);
			}

			if (AbstractTreeAction.TREE_EDIT_NODE == e.getActionCommand()) {
				if (ctree.isEditable())
					ctree.startEditingAtPath(ctree.getSelectionPath());
			}

			if (AbstractTreeAction.TREE_MERGE_BY_RE == e.getActionCommand()) {

				ResourceTreeMergeDialog d = new ResourceTreeMergeDialog(node);
				d.setVisible(true);
			}

			if (AbstractTreeAction.TREE_INSERT_FILTER_TAGS == e
					.getActionCommand()) {
				IncludeFilterNode include = new IncludeFilterNode();
				node.add(include);
				treeModel.notify(node);
			}

		} catch (Exception se) {
			se.printStackTrace();
		}
	}

	public void addTreeModelListener(TreeModelListener l) {
		treeModel.addTreeModelListener(l);
	}

	public void removeTreeModelListener(TreeModelListener l) {
		treeModel.removeTreeModelListener(l);
	}
}