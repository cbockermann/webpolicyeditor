package org.jwall.web.policy.editor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.util.Iterator;
import java.util.TreeSet;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;

import org.jwall.WebPolicyEditor;
import org.jwall.app.Action;
import org.jwall.app.Application;
import org.jwall.app.ui.DisplayDialog;
import org.jwall.app.ui.FileSelectionPanel;
import org.jwall.app.ui.View;
import org.jwall.web.policy.WebPolicy;
import org.jwall.web.policy.abstraction.ParameterType;
import org.jwall.web.policy.abstraction.ResourceClass;
import org.jwall.web.policy.compiler.PolicyCompilerImpl;
import org.jwall.web.policy.compiler.Templates;
import org.jwall.web.policy.compiler.ui.CompilerAction;
import org.jwall.web.policy.compiler.ui.CompilerPreferenceDialog;
import org.jwall.web.policy.editor.classes.ClassTreeView;
import org.jwall.web.policy.editor.classes.ParameterTypeListView;
import org.jwall.web.policy.editor.filter.FilterListView;
import org.jwall.web.policy.editor.tree.ResourceTreeView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * This class is basically just a composite view of several sub-components, e.g.
 * the policy-tree view, the parameter-type list and the resource-class panel.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class PolicyEditorView extends View implements ActionListener {
	/** The unique class ID */
	private static final long serialVersionUID = -6119658119541431548L;

	private static Logger log = LoggerFactory.getLogger(PolicyEditorView.class);
	File file;

	protected ResourceTreeView rtview;
	protected ClassTreeView ctview;
	protected ParameterTypeListView ptListView;
	protected FilterListView filterListView;
	protected WebPolicy profile;
	JSplitPane sp;
	double dividerLoc = 0.7;

	public PolicyEditorView(Application app) {
		super(app);
		setLayout(new BorderLayout());

		actions.add(new Action(this, Action.NEW_FILE, "File/New", 10.0d, true,
				true, KeyStroke.getKeyStroke(KeyEvent.VK_N, Toolkit
						.getDefaultToolkit().getMenuShortcutKeyMask())));
		actions.add(new Action(this, Action.LOAD_FILE, "File/Open", 20.0d,
				true, true, KeyStroke.getKeyStroke(KeyEvent.VK_O, Toolkit
						.getDefaultToolkit().getMenuShortcutKeyMask())));

		actions.add(new Action(this, Action.SAVE_FILE, "File/Save", 30.0d,
				true, false, KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit
						.getDefaultToolkit().getMenuShortcutKeyMask())));
		actions.add(new Action(this, Action.SAVE_AS_FILE, "File/Save As",
				40.0d, false, false));

		actions.add(new Action(this, CompilerAction.COMPILE_PROFILE,
				"Policy/Compile", 10.0d, true, file != null));

		rtview = new ResourceTreeView();
		ctview = new ClassTreeView();
		ptListView = new ParameterTypeListView();
		filterListView = new FilterListView();

		JTabbedPane tabs = new JTabbedPane();
		tabs.add("Parameter Types", ptListView);
		tabs.add("Filters", filterListView);

		Component right = tabs;
		Component left = rtview;

		JSplitPane fullleft = new JSplitPane(JSplitPane.VERTICAL_SPLIT, rtview,
				ctview);
		fullleft.setDividerSize(10);
		fullleft.setOneTouchExpandable(true);
		fullleft.setBorder(new EmptyBorder(2, 2, 0, 0));
		left = fullleft;
		fullleft.setDividerLocation(500);

		sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, left, right);

		sp.setBorder(null);
		sp.setDividerSize(10);

		add(sp, BorderLayout.CENTER);
		sp.setDividerLocation(700);
	}

	public void setVisible(boolean b) {
		super.setVisible(b);
	}

	/**
	 * This method opens the given file and reads the xml-tree within that file
	 * using SAX. The resulting DOM is then displayed in the main-window.
	 */
	public void loadRuleset() {
		try {
			if (file == null) {
				JFileChooser f = new JFileChooser();
				int r = f.showOpenDialog(WebPolicyEditor.getWindow());
				if (r == JFileChooser.APPROVE_OPTION) {
					file = f.getSelectedFile();
				} else
					return;
			}

			// log.fine("Loading ruleset from "+file.getAbsolutePath());

			getAction(CompilerAction.COMPILE_PROFILE).setEnabled(false);
			WebPolicyEditor.getWindow().setTitle(
					"PolicyEditor :: " + file.getName());

			profile = WebPolicy.load(file);
			getAction(Action.SAVE_AS_FILE).setEnabled(true);

			setProfile(profile);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void loadDefaultTypes() {
		ptListView.loadDefaultTypes();
	}

	/**
	 * 
	 * 
	 * @param root
	 */
	public void setProfile(WebPolicy profile) {
		// if( this.profile == null )
		// sp.setDividerLocation( 0.75d );

		this.profile = profile;

		if (rtview != null)
			rtview.setProfile(profile);

		if (ctview != null) {
			ctview.setProfile(profile);
			ctview.addTreeModelListener(rtview);
		}

		if (ptListView != null) {
			ptListView.setProfile(profile);

			if (profile.getParameterTypes() == null
					|| profile.getParameterTypes().isEmpty()) {
				ptListView.loadDefaultTypes();
			}
		}

		if (getAction(Action.SAVE_AS_FILE) != null)
			getAction(Action.SAVE_AS_FILE).setEnabled(true);

		if (getAction(CompilerAction.COMPILE_PROFILE) != null)
			getAction(CompilerAction.COMPILE_PROFILE).setEnabled(true);

		// dividerLoc = 0.7d;
		// sp.setDividerLocation( dividerLoc );

		revalidate();
	}

	/**
	 * This method writes the document that is currently loaded in the main-
	 * window into a XML-file.
	 */
	public void saveRuleset(File file) {
		try {
			if (file == null) {
				JFileChooser f = new JFileChooser();
				int r = f.showSaveDialog(this);
				if (r == JFileChooser.APPROVE_OPTION) {
					this.file = f.getSelectedFile();
				} else
					return;

			} else
				this.file = file;

			WebPolicy.save(profile, this.file);
			documentSaved();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void compileRuleset() {
		File target = null;

		if (file == null) {

			JFileChooser jfc = new JFileChooser();
			int r = jfc.showOpenDialog(this);
			if (r == JFileChooser.APPROVE_OPTION)
				target = jfc.getSelectedFile();
			else
				return;
		} else
			target = new File(file.getAbsolutePath().replaceFirst(".xml$",
					".rules"));

		try {

			CompilerPreferenceDialog d = new CompilerPreferenceDialog();
			d.setVisible(true);

			if (d.getReturnStatus() < 0)
				return;

			target.createNewFile();

			log.debug("Compiling rules to {}", target.getAbsolutePath());

			PolicyCompilerImpl pc = new PolicyCompilerImpl();
			pc.compile(profile, new FileWriter(target));

			JOptionPane.showMessageDialog(this,
					"The rules have been successfully compiled into file\n   "
							+ target.getAbsolutePath() + "\n");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	//
	//
	//

	public void documentChanged() {
		if (file != null)
			WebPolicyEditor.getWindow().setTitle(
					"ProfileEditor :: " + file.getName() + "*");

		getAction(Action.SAVE_FILE).setEnabled(true);
	}

	public void documentSaved() {
		WebPolicyEditor.getWindow().setTitle(
				"ProfileEditor :: " + file.getName());
		getAction(Action.SAVE_FILE).setEnabled(false);
	}

	public static void showDialog(URL url) {

		log.debug("Displaying url: {}", url);

		if (url == null)
			return;

		DisplayDialog d = new DisplayDialog(url, true);
		d.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		log.debug("ProfileEditorView: {}", e);

		if (CompilerAction.COMPILE_PROFILE.equals(e.getActionCommand())) {
			compileProfile();
		}

		if (Action.LOAD_FILE.equals(e.getActionCommand())) {
			loadProfile();
		}

		if (Action.SAVE_FILE.equals(e.getActionCommand())) {
			saveProfile(file);
		}

		if (Action.SAVE_AS_FILE.equals(e.getActionCommand())) {
			saveProfile(null);
		}

		if (Action.NEW_FILE.equals(e.getActionCommand())) {
			createNewProfile();
		}
	}

	public void setDividerLocation(double d) {
		sp.setDividerLocation(d);
		dividerLoc = d;
		this.validate();
	}

	public void createNewProfile() {
		WebPolicy p = new WebPolicy();

		URL url = WebPolicyEditor.class
				.getResource("/org/jwall/web/profile/standard-types.xml");
		if (url != null) {
			try {

				WebPolicy std = WebPolicy.load(url.openStream());
				if (std != null) {

					for (ResourceClass rc : std.getResourceClasses())
						p.add(rc);

					for (ParameterType pt : std.getParameterTypes())
						p.add(pt);
				}

			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}

		setProfile(p);
		documentChanged();
	}

	public void loadPolicy(File inFile) {
		this.file = inFile;
		try {
			if (file == null) {
				JFileChooser f = new JFileChooser();
				int r = f.showOpenDialog(WebPolicyEditor.getWindow());
				if (r == JFileChooser.APPROVE_OPTION) {
					file = f.getSelectedFile();
				} else
					return;
			}

			// if( ProfileEditor.DEBUG )
			log.info("Loading ruleset from {}", file.getAbsolutePath());

			getAction(CompilerAction.COMPILE_PROFILE).setEnabled(false);
			this.documentChanged();
			profile = WebPolicy.load(file);
			getAction(Action.SAVE_AS_FILE).setEnabled(true);

			setProfile(profile);

			String history = System
					.getProperty(FileSelectionPanel.PROPERTY_LAST_FILE_NAMES);
			if (history != null) {
				StringBuffer nh = new StringBuffer();
				TreeSet<String> fs = new TreeSet<String>();
				for (String f : history.split(":")) {
					fs.add(f);
				}
				fs.add(file.getAbsolutePath());

				Iterator<String> it = fs.iterator();
				while (it.hasNext()) {
					String f = it.next();
					nh.append(f);
					if (it.hasNext())
						nh.append(":");
				}

				System.setProperty(FileSelectionPanel.PROPERTY_LAST_FILE_NAMES,
						nh.toString());

				getApplication().saveProperties();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method opens the given file and reads the xml-tree within that file
	 * using SAX. The resulting DOM is then displayed in the main-window.
	 */
	public void loadProfile() {
		loadPolicy(null);
	}

	/**
	 * This method writes the document that is currently loaded in the main-
	 * window into a XML-file.
	 */
	public void saveProfile(File file) {
		try {
			if (file == null) {
				JFileChooser f = new JFileChooser();
				int r = f.showSaveDialog(WebPolicyEditor.getWindow());
				if (r == JFileChooser.APPROVE_OPTION) {
					this.file = f.getSelectedFile();
				} else
					return;

			} else
				this.file = file;

			log.debug("Saving profile to file: {}", this.file);
			WebPolicy.save(profile, this.file);
			documentSaved();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void compileProfile() {
		if (file == null) {
			JOptionPane.showMessageDialog(WebPolicyEditor.getWindow(),
					"You need to save the current profile first!");
			return;
		}

		File target = new File(file.getAbsolutePath().replaceFirst(".xml$",
				".rules"));

		try {
			if (file != null) {

				CompilerPreferenceDialog d = new CompilerPreferenceDialog();
				d.setVisible(true);

				if (d.getReturnStatus() < 0)
					return;
				else {

					String templateAlias = d.getSelectedTemplate();
					URL url = Templates.getTemplate(templateAlias);
					// URL url = ProfileCompilerImpl.class.getResource( template
					// );
					if (url == null) {
						JOptionPane
								.showMessageDialog(
										WebPolicyEditor.getWindow(),
										"An error occured! The selected template has not been found!",
										"Template not found!",
										JOptionPane.ERROR_MESSAGE);
						return;
					} else {
						System.setProperty(
								PolicyCompilerImpl.PROPERTY_COMPILER_XSLT_FILE,
								url.toString());
						if (d.rememberUserSelection()) {
							System.setProperty(
									"org.jwall.web.profile.compiler.use-template",
									templateAlias);
						}
					}
				}

				target.createNewFile();

				log.debug("Compiling rules to {}", target.getAbsolutePath());

				PolicyCompilerImpl pc = new PolicyCompilerImpl();
				pc.compile(profile, new FileWriter(target));

				JOptionPane.showMessageDialog(WebPolicyEditor.getWindow(),
						"The rules have been successfully compiled into file\n   "
								+ target.getAbsolutePath() + "\n");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public ClassTreeView getClassTreeView() {
		return ctview;
	}

	public ResourceTreeView getResourceTreeView() {
		return this.rtview;
	}

	public ParameterTypeListView getParameterTypeListView() {
		return this.ptListView;
	}

	public FilterListView getFilterListView() {
		return filterListView;
	}
}
