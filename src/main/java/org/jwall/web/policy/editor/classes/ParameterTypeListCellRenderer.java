package org.jwall.web.policy.editor.classes;

import java.awt.Color;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.border.EmptyBorder;

import org.jwall.WebPolicyEditor;
import org.jwall.web.policy.abstraction.ParameterType;

/**
 * 
 * 
 * @author Christian Bockermann <chris@jwall.org>
 */
public class ParameterTypeListCellRenderer
extends DefaultListCellRenderer
{
    public final static long serialVersionUID = 123L;
    public final static Color EVEN_COLOR = new Color( 240, 255, 255 );
    private int size = 100;

    public ParameterTypeListCellRenderer(){
        size = 100;
    }
    
    public ParameterTypeListCellRenderer( int sz ){
        size = sz;
    }
    
    
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        
        if( size < 0 ){
            setFont( getFont().deriveFont( 9.0f ) );
        } else {
            setFont( getFont().deriveFont( 11.0f ) );
            setBorder( new EmptyBorder( 4,4,4,4 ) );
        }

        if( ! isSelected && ! cellHasFocus ){
            
            if( index % 2 == 0 )
                setBackground( EVEN_COLOR );                
            
        }
        
        
        
        ParameterType p = (ParameterType) value;
        
        StringBuffer s = new StringBuffer("<html><b>"+p.getName()+"</b>");

        if( p.getId() != null )
            s.append( " [" + p.getId() + "]" );
        else
            s.append(" [missing]" );
        s.append("<br>");
        
        s.append("" + p.getRegexp() + "");

        s.append("</html>");
        setText( s.toString() );
        setIcon( WebPolicyEditor.getIcon(p) );

        if( p.getComment() != null )
            setToolTipText( p.getComment() );

        return this;
    }
}