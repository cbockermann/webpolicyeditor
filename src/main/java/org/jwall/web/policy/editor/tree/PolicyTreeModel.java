package org.jwall.web.policy.editor.tree;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.jwall.web.policy.Resource;
import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.WebPolicy;
import org.jwall.web.policy.abstraction.DefaultInheritanceModel;
import org.jwall.web.policy.abstraction.SuperClass;
import org.jwall.web.policy.editor.AbstractPolicyModel;
import org.jwall.web.policy.editor.DocumentListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * This class encapsulates the model for displaying the resources within the
 * tree-view. It will update the tree and the tree-view based upon model changes
 * and also keep track of changes due to inheritance or changes in inherited
 * nodes.
 * <p/>
 * This model does manage two separate view of the profile. The simple one is
 * the straight forward profile tree of resources, methods and parameters.
 * <p/>
 * In parallel the model will maintain a mapping of extendable nodes and their
 * &qquot;extended&quot; version. For displaying inherited properties, we need
 * to use the &quot;extended&quot; version of these nodes.
 * <p/>
 * One more contribution of this class is the back-reference mapping. This
 * mapping maintains the list of inheriting nodes for each extension. That way
 * it is possible to update the view of an extended node if its extension has
 * changed.
 * <p/>
 * <i> <b>TODO:</b> Currently the implementation is very simple and relies on an
 * &quot;on-the-fly&quot; extension of nodes that extend super classes. This
 * will obviously become slow on big profiles so the ideas of above need to be
 * implemented at some point... <i>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class PolicyTreeModel extends AbstractPolicyModel implements TreeModel {
	Logger log = LoggerFactory.getLogger(PolicyTreeModel.class);
	boolean editable = true;

	/** This class implements the inheritance model. */
	private DefaultInheritanceModel inherit;

	/**
	 * This map is used to keep track of the list of nodes which inherit a
	 * certain super class
	 */
	HashMap<SuperClass, LinkedList<TreeNode>> backref = new HashMap<SuperClass, LinkedList<TreeNode>>();

	/** This mapping is used to get the extended version of a tree node */
	HashMap<String, TreeNode> pathMap = new HashMap<String, TreeNode>();

	/** This filter allows for a partial search of the profile */
	TreeNodeFilter filter = new ResourceTreePathFilter();

	/**
	 * 
	 * This creates a new model of the given profile.
	 * 
	 * @param profile
	 *            The profile that is to be displayed.
	 * 
	 */
	public PolicyTreeModel(WebPolicy profile) {
		super(profile);

		inherit = new DefaultInheritanceModel(profile);
		listener = new LinkedList<DocumentListener>();
		mlistener = new LinkedList<TreeModelListener>();

		pathMap = new HashMap<String, TreeNode>();
		createPathMap();
	}

	/**
	 * This method returns the profile that this model is based on.
	 * 
	 * @return The profile that is the basis of this model.
	 */
	public WebPolicy getProfile() {
		return profile;
	}

	public void setFilter(TreeNodeFilter filter) {
		this.filter = filter;

		for (TreeModelListener l : mlistener)
			l.treeStructureChanged(new TreeModelEvent(profile.getContext(),
					getPath(profile.getContext())));
	}

	/**
	 * This method computes the list of children of the given tree node based
	 * upon the inheritance model of the profile. Thus all direct children of
	 * the node and all the inherited ones are returned in one list. The list
	 * does <i>not</i> contain inherited nodes which are overridden by local
	 * definitions.
	 * 
	 * @param node
	 * @return
	 */
	public LinkedList<TreeNode> getAllChildren(TreeNode node) {
		LinkedList<TreeNode> list = new LinkedList<TreeNode>(node.children());

		if (inherit != null && node.getType() == TreeNode.RESOURCE_NODE) {
			Resource r = (Resource) node;
			if (r.hasExtensions())
				list.addAll(inherit.getInheritedChildren(r));
		}

		if (filter != null) {

			Iterator<TreeNode> it = list.iterator();
			while (it.hasNext()) {
				TreeNode ch = it.next();
				if (!filter.matches(ch))
					it.remove();
			}
		}

		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.tree.TreeModel#getChild(java.lang.Object, int)
	 */
	public Object getChild(Object arg0, int arg1) {

		LinkedList<TreeNode> list = getAllChildren((TreeNode) arg0);
		return list.get(arg1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.tree.TreeModel#getChildCount(java.lang.Object)
	 */
	public int getChildCount(Object arg0) {
		return getAllChildren((TreeNode) arg0).size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.tree.TreeModel#getIndexOfChild(java.lang.Object,
	 * java.lang.Object)
	 */
	public int getIndexOfChild(Object arg0, Object arg1) {
		LinkedList<TreeNode> list = this.getAllChildren((TreeNode) arg0);
		TreeNode child = (TreeNode) arg1;
		return list.indexOf(child);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.tree.TreeModel#getRoot()
	 */
	public Object getRoot() {
		return getProfile().getContext();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.tree.TreeModel#isLeaf(java.lang.Object)
	 */
	public boolean isLeaf(Object arg0) {
		return getAllChildren((TreeNode) arg0).size() == 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.swing.tree.TreeModel#valueForPathChanged(javax.swing.tree.TreePath,
	 * java.lang.Object)
	 */
	public void valueForPathChanged(TreePath arg0, Object arg1) {
		log.debug("valueForPathChanged[ {} ] ~> {}", arg0, arg1);
		notify((TreeNode) arg1);
	}

	public void nodesChanged(SuperClass sc) {
		LinkedList<TreeNode> affected = backref.get(sc);
		for (TreeNode n : affected)
			notify(n);
	}

	public static Object[] getPath(TreeNode node) {

		LinkedList<TreeNode> nodes = new LinkedList<TreeNode>();

		TreeNode cur = node;
		while (cur != null) {
			nodes.addFirst(cur);
			cur = cur.getParent();
		}

		return nodes.toArray();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jwall.web.profile.editor.tree.AbstractProfileTreeModel#notify(org
	 * .jwall.web.profile.TreeNode)
	 */
	@Override
	public void notify(TreeNode node) {
		super.notify(node);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jwall.web.profile.editor.tree.AbstractProfileTreeModel#notifyChanged
	 * (org.jwall.web.profile.TreeNode)
	 */
	@Override
	public void notifyChanged(TreeNode node) {
		if (inherit != null && (node.getType() == TreeNode.RESOURCE_NODE))
			inherit.extend((Resource) node, profile);

		super.notifyChanged(node);
	}

	/**
	 * This method creates an internal mapping of path strings to nodes. It is
	 * intended to speed up the &quot;search by path&quot; for large profiles.
	 * Currently this is not used.
	 */
	private void createPathMap() {
		if (profile != null) {
			pathMap = new HashMap<String, TreeNode>();
			createPathMap(profile.getContext());
		}
		// log.debug("pathMap:");
		// for( String p : pathMap.keySet() ){
		// log.debug( "\t" + p );
		// }
	}

	/**
	 * This method will recursively traverse the resource tree and add all
	 * resources into the path map.
	 * 
	 * @param node
	 *            The node to process.
	 */
	private void createPathMap(TreeNode node) {

		if (node.getType() == TreeNode.CONTEXT_NODE
				|| node.getType() == TreeNode.RESOURCE_NODE) {

			pathMap.put(ResourceTreePathFilter.getPath(node), node);

			for (TreeNode ch : node.children())
				createPathMap(ch);
		}
	}
}