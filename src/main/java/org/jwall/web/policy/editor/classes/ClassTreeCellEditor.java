package org.jwall.web.policy.editor.classes;

import java.awt.Component;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.WebPolicy;
import org.jwall.web.policy.abstraction.ParameterType;
import org.jwall.web.policy.abstraction.ResourceClass;
import org.jwall.web.policy.editor.tree.ResourceTreeCellEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 
 * @author Christian Bockermann <chris@jwall.org>
 * 
 */
public class ClassTreeCellEditor extends ResourceTreeCellEditor {
	static Logger log = LoggerFactory.getLogger(ClassTreeCellEditor.class);

	public ClassTreeCellEditor(JTree tree, DefaultTreeCellRenderer renderer,
			WebPolicy rtm) {
		super(tree, renderer, rtm);
		setFont(new Font("Monospaced", Font.PLAIN, 13));
	}

	/**
	 * This method returns the value of the user-edition.
	 * 
	 * @return Object User specified value (TreeNode instance).
	 */
	public Object getCellEditorValue() {
		Object o = super.getCellEditorValue();
		if (cancelled)
			return o;

		if (node.getType() == TreeNode.RESOURCE_CLASS) {
			ResourceClass rc = (ResourceClass) node;
			rc.setName(name.getText());

			return rc;
		}

		if (node.getType() == TreeNode.PARAMETER_TYPE) {
			ParameterType param = (ParameterType) node;

			param.setName(name.getText());
			param.setRegexp(value.getText());

			return param;
		}

		return o;
	}

	public void createUIComponents(TreeNode node) {
		log.debug("Creating ui components for {}", node.getClass());

		if (node.getType() == TreeNode.RESOURCE_CLASS) {

			ResourceClass rc = (ResourceClass) node;
			JLabel l1 = new JLabel("Resource Class: ");
			l1.setFont(f);
			l1.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l1);

			String v = rc.getName();
			name = new JTextField(v, v.length() + 2);
			name.setFont(f);
			name.addKeyListener(this);
			name.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(name);

			return;
		}

		if (node.getType() == TreeNode.PARAMETER_TYPE) {

			ParameterType param = (ParameterType) node;
			JLabel l1 = new JLabel("Parameter Type: ");
			l1.setFont(f);
			l1.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l1);

			String v = param.getName();
			name = new JTextField(v, v.length() + 2);
			name.addKeyListener(this);
			name.setFont(f);
			name.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(name);

			JLabel l2 = new JLabel("Regexp: ");
			l2.setFont(f);
			l2.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l2);

			v = param.getRegexp();
			value = new JTextField(v, v.length() + 6);
			value.addKeyListener(this);
			value.setFont(f);
			value.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(value);

			return;
		}

		super.createUIComponents(node);
	}
}