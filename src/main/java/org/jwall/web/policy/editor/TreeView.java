package org.jwall.web.policy.editor;

import java.util.List;

import javax.swing.tree.TreeModel;

import org.jwall.web.policy.editor.tree.AbstractTreeAction;


/**
 * 
 * 
 * @author chris@jwall.org
 *
 */
public interface TreeView<E extends TreeModel>
{
    
    /**
     * This method allows for external objects to register new tree actions
     * to a the tree view.
     * 
     * @param act The action to register.
     */
    public void registerTreeAction( AbstractTreeAction act );
    
    
    /**
     * This method is used to unregister a tree action from the view.
     * 
     * @param act The action to be removed from the view.
     */
    public void unregisterTreeAction( AbstractTreeAction act );
    
    
    /**
     * This method returns the list of registered tree actions.
     * 
     * @return The list of tree actions registered to this view.
     */
    public List<AbstractTreeAction> getTreeActions();
    
    
    /**
     * 
     * 
     * @return
     */
    public E getTreeModel();
}