package org.jwall.web.policy.editor.tree;

import org.jwall.web.policy.TreeNode;

public interface ResourceTreeChangeListener
{

    
    public void treeNodeChanged( TreeNode node );
    
    public void treeNodeDeleted( TreeNode node );
}
