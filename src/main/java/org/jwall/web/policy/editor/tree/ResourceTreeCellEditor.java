package org.jwall.web.policy.editor.tree;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.EventObject;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.jwall.web.filter.ids.IncludeFilterNode;
import org.jwall.web.policy.Context;
import org.jwall.web.policy.Cookie;
import org.jwall.web.policy.Header;
import org.jwall.web.policy.Method;
import org.jwall.web.policy.Parameter;
import org.jwall.web.policy.Resource;
import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.WebPolicy;
import org.jwall.web.policy.editor.AbstractPolicyTreeCellEditor;
import org.jwall.web.policy.ext.CheckToken;
import org.jwall.web.policy.ext.CreateToken;
import org.jwall.web.policy.ext.Limit;

/**
 * 
 * 
 * @author Christian Bockermann <chris@jwall.org>
 * 
 */
public class ResourceTreeCellEditor extends AbstractPolicyTreeCellEditor {

	public ResourceTreeCellEditor(JTree tree, DefaultTreeCellRenderer renderer,
			WebPolicy rtm) {
		super(tree, renderer, rtm);
		setFont(new Font("Monospaced", Font.PLAIN, 13));
	}

	@Override
	public Object getCellEditorValue() {
		Object o = super.getCellEditorValue();
		if (cancelled)
			return o;

		if (node.getType() == TreeNode.PARAMETER_NODE) {
			Parameter param = (Parameter) node;
			param.setName(name.getText());
			param.setRegexp(value.getText());
			param.setRequired(required.isSelected());
			param.setScope("" + scope.getSelectedItem());

			return param;
		}

		if (nodeType == TreeNode.CONTEXT_NODE) {
			Context ctx = (Context) node;
			ctx.setBase(name.getText());
			return ctx;
		}

		if (nodeType == TreeNode.METHOD_NODE) {
			Method m = (Method) node;
			m.setValue(name.getText());
			return m;
		}

		if (nodeType == TreeNode.RESOURCE_NODE) {
			Resource res = (Resource) node;
			res.setName(name.getText());
			if (value.getText() != null)
				res.setExtensions(value.getText().trim());

			return res;
		}

		if (nodeType == TreeNode.INCLUDE_FILTER_NODE) {
			IncludeFilterNode inc = (IncludeFilterNode) node;
			if (name.getText() != null)
				inc.setTags(name.getText());

			return inc;
		}

		if (nodeType == TreeNode.CHECK_TOKEN_NODE) {
			CheckToken ct = (CheckToken) node;
			if (name.getText() != null)
				ct.setToken(name.getText());

			if (value.getText() != null)
				ct.setValue(value.getText());

			return ct;
		}

		if (nodeType == TreeNode.CREATE_TOKEN_NODE) {
			CreateToken ct = (CreateToken) node;
			if (name.getText() != null) {
				ct.setToken(name.getText());
			}
			ct.setLifeTime(Long.parseLong(value.getText()) * 1000);

			if (value2.getText() != null)
				ct.setValue(value2.getText());

			return ct;
		}

		if (nodeType == TreeNode.COOKIE_NODE) {
			Cookie c = (Cookie) node;
			c.setName(name.getText());
			c.setRegexp(value.getText());
			c.setRequired(required.isSelected());

			return c;
		}

		if (nodeType == TreeNode.HEADER_NODE) {
			Header h = (Header) node;
			h.setName(name.getText());
			h.setRegexp(value.getText());
			h.setRequired(required.isSelected());

			return h;
		}

		if (nodeType == TreeNode.LIMIT_NODE) {
			Limit limit = (Limit) node;
			limit.setLimit(name.getText());

			if (scope.getSelectedIndex() >= 0)
				limit.setBy(scope.getSelectedItem().toString());
			else
				limit.setBy("IP");

			return limit;
		}

		return o;
	}

	public void createUIComponents(TreeNode node) {
		if (node.getType() == TreeNode.METHOD_NODE) {
			Method m = (Method) node;
			JLabel l1 = new JLabel("Method(s): ");
			l1.setFont(f);
			l1.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l1);

			String v = m.getValue();
			name = new JTextField(v, v.length() + 2);
			name.addKeyListener(this);
			name.setFont(f);
			p.add(name);
		}

		if (node.getType() == TreeNode.HEADER_NODE) {

			Header param = (Header) node;
			JLabel l1 = new JLabel("Header: ");
			l1.setFont(f);
			l1.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l1);

			String v = param.getName();
			name = new JTextField(v, v.length() + 2);
			name.addKeyListener(this);
			name.setFont(f);
			name.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(name);

			JLabel l2 = new JLabel("Regexp: ");
			l2.setFont(f);
			l2.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l2);

			v = param.getRegexp();
			value = new JTextField(v, v.length() + 4);
			value.addKeyListener(this);
			value.setFont(f);
			value.setAlignmentY(Component.CENTER_ALIGNMENT);
			value.addFocusListener(this);
			p.add(value);

			required.setOpaque(true);
			required.setContentAreaFilled(false);
			required.addKeyListener(this);
			required.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(required);
			p.setFont(new Font("Monospaced", Font.PLAIN, 13));

		}

		if (node.getType() == TreeNode.COOKIE_NODE) {

			Cookie param = (Cookie) node;
			JLabel l1 = new JLabel("Cookie: ");
			l1.setFont(f);
			l1.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l1);

			String v = param.getName();
			name = new JTextField(v, v.length() + 2);
			name.addKeyListener(this);
			name.setFont(f);
			name.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(name);

			JLabel l2 = new JLabel("Regexp: ");
			l2.setFont(f);
			l2.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l2);

			v = param.getRegexp();
			value = new JTextField(v, v.length() + 4);
			value.addKeyListener(this);
			value.setFont(f);
			value.setAlignmentY(Component.CENTER_ALIGNMENT);
			value.addFocusListener(new FocusListener() {
				public void focusGained(FocusEvent evt) {
					value.selectAll();
				}

				public void focusLost(FocusEvent ev) {
					hideSelectionDialog();
				}
			});
			p.add(value);

			required.setOpaque(true);
			required.setContentAreaFilled(false);
			required.addKeyListener(this);
			required.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(required);
			p.setFont(new Font("Monospaced", Font.PLAIN, 13));

		}

		if (node.getType() == TreeNode.PARAMETER_NODE) {

			scopes = new String[] { "any", "body", "header" };
			this.scope = new JComboBox(scopes);
			Parameter param = (Parameter) node;
			JLabel l1 = new JLabel("Parameter: ");
			l1.setFont(f);
			l1.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l1);

			String v = param.getName();
			name = new JTextField(v, v.length() + 2);
			name.addKeyListener(this);
			name.setFont(f);
			name.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(name);

			JLabel l2 = new JLabel("Type: ");
			l2.setFont(f);
			l2.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l2);

			v = param.getRegexp();
			value = new JTextField(v, v.length() + 4);
			value.addKeyListener(this);
			value.addFocusListener(this);
			value.setFont(f);
			value.setAlignmentY(Component.CENTER_ALIGNMENT);
			value.addFocusListener(new FocusListener() {
				public void focusGained(FocusEvent evt) {
					value.selectAll();
				}

				public void focusLost(FocusEvent ev) {

				}
			});
			p.add(value);

			required.setOpaque(true);
			required.setContentAreaFilled(false);
			required.addKeyListener(this);
			required.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(required);
			p.setFont(new Font("Monospaced", Font.PLAIN, 13));

			JLabel l3 = new JLabel("Scope: ");
			l3.setFont(f);
			l3.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l3);
			p.add(scope);
			p.setAlignmentY(Component.CENTER_ALIGNMENT);
			scope.addKeyListener(this);
		}

		if (node.getType() == TreeNode.RESOURCE_NODE) {
			Resource r = (Resource) node;
			JLabel l1 = new JLabel("Name: ");
			l1.setAlignmentY(Component.CENTER_ALIGNMENT);
			l1.setFont(f);
			p.add(l1);
			p.setAlignmentY(Component.CENTER_ALIGNMENT);

			name = new JTextField(10);
			name.setAlignmentY(Component.CENTER_ALIGNMENT);
			name.addKeyListener(this);
			name.setText(r.getName());

			p.add(name);

			if (r.getExtensions().length > 0) {
				JLabel l2 = new JLabel("extends: ");
				l2.setAlignmentY(Component.CENTER_ALIGNMENT);
				l2.setFont(f);
				p.add(l2);
				value = new JTextField(10);
				StringBuffer b = new StringBuffer();
				for (int i = 0; i < r.getExtensions().length; i++) {
					b.append(r.getExtensions()[i]);
					if (i + 1 < r.getExtensions().length)
						b.append(",");
				}
				value.setText(b.toString());
				value.setAlignmentY(Component.CENTER_ALIGNMENT);
				value.addFocusListener(this);
				value.addKeyListener(this);
				p.add(value);
			}
		}

		if (node.getType() == TreeNode.CONTEXT_NODE) {
			Context ctx = (Context) node;
			JLabel l1 = new JLabel("Context: ");
			l1.setFont(f);
			l1.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l1);

			name = new JTextField(10);
			name.addKeyListener(this);
			name.setAlignmentY(Component.CENTER_ALIGNMENT);
			name.setToolTipText("This specifies the base-path of the profile. All resources within the "
					+ "profile are relative from this base.");
			if (ctx.getBase() != null)
				name.setText(ctx.getBase());
			p.add(name);
		}

		if (node.getType() == TreeNode.INCLUDE_FILTER_NODE) {
			IncludeFilterNode inc = (IncludeFilterNode) node;

			JLabel l1 = new JLabel("Include filters: ");
			l1.setFont(f);
			l1.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l1);

			name = new JTextField(10);
			name.addKeyListener(this);
			name.setAlignmentY(Component.CENTER_ALIGNMENT);
			name.setText(inc.getTagsAsString());

			p.add(name);
		}

		if (node.getType() == TreeNode.CHECK_TOKEN_NODE) {
			CheckToken ct = (CheckToken) node;
			JLabel l1 = new JLabel("Token: ");
			l1.setFont(f);
			l1.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l1);

			name = new JTextField(10);
			name.addKeyListener(this);
			name.setAlignmentY(Component.CENTER_ALIGNMENT);
			name.setText(ct.getToken());

			p.add(name);

			value = new JTextField(6);
			value.addKeyListener(this);
			value.setAlignmentY(Component.CENTER_ALIGNMENT);
			value.setText(ct.getValue());

			p.add(value);
		}

		if (node.getType() == TreeNode.CREATE_TOKEN_NODE) {
			CreateToken ct = (CreateToken) node;
			JLabel l1 = new JLabel("Token: ");
			l1.setFont(f);
			l1.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l1);

			name = new JTextField(10);
			name.addKeyListener(this);
			name.setAlignmentY(Component.CENTER_ALIGNMENT);
			name.setText(ct.getToken());

			p.add(name);

			JLabel l3 = new JLabel("value: ");
			l3.setFont(f);
			l3.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l3);

			value2 = new JTextField(6);
			value2.setAlignmentY(Component.CENTER_ALIGNMENT);
			value2.addKeyListener(this);
			if (ct.getValue() != null)
				value2.setText(ct.getValue());
			p.add(value2);

			JLabel l2 = new JLabel("expires: ");
			l2.setFont(f);
			l2.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l2);

			value = new JTextField(8);
			value.setAlignmentY(Component.CENTER_ALIGNMENT);
			value.setText(ct.getLifeTime().toString());

			p.add(value);
		}

		if (node.getType() == TreeNode.LIMIT_NODE) {

			scopes = new String[] { "ip", "session" };
			Limit limit = (Limit) node;
			JLabel l1 = new JLabel("Limit: ");
			l1.setFont(f);
			l1.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l1);

			name = new JTextField(8);
			name.addKeyListener(this);
			name.setAlignmentY(Component.CENTER_ALIGNMENT);
			name.setText(limit.getLimit());
			p.add(name);

			JLabel l2 = new JLabel("req/s");
			l2.setFont(f);
			l2.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(l2);

			scope = new JComboBox(new String[] { "IP", "Session" });
			scope.setAlignmentY(Component.CENTER_ALIGNMENT);
			p.add(scope);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.swing.tree.DefaultTreeCellEditor#isCellEditable(java.util.EventObject
	 * )
	 */
	@Override
	public boolean isCellEditable(EventObject event) {
		return super.isCellEditable(event);
	}

	public void hideSelectionDialog() {
		if (selectionDialog != null) {
			selectionDialog.setVisible(false);
		}
	}
}