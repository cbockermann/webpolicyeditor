package org.jwall.web.policy.editor.filter;

import java.util.LinkedList;

import org.jwall.app.list.AbstractListModel;
import org.jwall.web.filter.ids.FilterRule;

public class FilterListModel extends AbstractListModel<FilterRule> {
	protected static final long serialVersionUID = -8621768856333390850L;

	/**
	 * 
	 * @param p
	 */
	public FilterListModel() {
	}

	public LinkedList<FilterRule> getAllChildren(FilterRule root) {
		return new LinkedList<FilterRule>();
	}
}