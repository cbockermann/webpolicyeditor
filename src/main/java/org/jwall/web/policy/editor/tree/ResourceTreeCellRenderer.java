package org.jwall.web.policy.editor.tree;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.jwall.WebPolicyEditor;
import org.jwall.app.Application;
import org.jwall.tools.TimeFormat;
import org.jwall.web.filter.ids.IncludeFilterNode;
import org.jwall.web.policy.Context;
import org.jwall.web.policy.Cookie;
import org.jwall.web.policy.Header;
import org.jwall.web.policy.Method;
import org.jwall.web.policy.Parameter;
import org.jwall.web.policy.Resource;
import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.editor.AbstractPolicyTreeRenderer;
import org.jwall.web.policy.ext.CheckToken;
import org.jwall.web.policy.ext.CreateToken;
import org.jwall.web.policy.ext.Limit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 
 * @author Christian Bockermann <chris@jwall.org>
 */
public class ResourceTreeCellRenderer
extends DefaultTreeCellRenderer
{
    /** The unique class ID */
    private static final long serialVersionUID = -1193619641654063814L;

    static Logger logger = LoggerFactory.getLogger( ResourceTreeCellRenderer.class );

    public TreeNode dragSource = null;
    protected Font font = new Font( "Sansserif", Font.PLAIN, 11 );

    private final static Color BG_SELECT_INHERITED = new Color( 255, 198, 198 ); // 153, 204, 153 );
    private final static Color BG_NO_SELECT_INHERITED = new Color( 255, 255, 255 ); //new Color( 255, 225, 225 ); //204, 255, 204 );
    private final static Color BORDER_SELECT_INHERITED = new Color( 186, 164, 164 );

    private final static Color BORDER_SELECT_NON_INHERITED = new Color( 85, 115, 170 );
    private final static Color BG_SELECT_NON_INHERITED = new Color( 172, 210, 248 );
    private final static Color BG_NO_SELECT_NON_INHERITED = new Color( 255, 255, 255 );


    public void setFont( Font f ){
        this.font = f;
    }

    public Font getFont(){
        return font;
    }

    /* (non-Javadoc)
     * @see javax.swing.tree.DefaultTreeCellRenderer#getTreeCellRendererComponent(javax.swing.JTree, java.lang.Object, boolean, boolean, boolean, int, boolean)
     */
    @Override
    public Component getTreeCellRendererComponent(JTree arg0, Object arg1, boolean arg2, boolean arg3, boolean arg4, int arg5, boolean arg6)
    {
        logger.debug("Rendering tree: {}, node: {}", arg0, arg1);
        if( arg1 instanceof TreeNode )
            return getTreeCellRendererComponent( arg0, (TreeNode) arg1, arg2, arg3, arg4, arg5, arg6 );

        super.getTreeCellRendererComponent(arg0, arg1, arg2, arg3, arg4, arg5,arg6);
        return this;
    }  

    public void setDefaultColors(){
        setBackgroundSelectionColor( BG_SELECT_NON_INHERITED );
        setBackgroundNonSelectionColor( BG_NO_SELECT_NON_INHERITED );
        setBorderSelectionColor( BORDER_SELECT_NON_INHERITED );
    }

    public void setInheritanceColors(){
        setBackgroundSelectionColor( BG_SELECT_INHERITED );
        setBackgroundNonSelectionColor( BG_NO_SELECT_INHERITED );
        setBorderSelectionColor( BORDER_SELECT_INHERITED );
    }

    public Component getTreeCellRendererComponent(JTree arg0, TreeNode arg1, boolean arg2, boolean arg3, boolean arg4, int arg5, boolean arg6)
    {
        super.getTreeCellRendererComponent(arg0, arg1, arg2, arg3, arg4, arg5,arg6);
        setText( arg1.getName() );

        setDefaultColors();

        if( arg1.isInherited() ){
            setInheritanceColors();
        }



        if( arg1.getType() == TreeNode.METHOD_NODE ){
            Method m = (Method) arg1;

            StringBuffer s = new StringBuffer( "<html>" );
            if( m.isInherited() ){
                setInheritanceColors();
                s.append( "<i>" );

            }

            s.append( "Method " + m.getValue() );

            if( m.isInherited() )
                s.append( "</>" );

            s.append( "</html>" );

            setText( s.toString() );
            setIcon( WebPolicyEditor.getIcon(m) );
            return this;
        }

        if( arg1.getType() == TreeNode.META_INF_NODE ){
            setText( "Profile Information");
            setIcon( AbstractPolicyTreeRenderer.META_INF_ICON );
            return this;
        }

        if( arg1.getType() == TreeNode.META_AUTHOR_NODE ){
            setText( "<html>Author: <b>" + arg1.toString() +" </b></html>");
            setIcon( AbstractPolicyTreeRenderer.META_AUTHOR_ICON );
            return this;
        }

        if( arg1.getType() == TreeNode.META_VERSION_NODE ){
            setText( "<html>Version: <b>" + arg1.toString() + "</b></html>");
            setIcon( AbstractPolicyTreeRenderer.META_VERSION_ICON );
            return this;
        }

        if( arg1.getType() == TreeNode.POLICY_NODE ){
            setText("<html>Web Application Profile</html>");
            setIcon( AbstractPolicyTreeRenderer.PROFILE_ICON );
            return this;
        }

        if( arg1.getType() == TreeNode.SESSION_TRACKING_NODE ){
            setIcon( AbstractPolicyTreeRenderer.SESSION_TYPE_ICON );
            return this;
        }

        if( arg1.getType() == TreeNode.CONTEXT_NODE ){
            Context ctx = (Context) arg1;

            String path = ctx.getBase();
            if( path == null || path.equals( "" ) )
                path = "/";

            setText("<html><b>" + path + "</b></html>");
            setIcon( WebPolicyEditor.getIcon( ctx ) );
            setToolTipText("This node may be used for specifying the relative base-path of your profile.");
            return this;
        }

        if( arg1.getType() == TreeNode.RESOURCE_NODE ){
            Resource c = (Resource) arg1;

            String path = c.getName();
            if( path == null || path.equals( "" ) )
                path = "/";


            StringBuffer t = new StringBuffer( "<html>"); 

            if( c.isInherited() ){
                setInheritanceColors();
                t.append( "<i>" );
            }

            t.append("<b>" + path + "</b>" );

            if( c.hasExtensions() )
                t.append( ", extends: <b><i>" + c.getExtensionAsString() + "</i></b>" );

            if( c.isInherited() ){
                t.append( "</i>" );
            }

            t.append("</html>");

            setText( t.toString() );
            setIcon( AbstractPolicyTreeRenderer.getIcon( c ) );
            return this;
        }


        if( arg1.getType() == TreeNode.HEADER_NODE ){
            Header p = (Header) arg1;

            StringBuffer s = new StringBuffer("<html>");
            if( p.isInherited() ){
                setInheritanceColors();
                s.append("<i>");
            }

            s.append( "Header: <b>"+p.getName()+"</b>");

            s.append(", Regexp: <b>" + p.getRegexp() + "</b>");

            if( p.isRequired() )
                s.append(", Required: <b>"+p.isRequired()+"</b>");

            setText( s.toString() );
            setIcon( WebPolicyEditor.getIcon(p) );

            if( p.isInherited() )
                s.append("</i>");

            s.append("</html>");
            return this;
        }

        if( arg1.getType() == TreeNode.COOKIE_NODE ){
            Cookie c = (Cookie) arg1;

            StringBuffer s = new StringBuffer("<html>Cookie: <b>" + c.getName() + "</b>");
            s.append(", Regexp: <b>" + c.getRegexp() + "</b>" );
            if( c.isRequired() )
                s.append(", Required: <b>" + c.isRequired() + "</b>");

            s.append("</html>");
            setText( s.toString() );
            setIcon( WebPolicyEditor.getIcon( c ) );
            return this;
        }

        if( arg1.getType() == TreeNode.PARAMETER_NODE ){
            Parameter p = (Parameter) arg1;

            StringBuffer s = new StringBuffer("<html>");

            if( p.isInherited() ){
                setInheritanceColors();
                s.append( "<i>" );
            }

            s.append("Parameter: <b>"+p.getName()+"</b>");

            if( p.getRegexp().startsWith("$") )
                s.append(", Type: <b>" + p.getRegexp() + "</b>");
            else
                s.append(", Regexp: <b>" + p.getRegexp() + "</b>");

            if( p.isRequired() )
                s.append(", Required: <b>"+p.isRequired()+"</b>");

            if( p.getScope() != null  )
                s.append(", Scope: <b>"+p.getScope()+"</b>");

            if( p.isInherited() ){
                s.append( "</i>" );
            }
            s.append("</html>");
            setText( s.toString() );
            setIcon( WebPolicyEditor.getIcon(p) );

            return this;
        }

        if( arg1.getType() == TreeNode.INCLUDE_FILTER_NODE ){
            IncludeFilterNode include = (IncludeFilterNode) arg1;
            StringBuffer s = new StringBuffer("<html>Filter tags: ");

            for( int i = 0; i < include.getTags().length; i++ ){
                s.append( "<b>" + include.getTags()[i] + "</b>");
                if( i + 1 < include.getTags().length )
                    s.append(", ");
            }

            s.append("</html>");
            setText( s.toString() );
            setIcon( WebPolicyEditor.getIcon( arg1 ) );

            return this;
        }

        if( arg1.getType() == TreeNode.RESOURCE_CLASSES ){
            setText( "<html>Resource Classes</html>" );
            setIcon( WebPolicyEditor.getIcon( "org.jwall.web.policy.ResourceClasses" ) );
        }

        if( arg1.getType() == TreeNode.PARAMETER_CLASSES ){
            setText( "<html>Parameter Types</html>" );
            setIcon( WebPolicyEditor.getIcon( "org.jwall.web.policy.ParameterTypes" ) );
        }

        if( arg1.getType() == TreeNode.CHECK_TOKEN_NODE ){
            CheckToken ct = (CheckToken) arg1;

            StringBuffer s = new StringBuffer( "<html>Check Token: <i>" );

            s.append( ct.getToken() );
            s.append( "</i> " );

            if( ct.getValue() != null ){
                s.append( ", value: <i>" );
                s.append( ct.getValue() );
                s.append( "</i>" );
            }

            s.append( "</html>" );
            setText( s.toString() );
            setIcon( WebPolicyEditor.getIcon( "org.jwall.web.policy.CheckToken" ) );
        }

        if( arg1.getType() == TreeNode.CREATE_TOKEN_NODE ){
            CreateToken ct = (CreateToken) arg1;
            TimeFormat fmt = new TimeFormat();
            StringBuffer s = new StringBuffer( "<html>Create Token: <i>" );
            s.append( ct.getToken() );

            if( ct.getValue() != null ){
                s.append( "</i>, value: " );
                s.append( ct.getValue() );
            }
            
            s.append( "</i>, expires: <i>" );
            s.append( fmt.format( ct.getLifeTime().longValue() ) );
            s.append( "</i></html>" );
            setText( s.toString() );
            setIcon( WebPolicyEditor.getIcon( "org.jwall.web.policy.CreateToken" ) );
        }

        if( arg1.getType() == TreeNode.LIMIT_NODE ){

            Limit limit = (Limit) arg1;

            StringBuffer s = new StringBuffer( "<html>Limit: <i>" );
            s.append( limit.getLimit() + "</i> req/second" );
            
            if( limit.getBy() != null )
                s.append( " per <i>" + limit.getBy() + "</i>" );
            
            s.append( "</html>" );
            setText( s.toString() );
            setIcon( WebPolicyEditor.getIcon( "org.jwall.web.policy.Limit" ) );
        }

        return this;
    }

    public Dimension getPreferredSize(){
        Dimension d = super.getPreferredSize();
        d.height = 40;
        return d;
    }

    public void setIcon(String s){
        logger.debug( "Setting icon to: {}", s );
        this.setIcon( Application.getIcon(s) );
    }

    public void setIcon( Icon icon ){
        if( icon == null )
            return;

        super.setIcon( icon );
    }
}