package org.jwall.web.policy.editor;

/**
 * 
 * 
 * @author Christian Bockermann <chris@jwall.org>
 */
public class PolicyAction
    extends org.jwall.app.Action
{
	private static final long serialVersionUID = 4612088594002374379L;

	public final static String REMO_IMPORT = "org.jwall.web.policy.import_remo";
	
	
	/**
	 * 
	 * @param cmd
	 */
	public PolicyAction( String cmd, String menu, double idx ){
		super( cmd, menu, idx );
	}
}