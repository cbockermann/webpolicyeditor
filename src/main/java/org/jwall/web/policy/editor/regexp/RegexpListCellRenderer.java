package org.jwall.web.policy.editor.regexp;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;


/**
 * 
 * 
 * @author Christian Bockermann <chris@jwall.org>
 * @deprecated
 */
public class RegexpListCellRenderer extends DefaultListCellRenderer {

	public final static long serialVersionUID = 234324L;
	
	
	
	public RegexpListCellRenderer(){
	}



	@Override
	public Component getListCellRendererComponent(JList arg0, Object arg1, int arg2, boolean arg3, boolean arg4) {
		Component c = super.getListCellRendererComponent(arg0, arg1, arg2, arg3, arg4);
		return c;
	}
}
