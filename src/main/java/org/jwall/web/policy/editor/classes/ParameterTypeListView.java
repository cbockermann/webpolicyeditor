package org.jwall.web.policy.editor.classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.InputStream;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.swing.JOptionPane;

import org.jwall.WebPolicyEditor;
import org.jwall.app.list.AbstractListView;
import org.jwall.web.policy.AbstractTreeNode;
import org.jwall.web.policy.WebPolicy;
import org.jwall.web.policy.abstraction.ParameterType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * This class implements the list view for displaying a list of ParameterTypes.
 * It inherits from the AbstractListView and thus also provides a generic filter
 * mechanism.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class ParameterTypeListView extends AbstractListView<ParameterType>
		implements ActionListener {
	private static final long serialVersionUID = -4533024573466707379L;

	/* The logger for this class */
	static Logger log = LoggerFactory.getLogger(ParameterTypeListView.class);

	/** The default resource-path for the predefined standard types */
	public final static String DEFAULT_TYPES_URL = "/org/jwall/web/policy/standard-types.xml";

	/** A list of UI-actions that are exposed by this instance */
	List<ListAction> actions = new LinkedList<ListAction>();

	/**
	 * 
	 * This constructor creates a new parameter-type list which is loaded with
	 * types from a fixed default-URL. This file is looked up using the
	 * class-loader and should be contained within the class-path.
	 * 
	 */
	public ParameterTypeListView() {
		super(new ParameterTypeListModel(),
				new ParameterTypeTreeCellRenderer(),
				new ParameterTypeListFilter());

		actions.add(new ParameterTypeListAction(
				ParameterTypeListAction.LIST_ADD_PARAMETER_TYPE, lm, this));
		actions.add(new ParameterTypeListAction(
				ParameterTypeListAction.LIST_DELETE_PARAMETER_TYPE, lm, this));
		actions.add(new ParameterTypeListAction(
				ParameterTypeListAction.LIST_EDIT_PARAMETER_TYPE, lm, this));

		ParameterTypeTreeCellEditor editor = new ParameterTypeTreeCellEditor(
				list, new ParameterTypeTreeCellRenderer());
		list.setCellEditor(editor);
		list.setEditable(true);
		list.addKeyListener(this);
		list.addMouseListener(this);

		loadDefaultTypes();
	}

	/**
	 * This method does load the default types.
	 * 
	 */
	public void loadDefaultTypes() {

		log.info("Loading standard parameter-types.");

		try {

			URL url = WebPolicyEditor.class.getResource(DEFAULT_TYPES_URL);
			if (url != null)
				this.loadParameterTypes(url);

		} catch (Exception e) {
			log.error("Failed to load default parameter-types: {}",
					e.getMessage());
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,
					"The list of default-types could not be loaded. Please ensure that "
							+ DEFAULT_TYPES_URL
							+ " is contained within the classpath!", "Error",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * This method opens the given URL and expects the contents to provide a
	 * profile. This profile is treated as a container for providing a list of
	 * parameter-type definitions which are added to this list.
	 * 
	 * @param url
	 *            The URL to open.
	 * @throws Exception
	 */
	public void loadParameterTypes(URL url) throws Exception {

		log.debug("Loading parameter-types from: {}", url);

		if (url != null) {
			try {

				WebPolicy std = WebPolicy.load(url.openStream());

				if (std != null) {
					for (ParameterType pt : std.getParameterTypes())
						lm.add(pt);
				}

			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	/**
	 * This method adjusts the contents of the list-view to the types defined
	 * within the given profile.
	 * 
	 * @param p
	 *            The profile from which the types are to be fetched.
	 */
	public void setProfile(WebPolicy p) {
		ParameterTypeListModel model = (ParameterTypeListModel) lm;
		model.setProfile(p);
	}

	/**
	 * Performs the given UI-action.
	 * 
	 * @param e
	 *            The action-event triggering the action.
	 * @param node
	 *            The tree-node which has been selected while firing the action.
	 */
	public void actionPerformed(ActionEvent e, AbstractTreeNode node) {
		actionPerformed(e);
	}

	/**
     * 
     */
	public void actionPerformed(ActionEvent e) {

		if (ParameterTypeListAction.LIST_ADD_PARAMETER_TYPE == e
				.getActionCommand()) {

			String name = "newType";
			String newName = name;
			int i = 0;

			while (hasTypeCalled(newName)) {
				newName = name + "-" + i;
				i++;
			}

			ParameterType type = new ParameterType(newName, "\\d{1,10}");
			lm.add(type);
			list.treeDidChange();
		}

		if (ParameterTypeListAction.LIST_DELETE_PARAMETER_TYPE == e
				.getActionCommand()) {
			int rows[] = list.getSelectionRows();
			if (rows != null)
				lm.removeElementsAt(rows);
		}

	}

	/**
	 * This method checks whether a type-definition with the specified name
	 * already exists in the list-model
	 * 
	 * @param typeName
	 * @return
	 */
	public boolean hasTypeCalled(String typeName) {

		for (int i = 0; i < lm.getSize(); i++) {

			ParameterType type = (ParameterType) lm.getElementAt(i);
			if (type.getName().equals(typeName))
				return true;
		}

		return false;
	}

	/**
	 * @see org.jwall.app.list.AbstractListView#keyTyped(java.awt.event.KeyEvent)
	 */
	public void keyTyped(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_F2) {
			list.startEditingAtPath(list.getSelectionPath());
		} else
			super.keyTyped(e);
	}

	/**
	 * @see org.jwall.app.list.AbstractListView#mouseClicked(java.awt.event.MouseEvent)
	 */
	public void mousePressed(MouseEvent e) {
		evaluatePopup(e);
	}

	/**
	 * @see org.jwall.app.list.AbstractListView#mouseClicked(java.awt.event.MouseEvent)
	 */
	public void mouseClicked(MouseEvent e) {
		super.mouseClicked(e);
		evaluatePopup(e);
	}

	/**
	 * @see org.jwall.app.list.AbstractListView#mouseReleased(java.awt.event.MouseEvent)
	 */
	public void mouseReleased(MouseEvent e) {
		super.mouseReleased(e);
		evaluatePopup(e);
	}

	private void evaluatePopup(MouseEvent e) {
		if (e.getSource() == list && (e.isPopupTrigger())) {
			ParameterTypeListPopupMenu p = new ParameterTypeListPopupMenu(this);

			p.show(list, e.getX(), e.getY());
			list.validate();
			list.repaint();
			list.treeDidChange();
		}
	}

	/**
	 * @see org.jwall.web.policy.editor.TreeView#getTreeActions()
	 */
	public List<ListAction> getActions() {
		return actions;
	}

	/**
	 * <p>
	 * This method loads a set o additional types from the given inputstream.
	 * </p>
	 * <p>
	 * <b>Important:</b> The stream is assumed to provide a property-file.
	 * </p>
	 * 
	 * @param in
	 *            The inputstream for reading types from.
	 */
	public void loadParameterTypes(InputStream in) {
		try {

			Properties p = new Properties();
			p.load(in);

			for (Object key : p.keySet()) {
				this.lm.add(new ParameterType(key.toString(), p.getProperty(key
						.toString())));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}