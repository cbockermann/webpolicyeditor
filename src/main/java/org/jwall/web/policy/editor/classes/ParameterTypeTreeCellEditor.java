package org.jwall.web.policy.editor.classes;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellEditor;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.jwall.WebPolicyEditor;
import org.jwall.app.ui.SelectionDialog;
import org.jwall.app.ui.SelectionListener;
import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.abstraction.ParameterType;
import org.jwall.web.policy.editor.AbstractPolicyTreeRenderer;
import org.jwall.web.policy.editor.tree.AbstractTreeAction;

public class ParameterTypeTreeCellEditor
    extends DefaultTreeCellEditor
    implements KeyListener, FocusListener, SelectionListener
{
    public final static String[] PARAMETER_SCOPES = {
        "header",
        "body"
    };

    int nodeType = 0;

    JTextField name = new JTextField( "", 10 );
    JTextField value = new JTextField( "", 15 );
    JCheckBox required = new JCheckBox( "required" );
    JTextField score = new JTextField( "", 5 );
    
    JComboBox scope = new JComboBox( PARAMETER_SCOPES );
    TreeNode node = null;

    private boolean cancelled = false;
    JTree jtree;
    String oldValue = "";

    public ParameterTypeTreeCellEditor( JTree tree, DefaultTreeCellRenderer renderer ){
        super( tree, renderer );
        jtree = tree;
        jtree.addKeyListener( this );
        setFont(new Font("Monospaced", Font.PLAIN, 13));
    }


    @Override
    public Object getCellEditorValue() {
        Object o = super.getCellEditorValue();
        if( cancelled )
            return o;

        if( node.getType() == TreeNode.PARAMETER_TYPE ){
            ParameterType param = (ParameterType) node;
            param.setName( name.getText() );
            param.setRegexp( value.getText() );

            return param;
        }

        return o;
    }

    /* (non-Javadoc)
     * @see javax.swing.tree.DefaultTreeCellEditor#getTreeCellEditorComponent(javax.swing.JTree, java.lang.Object, boolean, boolean, boolean, int)
     */
    @Override
    public Component getTreeCellEditorComponent(JTree arg0, Object arg1, boolean arg2, boolean arg3, boolean arg4, int arg5)
    {
        Component c = super.getTreeCellEditorComponent(arg0, arg1, arg2, arg3, arg4, arg5);
        cancelled = false;

        if( arg1 instanceof TreeNode ){
            node = (TreeNode) arg1;
            Font f = new Font("Sansserif", Font.PLAIN, 11);

            JPanel p = new JPanel( new FlowLayout() );
            p.setBackground( Color.LIGHT_GRAY );
            p.setBorder( BorderFactory.createLineBorder( Color.DARK_GRAY, 1 ) );
            p.add( new JLabel( AbstractPolicyTreeRenderer.getIcon( node ) ) );
            nodeType = node.getType();
            
            if( node.getType() == TreeNode.PARAMETER_TYPE ){
                
                GridLayout g = new GridLayout( 2, 1 );
                g.setHgap( 0 );
                g.setVgap( 0 );
                JPanel up = new JPanel( new BorderLayout() );
                up.setOpaque( false );
                up.setBorder( null );
                JPanel up1 = new JPanel( new FlowLayout( FlowLayout.LEFT ) );
                up1.setOpaque( false );
                up1.setBorder( null );
                
                ParameterType param = (ParameterType) node;
                JLabel l1 = new JLabel("Name: ");
                l1.setFont(f);
                l1.setAlignmentY( Component.CENTER_ALIGNMENT );
                //p.add( l1 );

                String v = param.getName();
                name = new JTextField( v, v.length() + 2 );
                name.addKeyListener( this );
                name.setFont( f );
                name.setAlignmentY( Component.CENTER_ALIGNMENT );
                name.setAlignmentX( Component.LEFT_ALIGNMENT );
                //p.add( name );

                up1.add( l1 );
                up1.add( name );
                up.add( up1, BorderLayout.NORTH );
                
                JLabel l2 = new JLabel("Type: ");
                l2.setFont(f);
                l2.setAlignmentY( Component.CENTER_ALIGNMENT );
                //p.add( l2 );
                JPanel up2 = new JPanel( new FlowLayout( FlowLayout.LEFT ) );
                up2.setBorder( null );
                up2.add( l2 );
                
                v = param.getRegexp();
                if( v == null )
                    v = "";
                value = new JTextField( v, v.trim().length() + 2 );
                value.addKeyListener( this );
                value.addFocusListener( this );
                value.setFont(f);
                value.setAlignmentY( Component.CENTER_ALIGNMENT );
                value.setAlignmentX( Component.LEFT_ALIGNMENT );
                value.addFocusListener( new FocusListener(){
                    public void focusGained( FocusEvent evt ){
                        value.selectAll();
                    }
                    public void focusLost( FocusEvent ev ){
                    }
                });
                up2.add( value );
                up2.setOpaque( false );
                
                up.add( up2 );
                p.add( up, BorderLayout.SOUTH );
            }


            JPanel b = new JPanel();
            b.setAlignmentY( Component.CENTER_ALIGNMENT );
            //b.setLayout( new GridLayout( 2 , 1) );
            b.setOpaque( true );
            b.setBackground( Color.LIGHT_GRAY );

            JButton cancel = new JButton();
            cancel.setBorder(null);
            cancel.setBackground( Color.WHITE );
            cancel.setOpaque( true );
            cancel.setSize( new Dimension(10,10) );
            cancel.setActionCommand( AbstractTreeAction.TREE_STOP_EDITING );
            cancel.setIcon( AbstractPolicyTreeRenderer.getIcon( AbstractTreeAction.TREE_CANCEL_EDITING ) );
            cancel.setContentAreaFilled( false );
            cancel.setAlignmentY( Component.CENTER_ALIGNMENT );
            cancel.addActionListener( new ActionListener(){
                public void actionPerformed( ActionEvent e){
                    cancelCellEditing();
                }
            }
            );

            JButton stop = new JButton();
            stop.setBorder(null);
            stop.setBorderPainted( false );
            stop.setOpaque(true);
            stop.setSize( new Dimension(10,10) );
            stop.setBackground( Color.WHITE );
            stop.setContentAreaFilled( false );
            stop.setIcon( AbstractPolicyTreeRenderer.getIcon( AbstractTreeAction.TREE_STOP_EDITING ) );
            stop.setAlignmentY( Component.CENTER_ALIGNMENT );
            stop.addActionListener( new ActionListener(){
                public void actionPerformed( ActionEvent e ){
                    stopCellEditing();
                }
            });

            b.add( stop );
            b.add( cancel );
            p.add( b );

            return p;
        }
        c.addKeyListener( this );
        return c;
    }

    public void keyPressed(KeyEvent arg0) {
        if( arg0.getKeyCode() == KeyEvent.VK_ESCAPE )
            cancelled = true;
    }

    public void keyReleased(KeyEvent arg0) {
    }



    @Override
    public void cancelCellEditing() {
        cancelled = true;
        super.cancelCellEditing();
    }

    @Override
    public boolean stopCellEditing() {
        return super.stopCellEditing();
    }



    public void keyTyped(KeyEvent arg0) {
        if( arg0.getKeyCode() == KeyEvent.VK_ESCAPE ){
            cancelled = true;
            this.cancelCellEditing();
        }
        
        if( arg0.getKeyCode() == KeyEvent.VK_F2 ){
            jtree.startEditingAtPath( jtree.getSelectionPath() );
        }
        
        if( arg0.getKeyChar() == KeyEvent.VK_ENTER )
            this.stopCellEditing();


        if( node == null || node.getType() != TreeNode.PARAMETER_NODE )
            return;

        oldValue = value.getText();
    }

    public void selectionFinished( Object selected, SelectionDialog dialog ){
        if( selected != null && selected instanceof ParameterType ){
            ParameterType pt = (ParameterType) selected;
            if( !cancelled)
                value.setText( "${" + pt.getName() + "}" );
            else
                value.setText( oldValue );
        } else 
            value.setText( oldValue );
        
        if( dialog != null )
            dialog.finish();
    }

    
    public void selectionChanged( Object selected, SelectionDialog dialog ){
    }
    

    /* (non-Javadoc)
     * @see java.awt.event.FocusListener#focusGained(java.awt.event.FocusEvent)
     */
    public void focusLost(FocusEvent e)
    {
        WebPolicyEditor.getWindow().getStatusBar().setMessage( "" );
    }


    /* (non-Javadoc)
     * @see java.awt.event.FocusListener#focusLost(java.awt.event.FocusEvent)
     */
    public void focusGained(FocusEvent e)
    {
        if( e.getSource() == value ){
            value.selectAll();
        }
    }
}