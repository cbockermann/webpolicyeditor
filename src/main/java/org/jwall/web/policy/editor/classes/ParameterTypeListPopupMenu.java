package org.jwall.web.policy.editor.classes;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.LinkedList;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jwall.WebPolicyEditor;
import org.jwall.web.policy.AbstractTreeNode;
import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.editor.tree.AbstractTreeAction;


/**
 * 
 * This class provides an abstract implementation of a popup menu that may
 * be shown for a tree. The menu that is created will usually be empty and
 * inheriting classes are responsible for adding actions that will appear
 * in the menu.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class ParameterTypeListPopupMenu
extends JPopupMenu
implements ActionListener
{
    private static final long serialVersionUID = -5825858291814789351L;

    protected static String[] DEFAULT_MENUES = {
    };

    protected LinkedList<ListAction> treeActions;
    protected AbstractTreeNode node;
    protected HashMap<String,JMenu> rootItems = new HashMap<String,JMenu>();


    public ParameterTypeListPopupMenu( ParameterTypeListView view ){
        treeActions = new LinkedList<ListAction>();

        initTopMenues();

        for( ListAction ta : view.getActions() )
            add( ta );
    }

    
    public ParameterTypeListPopupMenu( ParameterTypeListView view, AbstractTreeNode node ){
        this( view );
        this.node = node;
    }
    

    protected void initTopMenues(){

        for( String name : DEFAULT_MENUES ){
            JMenu nm = new JMenu( name );
            nm.setIcon( WebPolicyEditor.getIcon( AbstractTreeAction.TREE_INSERT_NODE ) );
            rootItems.put( name, nm );
            add( nm );
        }
    }

    public TreeNode getSelectedNode(){
        return node;
    }


    public void add( ListAction ta ){
        treeActions.add( ta );

        JMenuItem ma = new JMenuItem( ta.getCommand() );
        ma.setIcon( WebPolicyEditor.getIcon( ta ) );
        ma.setActionCommand( ta.getCommand() );
        ma.setText( WebPolicyEditor.getMessage( ta.getCommand() ) );
        ma.addActionListener( this );
        ma.setEnabled( ta.enabledForNode( node ) );

        if( ta.getMenuPath() == null || ta.getMenuPath().length == 0 ) {
            super.add( ma );
            //rootItems.put( ProfileEditor.getMessage( ta.getCommand() ), ma );
        } else {
            JMenu menu = getMenu( ta.getMenuPath() );
            //if( menu != null )
            menu.add( ma );
            //else {
            //    super.add( ma );
            //    rootItems.put( ProfileEditor.getMessage( ta.getCommand() ), ma );
            //}
        }
    }


    private JMenu getMenu( String[] path ){

        JMenu cur = rootItems.get( path[0] );
        if( cur == null ){
            JMenu nm = new JMenu( path[0] );
            nm.setIcon( WebPolicyEditor.getIcon( AbstractTreeAction.TREE_INSERT_NODE ) );
            rootItems.put( path[0], nm );
            add( nm );
            cur = nm;
        }

        for( int i = 1; i < path.length; i++ ){

            for(int j = 0; j < cur.getComponentCount(); j++ ){

                Component c = getComponent(j);
                if( c instanceof JMenu ){

                    JMenu mi = (JMenu) c;
                    if( mi.getText().equals( path[i] ) ){
                        cur = mi;
                    }
                }
            }

            if( cur !=  null ){
                JMenu nmi = new JMenu( path[i] );
                nmi.setIcon( WebPolicyEditor.getIcon( AbstractTreeAction.TREE_INSERT_NODE ) );
                cur.add( nmi );
                cur = nmi;
            }
        }

        return cur;
    }


    /**
     * This is currently not supported. Dynamic Java menus are a bitch :-/
     * 
     * @param a
     */
    public void remove( AbstractTreeAction a ){
    }

    /**
     * 
     * This method creates a new menu-item for the given action command. It will
     * try to lookup the icon and set the label to the appropriate language (TODO).
     * 
     * @param action The action command that this item should call.
     * @param listener The action listener that is to be called.
     * 
     */
    public JMenuItem createMenuItem( String action, ActionListener listener ){
        JMenuItem m31 = new JMenuItem( WebPolicyEditor.getMessage( action ) );
        m31.setActionCommand( action );
        m31.setIcon( WebPolicyEditor.getIcon( action ) );
        m31.addActionListener( listener );    

        return m31;
    }

    public void actionPerformed(ActionEvent e)
    {
        for( ListAction act: treeActions ){
            if( act.handles( e ) ){
                act.actionPerformed( e  );
                return;
            }
        }
    }
}