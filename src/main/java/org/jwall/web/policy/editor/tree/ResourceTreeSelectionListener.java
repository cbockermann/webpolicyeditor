package org.jwall.web.policy.editor.tree;

import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.WebPolicy;

public interface ResourceTreeSelectionListener {
	public void treeNodeSelected(TreeNode node, WebPolicy profile);
}
