package org.jwall.web.policy.editor.classes;

import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.jwall.web.policy.AbstractTreeNode;
import org.jwall.web.policy.Resource;
import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.WebPolicy;
import org.jwall.web.policy.abstraction.ResourceClass;
import org.jwall.web.policy.editor.AbstractPolicyModel;
import org.jwall.web.policy.editor.PolicyModelListener;
import org.jwall.web.policy.editor.tree.AbstractTreeAction;
import org.jwall.web.policy.editor.tree.TreeNodeListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * This class implements the view-model of the user-defined class and type
 * declarations provided by a profile. It represents a tree with two children of
 * the root-node that are resource-classes and parameter-types.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org
 * 
 */
public class ClassTreeModel extends AbstractPolicyModel implements TreeModel,
		TreeNodeListener, PolicyModelListener {
	private static Logger log = LoggerFactory.getLogger(ClassTreeModel.class);
	ClassTree root;

	/**
	 * This constructor creates a new instance based upon the given profile.
	 * 
	 * @param profile
	 *            The profile which holds the classes.
	 */
	public ClassTreeModel(WebPolicy profile) {
		super(profile);

		root = new ClassTree("Classes", TreeNode.RESOURCE_CLASSES);

		try {
			for (ResourceClass rc : profile.getResourceClasses())
				root.add(rc);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Object getChild(Object parent, int index) {
		log.debug("getChild( {}, {} )", parent, index);

		if (parent instanceof TreeNode) {
			TreeNode p = (TreeNode) parent;
			return p.getChildAt(index);
		}

		return null;
	}

	public int getChildCount(Object parent) {

		if (parent instanceof TreeNode) {

			TreeNode node = (TreeNode) parent;
			return node.children().size();
		}

		return 0;
	}

	public int getIndexOfChild(Object parent, Object child) {
		if (parent instanceof TreeNode) {

			TreeNode node = (TreeNode) parent;
			return node.getIndex((TreeNode) child);
		}

		return 0;
	}

	public Object getRoot() {
		log.debug("getRoot()");
		return root;
	}

	public boolean isLeaf(Object node) {
		log.debug("isLeaf( {} )", node);

		if (node instanceof TreeNode) {
			TreeNode n = (TreeNode) node;

			return n.children().size() == 0;
		}

		return true;
	}

	public void valueForPathChanged(TreePath path, Object newValue) {
		log.debug("ClassTreeModel::valueForPathChanged[ {}�] ~> {}", path,
				newValue);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jwall.web.profile.editor.tree.TreeNodeListener#actionPerformed(java
	 * .awt.event.ActionEvent, org.jwall.web.profile.TreeNode)
	 */
	public void actionPerformed(ActionEvent e, AbstractTreeNode node) {
		log.debug("ClassTreeModel.actionPerformed( ActionEvent, TreeNode)");

		if (e.getActionCommand() == AbstractTreeAction.TREE_INSERT_RESOURCE) {
			Resource res = new Resource("class0");
			insertNode(node, res);

			if (node.getParent() != null)
				notify(node.getParent());

			for (TreeModelListener l : mlistener)
				l.treeStructureChanged(new TreeModelEvent(node,
						new Object[] { this.getRoot() }));

			return;
		}
	}

	public void nodeChanged(TreeNode node) {

	}

	class ClassTree extends AbstractTreeNode {
		/**
         * 
         */
		private static final long serialVersionUID = 3294031434035328754L;
		String name;
		int type;

		public ClassTree(String name, int t) {
			this.name = name;
			type = t;
		}

		public String getName() {
			return name;
		}

		@Override
		public TreeNode copy() {
			return this;
		}

		@Override
		public Map<String, String> getAttributes() {
			return new HashMap<String, String>();
		}

		@Override
		public int getType() {
			return TreeNode.RESOURCE_CLASSES;
		}

		public boolean allowsChild(int type) {
			return type == TreeNode.RESOURCE_CLASS;
		}
	}
}