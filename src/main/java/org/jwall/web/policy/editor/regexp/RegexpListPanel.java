package org.jwall.web.policy.editor.regexp;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jwall.WebPolicyEditor;
import org.jwall.web.policy.abstraction.ParameterType;

/**
 * 
 * This panel is used to display an manage the list of regular expressions.
 * Expressions can be added, removed and re-ordered.
 * 
 * @author Christian Bockermann <chris@jwall.org>
 * @deprecated
 */
public class RegexpListPanel extends JPanel 
implements ActionListener
{
	public final static String REGEXP_ADD = "org.jwall.web.policy.editor.regexp.list_add";
	public final static String REGEXP_DEL = "org.jwall.web.policy.editor.regexp.list_del";
	public final static String REGEXP_UP = "org.jwall.web.policy.editor.regexp.list_up";
	public final static String REGEXP_DOWN = "org.jwall.web.policy.editor.regexp.list_down";
	
	public final static long serialVersionUID = 4535346L;
	JList jlist;
	DefaultListModel lmodel;
	
	public RegexpListPanel( DefaultListModel list ){
		setLayout( new BorderLayout() );
		lmodel = list;
		jlist = new JList( lmodel );
		
		
		add( new JScrollPane( jlist ) , BorderLayout.CENTER );
		
		
		JPanel buttons = new JPanel();
		
		JButton add = new JButton( WebPolicyEditor.getIcon("editor/icons/16x16/document-new.png") );
		add.setActionCommand( REGEXP_ADD );
		add.addActionListener( this );
		
		buttons.add( add );
		
		JButton up = new JButton( WebPolicyEditor.getIcon("editor/icons/16x16/view-sort-ascending.png"));
		up.setActionCommand( REGEXP_UP );
		up.addActionListener(this);
		
		buttons.add( up );
		
		JButton down = new JButton( WebPolicyEditor.getIcon("editor/icons/16x16/view-sort-descending.png") );
		down.setActionCommand( REGEXP_DOWN );
		down.addActionListener(this);
		
		buttons.add( down );
		
		JButton del = new JButton( WebPolicyEditor.getIcon("editor/icons/16x16/edit-delete.png") );
		del.setActionCommand( REGEXP_DEL );
		del.addActionListener( this );
		del.setMaximumSize( new Dimension( 16, 16 ) );
		buttons.add( del );
		
		add( buttons, BorderLayout.NORTH );
		
		setToolTipText("The list of regular expressions.");
	}
	
	
	public void actionPerformed( ActionEvent evt ){
		int idx = jlist.getSelectedIndex();

		
		if( REGEXP_ADD.equals( evt.getActionCommand() )){
			lmodel.add( lmodel.size(), new String("new1") );
		}

		if( REGEXP_UP.equals( evt.getActionCommand() )){
			if( idx > 0 ){
				Object o = lmodel.remove( idx );
				lmodel.insertElementAt( o, idx - 1 );
				jlist.setSelectedIndex( idx - 1 );
			}
		}
		
		if( REGEXP_DOWN.equals( evt.getActionCommand() ) ){
			if( idx >= 0 &&  idx < lmodel.size() ){
				Object o = lmodel.remove( idx );
				lmodel.insertElementAt( o , idx + 1 );
				jlist.setSelectedIndex( idx + 1 );
			}
		}
		
		if( REGEXP_DEL.equals( evt.getActionCommand() ) ){
			if( idx >= 0 )
				lmodel.remove( idx );
		}
	}
	
	
	
	@Override
	public String getToolTipText() {
		if( jlist.isSelectionEmpty() )
			return super.getToolTipText();
		else {
			ParameterType rn = (ParameterType) jlist.getSelectedValue();
			return rn.getComment();
		}
	}


	public void setRegexpList( DefaultListModel lm ){
		lmodel = lm;
		jlist.setModel( lmodel );
	}
}
