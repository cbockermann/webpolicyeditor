package org.jwall.web.policy.editor;

import org.jwall.web.policy.TreeNode;

public interface PolicyModelListener
{
    public void nodeChanged( TreeNode node );
}
