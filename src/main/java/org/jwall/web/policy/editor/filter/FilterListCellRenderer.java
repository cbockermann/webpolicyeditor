package org.jwall.web.policy.editor.filter;

import java.awt.Color;
import java.awt.Component;
import java.util.Iterator;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.border.EmptyBorder;

import org.jwall.web.filter.ids.FilterRule;

/**
 * 
 * 
 * @author Christian Bockermann <chris@jwall.org>
 */
public class FilterListCellRenderer
extends DefaultListCellRenderer
{
    private static final long serialVersionUID = 8848797263170247020L;


    /* (non-Javadoc)
     * @see javax.swing.tree.DefaultTreeCellRenderer#getTreeCellRendererComponent(javax.swing.JTree, java.lang.Object, boolean, boolean, boolean, int, boolean)
     */
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

        
        if(!isSelected && !cellHasFocus ){
            if (  index % 2 == 0 )
                setBackground( new Color( 0xFF, 0xFF, 0xCC ) );
            else
                setBackground( Color.WHITE );
        }
        
        FilterRule rule = (FilterRule) value;
        if( rule == null )
            return this;
        
        setBorder( new EmptyBorder( 4,4,4,4 ) );
        
        StringBuffer s = new StringBuffer("<html>Rule: <code>"+HTMLEntityEncode(rule.getRegex())+"</code><br>");

        s.append("Id: <b>" + rule.getId() + "</b>, ");
        s.append("Tags: ");
        Iterator<String> it = rule.getTags().iterator();
        while( it.hasNext() ){
            s.append( "<b>" + it.next() + "</b>");
            if( it.hasNext() )
                s.append( ", " );
        }

        s.append("</html>");
        setText( s.toString() );
        setToolTipText( rule.getDescription() );

        return this;
    }

    public static String HTMLEntityEncode( String s )
    {
        StringBuffer buf = new StringBuffer();
        int len = (s == null ? -1 : s.length());

        for ( int i = 0; i < len; i++ )
        {
            char c = s.charAt( i );
            if ( c>='a' && c<='z' || c>='A' && c<='Z' || c>='0' && c<='9' )
            {
                buf.append( c );
            }
            else
            {
                buf.append( "&#" + (int)c + ";" );
            }
        }
        return buf.toString();
    }
}