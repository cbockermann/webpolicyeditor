package org.jwall.web.policy.editor.tree;

import org.jwall.web.policy.TreeNode;


/**
 * 
 * This interface defines a simple filter for filtering TreeNodes by
 * their attributes. Implementations may consider filtering by node
 * type, node-name or other properties.
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface TreeNodeFilter
{

    /**
     * This method is the central method of a filter. It must return <code>true</code>
     * if this filter matches the given node and will return <code>false</code> otherwise.
     * 
     * @param node The node that needs to be filtered.
     * @return <code>true</code> if this filter matches the node.
     */
    public boolean matches( TreeNode node );
}
