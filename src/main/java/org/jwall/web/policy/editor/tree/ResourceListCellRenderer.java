package org.jwall.web.policy.editor.tree;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.net.URL;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JList;

import org.jwall.WebPolicyEditor;
import org.jwall.web.policy.Context;
import org.jwall.web.policy.Method;
import org.jwall.web.policy.Parameter;
import org.jwall.web.policy.Resource;
import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.editor.AbstractPolicyTreeRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 
 * @author Christian Bockermann <chris@jwall.org>
 */
public class ResourceListCellRenderer
    extends DefaultListCellRenderer
{
    /** The unique class ID */
    private static final long serialVersionUID = -4856135770644164593L;

    static Logger logger = LoggerFactory.getLogger( ResourceListCellRenderer.class );
    
    public TreeNode dragSource = null;
    
    /* (non-Javadoc)
     * @see javax.swing.tree.DefaultTreeCellRenderer#getTreeCellRendererComponent(javax.swing.JTree, java.lang.Object, boolean, boolean, boolean, int, boolean)
     */
    @Override
    public Component getListCellRendererComponent(JList arg0, Object arg1, int index, boolean isSelected, boolean cellHasFocus)
    {
    	logger.debug("Rendering tree: {}, node: {}", arg0, arg1);
        if( arg1 instanceof TreeNode )
            return getListCellRendererComponent( arg0, (TreeNode) arg1, index, isSelected, cellHasFocus);
        
        setFont(new Font("Monospaced", Font.PLAIN, 13));
        super.getListCellRendererComponent(arg0, arg1, index, isSelected, cellHasFocus);
        return this;
    }        
        
    public Component getListCellRendererComponent(JList arg0, TreeNode arg1, int arg2, boolean arg3, boolean arg4)
    {
        super.getListCellRendererComponent(arg0, arg1, arg2, arg3, arg4);
        setFont(new Font("Monospaced", Font.PLAIN, 13));

        
        if( arg1.getType() == TreeNode.METHOD_NODE ){
        	Method m = (Method) arg1;
            setText( "Method "+m.getValue() );
            setIcon( WebPolicyEditor.getIcon(m) );
            return this;
        }
        
        if( arg1.getType() == TreeNode.META_INF_NODE ){
            setText( "Profile Information");
            setIcon( AbstractPolicyTreeRenderer.META_INF_ICON );
            return this;
        }
        
        if( arg1.getType() == TreeNode.META_AUTHOR_NODE ){
            setText( "<html>Author: <b>" + arg1.toString() +" </b></html>");
            setIcon( AbstractPolicyTreeRenderer.META_AUTHOR_ICON );
            return this;
        }
        
        if( arg1.getType() == TreeNode.META_VERSION_NODE ){
            setText( "<html>Version: <b>" + arg1.toString() + "</b></html>");
            setIcon( AbstractPolicyTreeRenderer.META_VERSION_ICON );
            return this;
        }
        
        if( arg1.getType() == TreeNode.POLICY_NODE ){
            setText("<html>Web Application Profile</html>");
            setIcon( AbstractPolicyTreeRenderer.PROFILE_ICON );
            return this;
        }
        
        if( arg1.getType() == TreeNode.SESSION_TRACKING_NODE ){
            setIcon( AbstractPolicyTreeRenderer.SESSION_TYPE_ICON );
            return this;
        }
        
        if( arg1.getType() == TreeNode.CONTEXT_NODE ){
        	Context ctx = (Context) arg1;
        	
        	String path = ctx.getBase();
        	if( path == null || path.equals( "" ) )
        		path = "/";
        	
        	setText("<html><b>" + path + "</b>");
        	setIcon( WebPolicyEditor.getIcon( ctx ) );
        	return this;
        }

        if( arg1.getType() == TreeNode.RESOURCE_NODE ){
        	Resource c = (Resource) arg1;
        	
        	String path = c.getName();
        	if( path == null || path.equals( "" ) )
        		path = "/";
        	
        	setText("<html><b>" + path + "</b>");
        	setIcon( AbstractPolicyTreeRenderer.getIcon( c ) );
        	return this;
        }

        
        if( arg1.getType() == TreeNode.PARAMETER_NODE ){
        	Parameter p = (Parameter) arg1;
        	
        	StringBuffer s = new StringBuffer("<html>Parameter: <b>"+p.getName()+"</b>");
        	
        	s.append(", Regexp: <b>" + p.getRegexp() + "</b>");
        	if( p.isRequired() )
        		s.append(", Required: <b>"+p.isRequired()+"</b>");

        	if( p.getScope() != null  )
        		s.append(", Scope: <b>"+p.getScope()+"</b>");
        	
        	s.append("</html>");
            setText( s.toString() );
            setIcon( WebPolicyEditor.getIcon(p) );

            return this;
        }
        
        setText( arg1.getName() );
        return this;
    }
    
    public Dimension getPreferredSize(){
        Dimension d = super.getPreferredSize();
        d.height = 30;
        return d;
    }

    public void setIcon(String s){
        this.setIcon( getIcon(s) );
    }
    
    public static ImageIcon getIcon(String s){
        URL url = ResourceListCellRenderer.class.getResource("/org/jwall/web/policy/"+s);
        if(url == null)
            logger.warn("Did not find icon: /org/jwall/web/profile/{}", s );
        
        return new ImageIcon(url);
    }
}
