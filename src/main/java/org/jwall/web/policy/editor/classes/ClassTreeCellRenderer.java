package org.jwall.web.policy.editor.classes;

import java.awt.Component;
import java.awt.Font;

import javax.swing.JTree;

import org.jwall.app.Application;
import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.abstraction.ParameterType;
import org.jwall.web.policy.abstraction.ResourceClass;
import org.jwall.web.policy.editor.AbstractPolicyTreeRenderer;
import org.jwall.web.policy.editor.tree.ResourceTreeCellRenderer;

public class ClassTreeCellRenderer
    extends ResourceTreeCellRenderer
{
    private static final long serialVersionUID = -8758501971077414622L;

    public ClassTreeCellRenderer(){
        font = new Font( "Sanserif", Font.PLAIN, 11 );
    }

    
    
    
    /* (non-Javadoc)
     * @see org.jwall.web.policy.editor.tree.ResourceTreeCellRenderer#setInheritanceColors()
     */
    @Override
    public void setInheritanceColors()
    {
    }


    /* (non-Javadoc)
     * @see org.jwall.web.policy.editor.tree.ResourceTreeCellRenderer#getTreeCellRendererComponent(javax.swing.JTree, org.jwall.web.policy.TreeNode, boolean, boolean, boolean, int, boolean)
     */
    @Override
    public Component getTreeCellRendererComponent(JTree arg0, TreeNode arg1,
            boolean arg2, boolean arg3, boolean arg4, int arg5, boolean arg6)
    {
        super.getTreeCellRendererComponent(arg0, arg1, arg2, arg3, arg4, arg5,
                arg6);

        
        if( arg1.getType() == TreeNode.PARAMETER_CLASSES ){
            setText("<html><b>Parameter Types</b></html>");
            setIcon( Application.getIcon( AbstractPolicyTreeRenderer.PARAMETER_CLASSES ) );
        }
        
        if( arg1.getType() == TreeNode.RESOURCE_CLASSES ){
            setText("<html><b>Resource Classes</b></html>");
            setIcon( Application.getIcon( "org.jwall.web.policy.ResourceClasses" ) );
        }


        if( arg1.getType() == TreeNode.PARAMETER_TYPE ){
            ParameterType type = (ParameterType) arg1;
            setText("<html><b>"+type.getName()+"</b><br>Regexp: <code>"+type.getRegexp()+"</code></html>");
            setIcon( Application.getIcon( type ) );
        }
        
        if( arg1.getType() == TreeNode.RESOURCE_CLASS ){
            ResourceClass rc = (ResourceClass) arg1;
            setText("<html>Class <b>" + rc.getName() + "</b></html>");
            setIcon( Application.getIcon( rc ) );
        }

        return this;
    }

}
