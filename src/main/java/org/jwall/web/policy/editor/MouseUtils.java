package org.jwall.web.policy.editor;

import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;


/**
 * <p>
 * This class is simply providing some easy methods for handling mouse-events. These are provided
 * here for two reasons: convenience and abstraction.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class MouseUtils
{
    /**
     * This method simply checks whether the given mouse-event is related
     * to a right-click or not. 
     * 
     * @param e The MouseEvent to check.
     * @return  <code>true</code>, if the event is a right-click 
     *          MouseEvent, <code>false</code> otherwise.
     */
    public static boolean isRightClick( MouseEvent e ){

        if( SwingUtilities.isRightMouseButton( e ) )
            return true;
        
        if( e.getButton() == MouseEvent.BUTTON1 && e.isControlDown() )
            return true;
        
        return false;
    }
}
