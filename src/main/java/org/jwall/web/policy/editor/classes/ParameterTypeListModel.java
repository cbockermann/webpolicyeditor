package org.jwall.web.policy.editor.classes;

import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.tree.TreePath;

import org.jwall.app.list.AbstractListModel;
import org.jwall.app.list.ListFilter;
import org.jwall.web.policy.WebPolicy;
import org.jwall.web.policy.abstraction.ParameterType;

public class ParameterTypeListModel extends AbstractListModel<ParameterType> {
	protected static final long serialVersionUID = -8621768856333390850L;

	ParameterType root = new ParameterType("@@@@", "....");
	WebPolicy profile;

	/**
     * 
     */
	public ParameterTypeListModel() {
		profile = new WebPolicy();
	}

	/**
	 * 
	 * @param p
	 */
	public ParameterTypeListModel(WebPolicy p) {
		profile = p;
	}

	public void setProfile(WebPolicy p) {
		profile = p;
		notifyListener();
	}

	public WebPolicy getProfile() {
		return profile;
	}

	public LinkedList<ParameterType> getAllChildren(ParameterType type) {

		if (profile == null)
			return new LinkedList<ParameterType>();

		LinkedList<ParameterType> types = new LinkedList<ParameterType>();
		ListFilter<ParameterType> filter = this.getFilter();

		Iterator<ParameterType> it = profile.getParameterTypes().iterator();
		while (it.hasNext()) {
			ParameterType pt = it.next();
			if (filter.matches(pt))
				types.add(pt);
		}

		return types;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.tree.TreeModel#getChild(java.lang.Object, int)
	 */
	public Object getChild(Object parent, int index) {
		if (parent == root)
			return getAllChildren(root).get(index);

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.tree.TreeModel#getChildCount(java.lang.Object)
	 */
	public int getChildCount(Object parent) {
		if (parent == root)
			return getAllChildren(root).size();

		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.tree.TreeModel#getIndexOfChild(java.lang.Object,
	 * java.lang.Object)
	 */
	public int getIndexOfChild(Object parent, Object child) {
		if (parent == root)
			return getAllChildren(root).indexOf(child);

		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.tree.TreeModel#getRoot()
	 */
	public Object getRoot() {
		return root;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.tree.TreeModel#isLeaf(java.lang.Object)
	 */
	public boolean isLeaf(Object node) {
		return node != root;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.swing.tree.TreeModel#valueForPathChanged(javax.swing.tree.TreePath,
	 * java.lang.Object)
	 */
	public void valueForPathChanged(TreePath path, Object newValue) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.ListModel#getElementAt(int)
	 */
	public Object getElementAt(int index) {
		return getChild(root, index);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.ListModel#getSize()
	 */
	public int getSize() {
		return getChildCount(root);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jwall.app.ui.AbstractListModel#add(java.lang.Object)
	 */
	public void add(ParameterType element) {
		profile.add(element);
		notifyListener();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jwall.app.ui.AbstractListModel#removeElementAt(int)
	 */
	public void removeElementAt(int i) {
		removeElementsAt(new int[] { i });
		notifyListener();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jwall.app.ui.AbstractListModel#removeElementsAt(int[])
	 */
	public void removeElementsAt(int[] idx) {

		LinkedList<ParameterType> types = new LinkedList<ParameterType>();
		for (int i : idx) {
			types.add((ParameterType) this.getChild(root, i));
		}

		for (ParameterType pt : types)
			profile.getParameterTypes().remove(pt);

		notifyListener();
	}
}
