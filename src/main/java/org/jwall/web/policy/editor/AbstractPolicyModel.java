package org.jwall.web.policy.editor;

import java.util.LinkedList;
import java.util.List;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;

import org.jwall.web.policy.AbstractTreeNode;
import org.jwall.web.policy.TreeNode;
import org.jwall.web.policy.WebPolicy;
import org.jwall.web.policy.abstraction.SuperClass;
import org.jwall.web.policy.editor.tree.PolicyTreeModel;

/**
 * 
 * This abstract class implements several methods used within the tree-model. It
 * serves as a basic implementation for the ClassTree- and the
 * ResourceTreeModel. Basicall it will be wrapped around the &quot;real&quot;
 * profile data structure and can perform basic operations.
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public abstract class AbstractPolicyModel {
	/** The list of document listeners, to be notified if the tree changes */
	protected List<DocumentListener> listener;

	/** The list of model listeners to be notified, if the tree changes */
	protected List<TreeModelListener> mlistener;

	protected List<PolicyModelListener> plistener;

	/** The base profile that this model is wrapped around */
	protected WebPolicy profile;

	/**
	 * 
	 * This constructor calls the super-constructor and initializes the basic
	 * data-structures.
	 * 
	 */
	public AbstractPolicyModel(WebPolicy p) {
		super();
		profile = p;
		listener = new LinkedList<DocumentListener>();
		mlistener = new LinkedList<TreeModelListener>();
	}

	public WebPolicy getProfile() {
		return profile;
	}

	/**
	 * This method simply adds the given listener to the internal list.
	 * 
	 * @param tml
	 *            The listener that is to be added.
	 */
	public void addTreeModelListener(TreeModelListener tml) {
		mlistener.add(tml);
	}

	/**
	 * This method removes the given listener to the internal list.
	 * 
	 * @param tml
	 *            The listener that is to be removed.
	 */
	public void removeTreeModelListener(TreeModelListener tml) {
		mlistener.remove(tml);
	}

	/**
	 * This method adds the given listener to this model.
	 * 
	 * @param l
	 *            The listener that is to be added.
	 */
	public void addProfileModelListener(PolicyModelListener l) {
		plistener.add(l);
	}

	/**
	 * This method removes the given listener from this model.
	 * 
	 * @param l
	 *            The listener that is to be removed.
	 */
	public void removeProfileModelListener(PolicyModelListener l) {
		plistener.remove(l);
	}

	/**
	 * This method simply adds the given listener to the internal list.
	 * 
	 */
	public void addDocumentListener(DocumentListener l) {
		listener.add(l);
	}

	/**
	 * This method removes the given listener to the internal list.
	 * 
	 */
	public void removeDocumentListener(DocumentListener l) {
		listener.remove(l);
	}

	/**
	 * This method is convenient for notifying all document-listeners.
	 */
	private void docChanged() {
		for (DocumentListener l : listener)
			l.documentChanged(null);
	}

	/**
	 * This method simply removes the given node from the tree and notifies all
	 * listeners. If the given node is the parent, this call will actually do
	 * nothing.
	 * 
	 * @param node
	 */
	public void deleteNode(TreeNode node) {
		TreeNode parent = node.getParent();
		if (parent != null) {
			parent.remove(node);
			notify(parent);
		}
	}

	/**
	 * This method inserts the new node <code>node</code> into the given parent
	 * node <code>parent</code> and notifies all corresponding listeners.
	 * 
	 * @param parent
	 * @param node
	 */
	public void insertNode(AbstractTreeNode parent, AbstractTreeNode node) {
		try {
			parent.add(node);
			notify(parent);
		} catch (Exception se) {
			se.printStackTrace();
		}
	}

	public void notify(TreeNode node) {
		notifyRemove(node);
		notifyChanged(node);
		notifyInsert(node);

		for (TreeModelListener l : mlistener) {
			l.treeStructureChanged(new TreeModelEvent(node, PolicyTreeModel
					.getPath(node)));
		}
		docChanged();
	}

	public void notifyChanged(TreeNode node) {
		for (TreeModelListener l : mlistener)
			l.treeNodesChanged(new TreeModelEvent(node, PolicyTreeModel
					.getPath(node)));

		docChanged();
	}

	public void notifyRemove(TreeNode node) {
		for (TreeModelListener l : mlistener) {
			l.treeNodesRemoved(new TreeModelEvent(node, PolicyTreeModel
					.getPath(node)));
			if (node.getParent() != null)
				l.treeNodesRemoved(new TreeModelEvent(node, PolicyTreeModel
						.getPath(node.getParent())));
		}

		docChanged();
	}

	public void notifyInsert(TreeNode node) {
		for (TreeModelListener l : mlistener) {
			l.treeNodesInserted(new TreeModelEvent(node, PolicyTreeModel
					.getPath(node)));
			if (node.getParent() != null)
				l.treeNodesInserted(new TreeModelEvent(node, PolicyTreeModel
						.getPath(node.getParent())));
		}

		docChanged();
	}

	public boolean isEditable() {
		return true;
	}

	protected SuperClass getSuperClass(TreeNode node) {
		TreeNode cur = node;
		while (cur != null) {
			if (cur instanceof SuperClass)
				return (SuperClass) node;

			cur = cur.getParent();
		}

		return null;
	}
}