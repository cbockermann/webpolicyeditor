package org.jwall.web.policy.compiler.ui;

import org.jwall.app.Action;

public class CompilerAction
    extends Action
{
    private static final long serialVersionUID = -4980412564949605003L;
    public final static String COMPILE_PROFILE = "org.jwall.web.policy.compile";
    
    public CompilerAction( String cmd, String menu, double idx ){
        super( cmd, menu, idx );
    }
}
