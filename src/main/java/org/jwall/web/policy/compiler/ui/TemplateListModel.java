package org.jwall.web.policy.compiler.ui;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import org.jwall.web.policy.compiler.Templates;

public class TemplateListModel
implements ListModel
{

    List<ListDataListener> listener = new LinkedList<ListDataListener>();
    Vector<String> templates = new Vector<String>();
    Vector<String> templateDescriptions = new Vector<String>();


    public TemplateListModel(){

        Map<String,URL> temp = Templates.findTemplates();

        for( String alias : temp.keySet() ){

            try {
                URL url = temp.get( alias ); //TemplateListModel.class.getResource( temp.get( alias ) );
                if( url != null ){
                    templateDescriptions.add( Templates.extractDescription( url.openStream() ) );
                    templates.add( alias );
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public Object getElementAt(int index)
    {
        return templates.elementAt( index );
    }

    public int getSize()
    {
        return templates.size();
    }

    public void addListDataListener(ListDataListener l)
    {
        listener.add( l );
    }

    public void removeListDataListener(ListDataListener l)
    {   
        listener.remove( l );
    }

}
