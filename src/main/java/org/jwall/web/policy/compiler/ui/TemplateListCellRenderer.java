package org.jwall.web.policy.compiler.ui;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jwall.WebPolicyEditor;

public class TemplateListCellRenderer
    extends DefaultListCellRenderer
{
    private static final long serialVersionUID = -2409041191358129205L;
    TemplateListModel tlm;

    public TemplateListCellRenderer( TemplateListModel lm ){
        tlm = lm;
    }

    
    /* (non-Javadoc)
     * @see javax.swing.DefaultListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
     */
    @Override
    public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected, boolean cellHasFocus)
    {
        super.getListCellRendererComponent(list, value, index, isSelected,
                cellHasFocus);
        
        
        JPanel p = new JPanel( new BorderLayout() );
        
        this.setVerticalTextPosition( JLabel.CENTER );
        this.setIconTextGap( 14 );
        this.setAlignmentY( Component.CENTER_ALIGNMENT );
        
        p.setBackground( this.getBackground() );
        p.setForeground( this.getForeground() );
        
        ImageIcon icon = WebPolicyEditor.getIcon( "org.jwall.web.policy.compiler.template" );
        
        if( index == 0 )
            icon = WebPolicyEditor.getIcon( "org.jwall.web.policy.compiler.low-security-template" );
        
        if( index == 1 )
            icon = WebPolicyEditor.getIcon( "org.jwall.web.policy.compiler.high-security-template" );
        
        if( icon != null ){
            //this.setBorder( new EmptyBorder( 20, 0, 0, 0 ) );
            //setIcon( icon );
        }
        
        JLabel i = new JLabel( icon );
        i.setBorder( new EmptyBorder( 15, 15, 15, 15 ) );
        i.setOpaque( true );
        i.setBackground( getBackground() );
        i.setForeground( getForeground() );
        
        p.add( i, BorderLayout.WEST );
        
        p.setOpaque( true );
        
        if( index >= 0 && index < tlm.templateDescriptions.size() ){
            StringBuffer txt = new StringBuffer("<html><body style=\"font-family: Verdana,Arial,Sansserif; font-size:11pt;\">");
            txt.append( tlm.templateDescriptions.elementAt( index ) );
            txt.append( "</body></html>" );
            //setText( txt.toString() );
            
            JEditorPane ep = new JEditorPane();
            ep.setContentType( "text/html" );
            ep.setText( txt.toString() );
            ep.setEditable( false );
            ep.setBorder( new EmptyBorder( 5, 5, 5, 5 ) );
            
            ep.setForeground( this.getForeground() );
            ep.setBackground( this.getBackground() );
            
            JLabel text = new JLabel( txt.toString() );
            text.setAlignmentY( Component.CENTER_ALIGNMENT );
            text.setVerticalTextPosition( JLabel.TOP );
            text.setBorder( null ); //new EmptyBorder( 5, 5, 5, 5 ) );
            
            p.add( ep, BorderLayout.CENTER );
        }
        return p;
        
        //return this;
    }
}