package org.jwall.web.policy.compiler.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jwall.app.Application;
import org.jwall.app.ui.Dialog;
import org.jwall.app.ui.ImagePanel;
import org.jwall.app.ui.InputDialog;


/**
 * <p>
 * This dialog provides a user choice for selecting the template to be used for rule set generation. Per default
 * there are two templates available: the &quot;weak&quot; and the &quot;strict&quot; one. The list can be extended
 * by custom templates later on.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class CompilerPreferenceDialog
    extends Dialog
    implements ListSelectionListener
{

    /** The unique class version */
    private static final long serialVersionUID = -8143243508538302267L;

    /** This attribute reflects the users choice, i.e. a negative value means the user hit the cancel button */
    int returnStatus = 0;
    
    /** The buttons provided to the user */
    JButton ok, cancel;
    
    /** A check box to allow the choice become persistent */
    JCheckBox remember;
    
    /** The UI list holding the templates */
    JList templates;

    /** The list model of the templates */
    TemplateListModel lm;
    
    /** The path to the selected template */
    String selectedTemplate = null;

    
    /**
     * 
     * Creates a new dialog which allows for the user to select one of the templates.
     * 
     */
    public CompilerPreferenceDialog(){
        super();
        setModal( true );
        setResizable( false );
        setTitle( "Select Template for Rule Generation" );
        
        this.getContentPane().setLayout( new BorderLayout() );
        
        ImagePanel i = new ImagePanel(InputDialog.class.getResource("/icons/logo_tall_60x513.jpg"), 0, 0, 60, 513);
        i.setHeight( 513 );
        i.setWidth( 60 );
        i.setBackground( Color.BLACK );
        this.getContentPane().add( i, BorderLayout.WEST );
        
        
        JEditorPane msg = new JEditorPane();
        msg.setBackground( Color.WHITE );
        msg.setFont( msg.getFont().deriveFont( Font.PLAIN ) );
        JPanel info = new JPanel( new BorderLayout() );
        info.setBackground( Color.WHITE );
        
        JLabel l = new JLabel();
        l.setAlignmentX( Component.CENTER_ALIGNMENT );
        l.setAlignmentY( Component.CENTER_ALIGNMENT );
        l.setBorder( new EmptyBorder( 5, 5, 5, 5 ) );
        l.setIcon( Application.getIcon("org.jwall.security.cert.certificate") );
        info.add(  l, BorderLayout.EAST );
        info.add( msg, BorderLayout.CENTER );
        msg.setContentType( "text/html" );
        msg.setText( "<html><body style=\"font-family: Verdana,Arial,Sansserif; font-size: 12pt;\">"
                + "The WebProfileEditor provides several templates for rule generation, each of which "
                + "focuses on a different level of strictness. Please choose the template you want to "
                + "use from the list below:"
                + "</body></html>" );
        msg.setEditable( false );
        msg.setBorder( new EmptyBorder( 25, 15, 15, 15 ) );
        
        JPanel p = new JPanel( new BorderLayout() );
        p.add( info, BorderLayout.NORTH );
        
        JPanel buttons = new JPanel( new FlowLayout( FlowLayout.RIGHT ) );
        
        lm = new TemplateListModel();
        TemplateListCellRenderer renderer = new TemplateListCellRenderer( lm );
        templates = new JList( lm );
        templates.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
        templates.addListSelectionListener( this );
        templates.setCellRenderer( renderer );
        templates.setBorder( BorderFactory.createLoweredBevelBorder() );
        //templates.setFixedCellHeight( 80 );
        templates.setFixedCellWidth( 300 );
        JScrollPane sp = new JScrollPane( templates );
        sp.setBorder( new EmptyBorder( 10, 10, 10, 10 ) );
        
        p.add( sp, BorderLayout.CENTER );
        
        remember = new JCheckBox("Remember template selection.");
        remember.setBorder( new EmptyBorder( 0, 15, 0, 0 ) );
        remember.setFocusPainted( false );
        remember.setEnabled( !templates.isSelectionEmpty() );
        p.add( remember, BorderLayout.SOUTH );
        
        
        ok = new JButton("Ok");
        ok.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e){
                
                int idx = templates.getSelectedIndex();
                selectedTemplate = lm.templates.elementAt( idx ); //.templates[idx];
                setVisible( false );
            }
        });
        ok.setEnabled( !templates.isSelectionEmpty() );
        
        cancel = new JButton("Cancel");
        cancel.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e){
                returnStatus = -1;
                setVisible( false );
            }
        });
        
        buttons.add( ok );
        buttons.add( cancel );
        this.getContentPane().add( p, BorderLayout.CENTER );
        
        this.getContentPane().add( buttons, BorderLayout.SOUTH );
        
        setSize( 640, 400 );
        this.center();
    }
 
    
    /**
     * Returns the user action on this dialog, i.e. either a positive value if the user
     * hit the OK button or a negative value if the user canceled the selection.
     * 
     * @return Whether the user selected a template or canceled the selection.
     */
    public int getReturnStatus(){
        return returnStatus;
    }

    
    /**
     * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
     */
    public void valueChanged(ListSelectionEvent e)
    {
        if( !e.getValueIsAdjusting() ){
            ok.setEnabled( !templates.isSelectionEmpty() );
            remember.setEnabled( !templates.isSelectionEmpty() );
        }
    }
    
    public String getSelectedTemplate(){
        return selectedTemplate;
    }

    
    /**
     * Returns whether the user marked the check box to remember the selection next time.
     * 
     * @return
     */
    public boolean rememberUserSelection(){
        return remember.isSelected();
    }
}