<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text"/>

<!-- 
@jwall.template.version $Revision:231 $
@jwall.template.name session
@jwall.template.author Christian Bockermann <chris@jwall.org>
@jwall.template.description
<b>Session Rules</b> <br>
This template will create all session-based stuff, e.g. the sticky
session tracking or the session base request counter. It does not
create any resource specific rules, yet.
  -->

<!--                                                                                      -->
<!--    This is the root-template for transforming profiles into ModSecurity rulesets.    -->
<!--    It does not include a catch-all rule, which means unspecified URLs, parameters,   -->
<!--    etc. simply do not get checked.                                                   -->
<!--                                                                                      -->
<xsl:template match="/WebPolicy">######  Begin SESSION TRACKING  ###############
#
#  $Id:SessionTracking.xslt 217 2009-01-12 10:21:51Z chris $
#
#  This rule set has been compiled using the  WebPolicyCompiler
#  See  http://www.jwall.org/web/policy/wpc.html  for details. 
#       
<xsl:apply-templates select="SessionTracking" />
#
#######  End: SESSION TRACKING   ###############
</xsl:template>


<!--                                                                      -->
<!--  Create the session tracking rules                                   -->
<!--                                                                      -->
<xsl:template match="SessionTracking">
      #
      #  All rules created will be non interruptive rules.
      # 
      SecDefaultAction "phase:1,log,auditlog,pass"
<xsl:apply-templates select="Cookie" />
</xsl:template>


<!--                                                                      -->
<!--  This template initiates a cookie based session collection           -->
<!--                                                                      -->
<xsl:template match="Cookie">
<xsl:variable name="valuePattern">
<xsl:choose>
   <xsl:when test="count( ./@regexp ) = 0">[\w\d]+</xsl:when>
   <xsl:otherwise><xsl:value-of select="@regexp" /></xsl:otherwise>
</xsl:choose>
</xsl:variable>
<xsl:variable name="expireTime">
<xsl:choose>
   <xsl:when test="count(../@expires) = 0">3600</xsl:when>
   <xsl:otherwise><xsl:value-of select="@expires" /></xsl:otherwise>
</xsl:choose>
</xsl:variable>

      #
      # initialize the session based on a session cookie <xsl:value-of select="@name" />
      # the session will be validated and the validation-token will expire after <xsl:value-of select="$expireTime"/> seconds
      #
      SecRule RESPONSE_HEADERS:/Set-Cookie2?/ "<xsl:value-of select="@name"/>=(<xsl:value-of select="$valuePattern"/>)\;\s?" \
           "phase:3,capture,msg:'Captured session id from response cookie: %{TX.1}',chain"
    
      SecAction setsid:%{TX.1},setvar:session.valid=1,expirevar:session.valid=<xsl:value-of select="$expireTime" />, \
                setvar:session.id=%{TX.1},expirevar:session.id=<xsl:value-of select="$expireTime" />

<xsl:if test="count( @regexp ) != 0">
      # if a regexp-pattern was provided, we include a pattern check for the cookie 
      #          
      SecRule REQUEST_COOKIES:<xsl:value-of select="@name" /> !^<xsl:value-of select="$valuePattern" />$ \
           phase:1,log,auditlog,msg:'Transmitted cookie &quot;<xsl:value-of select="@name" />&quot; does not match value-pattern!',deny,status:400
</xsl:if>         
      # If the client did send a session cookie we need to make ModSecurity load
      # the corresponding session using  "setsid". 
      #
      SecRule REQUEST_COOKIES:<xsl:value-of select="@name" /> !^$ "phase:1,log, \
            msg:'Found cookie in request, initializing session!', \
            setsid:%{REQUEST_COOKIES.<xsl:value-of select="@name" />},skip:1"
<!--                                                                     -->
<!--    session validation is only done for strict session tracking      -->
<!--                                                                     -->
<xsl:if test="../@strict = 'true'">
      # If the client did not pass a session cookie, e.g. in the first request,
      # we do not check for session validity and skip the validity check-rule
      #
      SecAction phase:1,log,skip:1, \
          msg:'No session transmitted, skipping session validity check!'

      # If the session id is invalid then we drop the connection
      #
      SecRule &amp;SESSION:VALID "@eq 0" phase:1,log,auditlog,deny,status:500, \
            msg:'The requested session is not valid!!'
</xsl:if>           
</xsl:template>

<!--                                                                      -->
<!--  This template strips off all texts from basic text-bodies.          -->
<!--                                                                      -->
<xsl:template match="text()">
  <xsl:value-of select="normalize-space(.)"/>
</xsl:template>

</xsl:stylesheet>