<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text"/>

<!-- 
@jwall.template.name strict
@jwall.template.version $Revision:231 $
@jwall.template.author Christian Bockermann <chris@jwall.org>
@jwall.template.description
<b>Strict Rules</b> <br>
This template generates a very strict rule set by disallowing everything which
is not specified by the profile.
 -->

<!--                                                                 -->
<!--    This is the root-template for transforming profiles into     -->
<!--    ModSecurity rulesets.                                        -->
<!--                                                                 -->
<xsl:template match="/WebPolicy">
#
#
#  $Id:StrictPositiveRules.xslt 217 2009-01-12 10:21:51Z chris $
#
#  This ruleset has been compiled using the  WebPolicyEditor
#  See  http://www.jwall.org/web/profile/  for details. 
#
#  The following lines do set some basic properties of the rulesets.
#       
   <!--  Basically we apply all templates here  -->
   <xsl:apply-templates select="Context"> 
       <xsl:with-param name="path" select="''"/>
   </xsl:apply-templates>

   <!--                                                   -->
   <!--  The final LocationMatch will be a "deny all"     -->
   <!--                                                   -->
   #
   # The final "deny all"
   #
   &lt;LocationMatch &quot;^.*$&quot;&gt;
        SecRule &amp;TX:METHOD_CHECKED  "!@gt 0" "auditlog,phase:2,log,msg:'Invalid URL requested!',redirect:UnknownURLRequested.html" 
   &lt;/LocationMatch&gt;
   
</xsl:template>



<!--                                                              -->
<!--  This template creates all meta-information within the       -->
<!--  resulting rule-file.                                        -->
<!--                                                              -->
<xsl:template match="Meta-Inf">
#
# Meta-Information
#   Author: <xsl:value-of select="Author" />
#   Version: <xsl:value-of select="Version" />
# 
</xsl:template>



<!--                                                                                     -->
<!--  A web application is basically a set of resources. Each resource will be           -->
<!--  handled seperately, this is the template for a resource entry...                   -->
<!--                                                                                     -->
<xsl:template match="Resource">
  <!--  The if is a simple optimization: don't create entries for "empty" resources!     -->
   <xsl:if test="count(./*) - count(./Resource) > 0">
  
    <!--  Here we compute the full path of this resource by traversing all the way to the root node.  -->
    <xsl:variable name="path">
      <xsl:for-each select="ancestor-or-self::Resource">
        <xsl:if test="./@name != ''">
           <xsl:value-of select="concat('/',./@name)" />
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    
  &lt;LocationMatch &quot;^<xsl:value-of select="$path"/>$&quot;&gt;
    #
    # mypath: <xsl:value-of select="$path"/> 
    # resource-specific rules 
    #
    <xsl:apply-templates select="Method" />

    #
    # the control-section
    #
    SecRule &amp;TX:METHOD_CHECKED "@eq 0" "setvar:tx.score=+1,log,auditlog,msg:'Fatal: Invalid method for <xsl:value-of select="$path" />'"
    SecRule TX:SCORE "@gt 0" "log,auditlog,msg:'transaction-score is %{TX.SCORE}'"
    
  &lt;/LocationMatch&gt;
  </xsl:if>

  <!--  Process the nested resources...  -->
  <xsl:apply-templates select="Resource" />
  
</xsl:template>


<xsl:template match="Context">
  #
  # processing the context
  #
  <xsl:apply-templates select="Resource">
  	       <xsl:with-param name="path" select="''"/>
  </xsl:apply-templates>
</xsl:template>


<!--                                                      -->
<!--  The following template creates a method-check       -->
<!--                                                      -->
<xsl:template match="Method">
    #
    # called with method=<xsl:value-of select="./@value" />
    #
    SecRule REQUEST_METHOD &quot;!^<xsl:value-of select="./@value"/>$&quot; phase:2,t:none,log,auditlog,skip:<xsl:value-of select="1 + 2 * count(child::Parameter)"/>
    SecAction setvar:tx.method_checked=1,pass,nolog,noauditlog<xsl:apply-templates select="Parameter|CreateToken|CheckToken"/>
</xsl:template>



<!--                                                      -->
<!--  The following template creates a parameter-check    -->
<!--                                                      -->
<xsl:template match="Parameter">
    <xsl:variable name="score">1</xsl:variable>
	<xsl:if test="count( ./@score ) > 0 and ./score != '' ">
        <xsl:variable name="score"><xsl:value-of select="1" /></xsl:variable>
    </xsl:if>

    #
    # Parameter &quot;<xsl:value-of select="./@name" />&quot;
    #    <xsl:if test="count(@required) > 0">
    SecRule &amp;ARGS:<xsl:value-of select="./@name"/> "@eq 0" "setvar:tx.score=+<xsl:value-of select="$score" />,pass,msg:'Missing required parameter <xsl:value-of select="./@name"/>'"</xsl:if>

    <!--   Here we check if we have to evaluate the regexp ( all regexps like ${name} need to be remapped! )  -->
    <xsl:choose>
      <xsl:when test="starts-with( ./@regexp, '${' )">
  	    <xsl:variable name="rxID"><xsl:value-of select="substring( ./@regexp, 3, string-length(./@regexp) - 3 )" /></xsl:variable>
  	    <xsl:variable name="rx"><xsl:value-of select="//ParameterType[@name=$rxID]/@regexp"/></xsl:variable>
    # The pattern reference is: &quot;<xsl:value-of select="$rxID"/>&quot;
    SecRule ARGS:<xsl:value-of select="./@name"/> &quot;!<xsl:value-of select="$rx"/>&quot; &quot;setvar:tx.score=+<xsl:value-of select="$score"/>,phase:2,pass&quot;<xsl:apply-templates />
  	  </xsl:when>
  	<xsl:otherwise>
    SecRule ARGS:<xsl:value-of select="./@name"/> &quot;!<xsl:value-of select="./@regexp"/>&quot; &quot;setvar:tx.score=+<xsl:value-of select="$score"/>,phase:2,pass&quot;<xsl:apply-templates />
  	</xsl:otherwise>

  </xsl:choose>

  <!--  Apply possbibly nested templates (dependent parameters, etc.)  -->
  <xsl:apply-templates />

</xsl:template>



<!--                                                      -->
<!--  The following template creates a cookie-check       -->
<!--                                                      -->
<xsl:template match="Cookie">

    <xsl:if test="count(required) > 0">
    SecRule &amp;REQUEST_COOKIES:<xsl:value-of select="./@name" /> &quot;!@eq 1&quot; &quot;setvar:tx.score+=1,phase:2,msg:'Mandatory cookie  <xsl:value-of select="./@name"/>  not present!'&quot;
    </xsl:if>
    <!--   Here we check if we have to evaluate the regexp ( all regexps like ${name} need to be remapped! )  -->
    <xsl:choose>
      <xsl:when test="starts-with( ./@regexp, '${' )">
  	    <xsl:variable name="rxID"><xsl:value-of select="substring( ./@regexp, 3, string-length(./@regexp) - 3 )" /></xsl:variable>
  	    <xsl:variable name="rx"><xsl:value-of select="//ParameterType[@name=$rxID]/@regexp"/></xsl:variable>
    # The pattern reference is: &quot;<xsl:value-of select="$rxID"/>&quot;
	SecRule REQUEST_COOKIES:<xsl:value-of select="./@name"/> &quot;!<xsl:value-of select="$rx"/>&quot; &quot;setvar:tx.score=+<xsl:value-of select="@score"/>,phase:2,pass&quot;<xsl:apply-templates />
  	  </xsl:when>
  	  <xsl:otherwise>
	SecRule REQUEST_COOKIES:<xsl:value-of select="./@name"/> &quot;!<xsl:value-of select="./@regexp"/>&quot; &quot;setvar:tx.score=+<xsl:value-of select="@score"/>,phase:2,pass&quot;<xsl:apply-templates />
      </xsl:otherwise>
    </xsl:choose>
</xsl:template>



<!--                                                      -->
<!--  The following template creates a header-check       -->
<!--                                                      -->
<xsl:template match="Header">

    <xsl:if test="count(required) > 0">
    SecRule &amp;REQUEST_HEADER:<xsl:value-of select="./@name" /> &quot;!@eq 1&quot; &quot;setvar:tx.score+=1,phase:2,msg:'Mandatory header  <xsl:value-of select="./@name"/>  not present!'&quot;
    </xsl:if>
    <!--   Here we check if we have to evaluate the regexp ( all regexps like ${name} need to be remapped! )  -->
    <xsl:choose>
      <xsl:when test="starts-with( ./@regexp, '${' )">
  	    <xsl:variable name="rxID"><xsl:value-of select="substring( ./@regexp, 3, string-length(./@regexp) - 3 )" /></xsl:variable>
  	    <xsl:variable name="rx"><xsl:value-of select="//ParameterType[@name=$rxID]/@regexp"/></xsl:variable>
    # The pattern reference is: &quot;<xsl:value-of select="$rxID"/>&quot;
	SecRule REQUEST_HEADER:<xsl:value-of select="./@name"/> &quot;!<xsl:value-of select="$rx"/>&quot; &quot;setvar:tx.score=+<xsl:value-of select="@score"/>,phase:2,pass&quot;<xsl:apply-templates />
  	  </xsl:when>
  	  <xsl:otherwise>
	SecRule REQUEST_HEADER:<xsl:value-of select="./@name"/> &quot;!<xsl:value-of select="./@regexp"/>&quot; &quot;setvar:tx.score=+<xsl:value-of select="@score"/>,phase:2,pass&quot;<xsl:apply-templates />
      </xsl:otherwise>
    </xsl:choose>
</xsl:template>


<!--                                                                      -->
<!--  This template strips off all texts from basic text-bodies.          -->
<!--                                                                      -->
<xsl:template match="text()">
  <xsl:value-of select="normalize-space(.)"/>
</xsl:template>

<xsl:template match="//ClassDefinitions">
</xsl:template>

</xsl:stylesheet>
