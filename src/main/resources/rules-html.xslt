<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html"/>


<!--                                                                 -->
<!--    This is the root-template for transforming profiles into     -->
<!--    ModSecurity rulesets.                                        -->
<!--                                                                 -->
<xsl:template match="core-rules">

<html>

  <head>
  	<title>Visualizing the core-rules ruleset</title>

    <style>

body {
   font-family: Verdana,Arial,Helvetica;
   font-size: 10pt;
}

div.disclaimer {
   margin: 1px;
   background-color: #E0E0E0;
   color: #A0A0A0;
   text-align: right;
   font-size: 8pt;
}

.label {
   font-family: Verdana,Arial,Helvetica;
   font-size: 9pt;
   font-weight: bold;
   padding-right: 10px;
   vertical-align: top;
   color: #333333;
}

div {
   padding: 3px;
   border-style: solid;
   border-width: 1px;
}

div.ph1 {
   background-color: #010101;
}

div.ph2 {
   //width: 800px;
   margin: 4px;
   border-color: #A0A0A0;
   background-color: #BFBFBF;
}
    
.comment {
   font-size: 10pt;
}    

div.comment {
   border-color: #C8C864;
   background-color: #FFFFC8;
}

div.selector {
   font-family: Courier;
   font-size: 10pt;
   padding: 4px;
   border-style: none;
//   border-color: #C8C864;
//   background-color: #FFFFC8;
   background-color: #FFFFFF;
}    

div.operator {
   padding: 4px;
   font-family: Courier;
   font-size: 10pt;
   border-style: none;
   border-width: 1px;
   border-color: #C8C864;
//   background-color: #FFFFC8;
   background-color: #FFFFFF;
}    

div.t {
   padding: 4px;
   font-family: Courier;
   font-size: 10pt;
   border-style: none;
   border-width: 1px;
   border-color: #C8C864;
   background-color: #DDDDDD;
}

div.actions {
   font-family: Courier;
   font-size: 10pt;
   border-style: none;
   border-width: 1px;
   border-color: #C8C864;
   //background-color: #FFFFC8;
   //background-color: #99CCFF;
   background-color: #DDDDDD;
}
div.location {
   font-family: Verdana;
   font-size: 8pt;
   border-style: none;
}

div.RulePhase1 {
   margin: 1px;
   padding: 4px;
   border-color: #666699;
   background-color: #CCCCFF;
}

div.RulePhase2 {
   margin: 1px;
   padding: 4px;
   border-color: #993333;
   background-color: #FF9999;
}

div.RulePhase3 {
   margin: 1px;
   padding: 4px;
   border-color: #669933;
   background-color: #CCFF99;
}

div.RulePhase4 {
   margin: 1px;
   padding: 4px;
   border-color: #336699;
   background-color: #99CCFF;
}

div.RulePhase5 {
   margin: 1px;
   padding: 4px;
   border-color: #666699;
   background-color: #CCCCFF;
}

div.chained {
   margin: 0px;
   padding: 1px;
   border-style:none;
   //background-color: #A0A0A0;
}

div.rule {
   margin: 0px;
   border-color: #A0A0A0;
   border-width: 2px;
   background-color: #BFBFBF;
}

div.meta {
   text-align:left;
   font-size: 8pt;
   font-family: Verdana,Arial,Helvetica;
   border-style: none;
}

div.tags {
   font-family: Verdana,Arial,Helvetica;
   font-size: 9pt;
   text-align: left;
   border-style: none;
}

    </style>
  </head>

<body>

    <div class="disclaimer">
        This page was generated using the <a href="http://jwall.org/core-rules/">CoreRule2Html</a>.
        Copyright by Christian Bockermann &lt;chris (at) jwall.org&gt;.<br/>
        The <i>ModSecurity</i> module and the <i>core rules</i> are a registered trademark by <i>Breach Security</i>. 
    </div>

    <div class="RulePhase1"><h2>Phase 1</h2>
       <a name="phase2"/>
       <xsl:apply-templates select="child::rule[attribute::phase=1]"/>
    </div>
    
    
    <div class="RulePhase2"><h2>Phase 2</h2>
      <a name="phase2"/>
      <xsl:apply-templates select="child::rule[attribute::phase=2]"/>
    </div>

    <div class="RulePhase3"><h2>Phase 3</h2>
      <a name="phase3"/>
      <xsl:apply-templates select="child::rule[attribute::phase=3]"/>
	</div>

    <div class="RulePhase4"><h2>Phase 4</h2>
      <a name="phase4"/>
      <xsl:apply-templates select="child::rule[attribute::phase=4]"/>
	</div>

    <div class="RulePhase5"><h2>Phase 5</h2>
      <a name="phase5"/>
      <xsl:apply-templates select="child::rule[attribute::phase=5]"/>
	</div>

    <div class="disclaimer">
        This page was generated using the <a href="http://jwall.org/core-rules/">CoreRule2Html</a>.
        Copyright by Christian Bockermann &lt;chris (at) jwall.org&gt;.<br/>
        The <i>ModSecurity</i> module and the <i>core rules</i> are a registered trademark by <i>Breach Security</i>. 
    </div>

</body>  
  
</html>   
</xsl:template>


<xsl:template match="rule">
   <div class="rule"> 
   <xsl:if test="@id != ''">
       <xsl:variable name="id"><xsl:value-of select="@id" /></xsl:variable>
	   <a name="$id"/>
   </xsl:if>
   <xsl:variable name="phase"><xsl:value-of select="@phase" /></xsl:variable>
   
      <table border="0" width="100%">
        <tr>
          <td class="label"> <b>SecRule</b><p/> </td>
          <td align="right" width="50%">
            <div class="meta">
             <xsl:if test="@id != ''"> <img src="stock_id.png" style="height:12px; padding-right: 2px;" /> <b style="font-size: 8pt;">ID </b> <xsl:value-of select="@id" /><br/></xsl:if>
             <xsl:if test="@phase != ''"> <b>Phase </b> <xsl:value-of select="./@phase" /><br/></xsl:if>
             <b>Location </b><xsl:value-of select="@location" />
            </div>
          </td>
        </tr>
        <tr>
          <td colspan="2">
      <table>
        <tr>
           <td class="label"> <nobr> <img src="stock_insert_endnote.png" /> Target </nobr> </td> <td> <xsl:apply-templates select="selector" /> </td>
        </tr>
        <tr>
           <td></td> 
           <td>
				<xsl:if test="count( actions/t[text() != 'none'] ) > 0 ">
              	  <div class="t" style="vertical-align: middle;"> 
					<xsl:apply-templates select="actions/t" />
				  </div>
				</xsl:if>
           </td>
        </tr>
        <tr>
           <xsl:variable name="op"><xsl:value-of select="./operator"/></xsl:variable>
           <xsl:choose>
           <xsl:when test="starts-with( $op, '@' ) or starts-with( $op, '!@' ) ">
             <td class="label"> <img src="stock_test-mode.png" /> Op </td> <td> <xsl:apply-templates select="operator" /> </td>
           </xsl:when>
           <xsl:otherwise>
			 <td class="label">  <!--   <img src="stock_test-mode.png" /> -->matches </td> <td> <xsl:apply-templates select="operator" /> </td>
           </xsl:otherwise>
           </xsl:choose>
        </tr>
         
         
        <xsl:if test="count( actions/*[name() != 't' and name() != 'tag'] ) > 0 ">
        <tr>
          <td class="label">Actions</td>
          <td>
           <div class="actions">
              <xsl:apply-templates select="actions/*[name() != 't' and name() != 'tag']" />
           </div>
           </td>
        </tr>
        </xsl:if>

		<xsl:if test="count( actions/tag ) > 0" >
		<tr>
		   <td class="label">Tags </td>
		   <td>
		     <div class="tags">
		     <xsl:for-each select="actions/tag">
		        <b><xsl:value-of select="text()" /></b>
		     </xsl:for-each>
		     </div>
		   </td>
		</tr>
		</xsl:if>

      </table> 
           </td>
         </tr>
         
       </table>

	  <xsl:if test="count( rule ) > 0 ">
	    <div class="chained">
   	      <xsl:apply-templates select="rule" />
        </div>
	  </xsl:if>
	  
	  <xsl:if test="count( comment ) > 0 ">
        <xsl:apply-templates select="comment" />
	  </xsl:if>
	  </div>
	  <br/>
</xsl:template>


<xsl:template match="actions/*[name() != 't' and name() != 'tag']">
	  <xsl:value-of select="name(.)"/><xsl:if test="text() != ''">:<xsl:value-of select="text()" /></xsl:if><br/>
</xsl:template>

<xsl:template match="actions/t">
    <xsl:if test=" text() != 'none' ">
		<img src="stock_draw-line-with-circle-arrow.png" style="height: 12px; padding-left:4px; padding-right: 4px;" /> <xsl:value-of select="text()"/>
    </xsl:if>
</xsl:template>



<xsl:template match="selector">
    <div class="selector">
      <xsl:value-of select="text()" />
    </div>
</xsl:template>


<xsl:template match="operator">
    <div class="operator"> 
      <xsl:value-of select="text()" /> 
    </div>
</xsl:template>

<xsl:template match="comment">
   <div class="comment">
     <table>
     <tr>
       <td valign="top">
         <img src="stock_notes.png" />
       </td>
       <td class="comment">
         <pre style="font-family:Verdana;">
       	   <xsl:value-of select="text()" />
       	 </pre>
       </td>
     </tr>
     </table>
   </div>
</xsl:template>



</xsl:stylesheet>