<html>

<head>
   <title>Web Application Profiles</title>
<!--
   <link rel="stylesheet" type="text/css" href="style.css" media="screen" />
-->
</head>

<body>

<div style="width: 600px;">

<h1>Web Application Profiles</h1>

The <i>Web Application Profiles</i> of the JWall system constitute a highlevel XML description of
web applications which serve as basis for <i>positive security models</i> for the applications. The
description is based upon a clean and easily understandable XML schema, the <i>Web Profile Language</i>.
<p>
The use of XML allows for a specification of a white-listing approach which is independent of the
web application firewall or validation system that is to be used. As one of the first WAF systems
that the profiles can be used in, we provide a Java based compiler for transforming the profiles
into a ruleset for the  <i>ModSecurity</i> module.
<p>
In order to ease the creation of such a positive security model for an application, <code>jwall.org</code>
provides two approaches:
<ul>
  <li><i>Manual creation</i> using the easy-to-use graphical <a href="http://www.jwall.org/web/profile/editor.jsp">ProfileEditor</a></li>
  <li><i>Learning</i> profiles from audit data by the use of the <a href="http://www.jwall.org/web/profiler/index.jsp">WebApplicationProfiler</a></li>
</ul>

The <i>ProfileEditor</i> is a standalone Java application which allows loading, editing and compiling (i.e.
transformation into <i>ModSecurity</i> rules). 
<p>
In addition to a plain white-listing approach, the XML language and the <i>ProfileEditor</i> allow for
the integration of existing pattern-based approaches like <a target="_blank" href="http://php-ids.org">PHP-IDS</a>
or even the <a href="http://www.modsecurity.org/core-rules/">Core Rules</a> provided with the <i>ModSecurity</i>
module. For more details on this integration see <a href="mixed-models.html">mixed models</a>.


<h2>Language Concept</h2>
The basic concept of the <i>Web Profile Language</i> is a hierarchical decomposition of a web application
by means of the applications address space, i.e. URLs of its resources. This natural view of the
application provides an understandable display of resources and natively fits the conceptual view of
web applications as defined by J2EE, PHP and other web environments.
<p>
The language depends on nesting descriptions of your application's resources which results in a
description.

<p>
Having said this, we can artlessly interpret an application as a hierarchical ordered set of
resources which are identified by their URLs. Describing your application by specifying all
resources that shall be accessable lets you check for a first class of common security issues
of web applications: unwanted exposure of sensitive files.

<p>
The following figure 1 shows the hierarchical decomposition of a simple web application that
consists of a small number of resources. Each resource is further specified in more details
by more nested elements describing the request properties valid for this resource.

<div align="center" style="padding-top: 10px; padding-bottom: 20px; color: #0f0f0f; font-size: 10pt;">
  <img src="resourceTree.png" height="240" border="0" />
<br/>
Figure 1: Resource tree of an application.
</div>

The <i>Web Profile Language</i> provides <code>Resource</code> elements for describing a resource and
by nesting resources the above tree structure can easily be described with the profile language. The
resources are further described by nesting additional XML elements within a certain resource's element.
As pointed out in the figure above the resource <code>/intern/account.php</code> can be described
by nesting a <code>Method</code> element and <code>Parameter</code> elements within the resource
description:

<pre>
   ...
     &lt;Resource name="intern"&gt;
         &lt;Resource name="account.php"&gt;

             &lt;Parameter name="id" type="${SessionId}" scope="header" /&gt;

             &lt;Method value="GET"&gt;
                &lt;!--
                    Description of the resource /intern/account.php, 
                    when accessed using a GET request
                --&gt;
             &lt;/Method&gt;

             &lt;Method value="POST"&gt;
                &lt;!--
                    Description of the resource /intern/account.php
                    when accessed with a POST request
                  --&gt;
             &lt;/Method&gt;

         &lt;/Resource&gt;
     &lt;/Resource&gt;
   ...
</pre>

As another feature to ease the creation of such profiles, the language allows for the specification
and use of pre-defined resource classes and parameter types. These can be used in a way similar to
the <i>inheritance</i> of modern object oriented programming languages.
<p>
In the example from above, the parameter type <code>SessionID</code> is a user-defined type that
is referenced by issuing <code>${SessionID}</code> in the <code>type</code> attribute of the
parameter element.
<p>
A more sophisticated example is the definition and use of a <i>resource class</i>. This class
defines several methods and parameters that can be inherited by other resources. Let the
following be the definition of a <i>resource class</i>:

<pre>
    &lt;ResourceClass name="Image"&gt;
       &lt;Method value="GET|HEAD" />
    &lt;/ResourceClass&gt;
</pre>

Then we can easily define image resources by creating a <code>Resource</code> element within our
profile's resource tree, and let it extend the <i>Image</i> class:

<pre>
    &lt;Resource name="images"&gt;
       &lt;Resource name="[a-z0-9]{4,8}\.gif" extends="Image" /&gt;
    &lt;/Resource&gt;
</pre>

As you can probably easily guess from this simple example, this small XML snippet will specify
a set of resources which match the path <code>/images/[a-z0-9]{4,8}\.gif</code> and which inherit
the valid request properties from the <i>Image</i> class.
<p/>
Thus, the images matching the specified path(s) can be accessed using a simple GET or HEAD method.
Similar definitions can also be made for specific headers, authorization or parameters.
<p/>
After this small overview of the language concept, we will go into more details within the next
sections. The next session gives a description of the basic elements of the language. After this,
we describe the concept of inheritance within the language in section <a href="#abstraction">Abstraction</a>.
 
 
<h2>1. Components of a Profile</h2>
An application's profile consists of a set of different resources, each of which represents a component
of the application. The resources are structured according to the herarchical nature of the URL
addressing scheme. Given some basic context path &quot;<code>/</code>&quot;, the application consists
of all resources contained within that base path. 
<p>

<h3>Defining Resources</h3>
Basic to the profile definition is the resource tree which represents the whole hierarchical
structure of an application which is described in the following. In addition to that the profiles
may include some meta-information that is providing the author and the version of a profile. 

Specifying a resource by its URL is thus the first step in describing a part of the application.
This is achieved by splitting the URL into path-elements, for example, the resource 
<code>/images/logo.gif</code> will be specified as follows:

<pre>
    ...
    &lt;Resource name="images"&gt;
        &lt;Resource name="logo.gif"&gt;

        &lt;/Resource&gt;
    &lt;/Resource&gt;
    ...
</pre>

Within the inner <code>Resource</code>-tag the legal properties of requests to that resource will
be described. Since profiles define a positive security-model, the above definition needs a little
bit more to be accessible. In the following we extend the resource-definition to allow GET-requests
to come through:

<pre>
    ...
    &lt;Resource name="images"&gt;
        &lt;Resource name="logo.gif"&gt;
            &lt;Method value="GET" /&gt;
        &lt;/Resource&gt;
    &lt;/Resource&gt;
    ...
</pre>

This will reflects all GET-requests without any parameter to be legal requests to <code>images/logo.gif</code>.
Nearly all attributes (i.e. a resources' <code>name</code>, a methods' <code>value</code>) allow for
the use of regular expressions which enables you to specify more than just one method at a time. For
example to allow HEAD-requests in addition to only the GET-method you can extend the definition above
by using

<pre>
    ...
        &lt;Method value="(GET|HEAD)" /&gt;
    ...
</pre>


<h3>Specifying Valid Parameters</h3>
A lot of the attacks against web applications can be prevented by using a strict typing system on parameters. This
is the first intention of the parameter specifications within the <i>Web Profile Language</i>. The definition of
parameter types mostly relies on standard regular expressions. 
<p/>
The use of regular expressions allows the specification of a lot of simple parameter type as well as an application
specific typing mechanism, e.g. the defintion of a fixed set of legal parameter values as mostly within select
fields.
<p/>
The basic element for specifying a parameter is called <code>Parameter</code>. It has an attribute <code>name</code>
for refering to the parameter's name. This attribute also takes regular expressions for specifying type properties
for a set of parameters. The following example specifies a simple <code>ID</code> parameter which takes an hexadecimal
session ID of exactly 32 digits:

<pre>
     &lt;Parameter name="ID" regexp="[a-fA-F0-9]{32,32}" required="false" scope="header" /&gt;
</pre>

Obviously, the <code>required</code>-attribute specifies that this parameter has to be present within a request. Using
the <code>scope</code> attribute allows you to distinguish between parameters sent within the header, i.e. query-string,
and those transmitted in the request-body. <br/>
The following table lists all attributes of the <code>Parameter</code> element:

<div align="center" style="font-size: 10pt; padding-top: 15px; padding-bottom: 15px;">
<table border="1" width="80%" style="font-size: 10pt;" cellspacing="0">
   <tr>
     <th>Attribute</th><th>Description</th>
   </tr>
   <tr>
     <td><code>name</code></td>
     <td>This attribute specifies the name of the header or cookie that is checked using the regular expression.</td>
   </tr>
   <tr>
     <td><code>regexp</code></td>
     <td>This attribute contains either a regular expression that is used for checking the header or cookie, or
         a reference, starting with a <code>$</code> character, which references a pre-defined regular expression.</td>
   </tr>
   <tr>
     <td><code>required</td>
     <td>A parameter can either be optional or required. A value of <code>true</code> for this attribute
         will require the parameter to be present within the request (besides matching the regular expression).</td>
   </tr>
   <tr>
     <td><code>scope</code></td>
     <td>Parameters can be transmitted either within the <i>query string</i>, i.e. the request header, or the message
         body, e.g. within a POST request. This attribute allows for the user to specify either the <code>header</code>
         or <code>body</code> scope for a request.
     </td>
   </tr>
</table>
</div>

The <code>Parameter</code> element can be nested into several of the elements described so far: <code>Resource</code>, for
defining a parameter type which covers all possible methods, or within the <code>Method</code> element, to restrict the
parameter in dependency of the request-method.
<p/>
Parameters can even be dependent on each other, i.e. a certain parameter is only required to be present in case of another
one. In the following snippet, the parameter <code>productID</code> is mandatory if the parameter <code>action</code> has
the specific value &quot;<code>show</code>&quot:

<pre>
     &lt;Parameter name="action" regexp="show" required="false" scope="header" &gt;
        &lt;Parameter name="productID" regexp="\d{5,10}" required="true" scope="header" /&gt;
     &lt;/Parameter&gt;
</pre>

<h3>Specifying Headers and Cookies</h3>
The element <code>Parameter</code> is very universal and a central part of the profile language. Similar to checking the
types of parameters, it is also possible to use the <code>Header</code> and <code>Cookie</code> elements for type checking
header fields and request cookies.
<p/>
As with the <code>Parameter</code> element, the elements <code>Header</code> and <code>Cookie</code> provide similar
attributes, which are listed within the following table:

<div align="center" class="table">
<table border="1" width="80%" cellspacing="0" align="center">
   <tr>
     <th>Attribute</th><th>Description</th>
   </tr>
   <tr>
     <td><code>name</code></td>
     <td>This attribute specifies the name of the header or cookie that is checked using the regular expression.</td>
   </tr>
   <tr>
     <td><code>regexp</code></td>
     <td>This attribute contains either a regular expression that is used for checking the header or cookie, or
         a reference, starting with a <code>$</code> character, which references a pre-defined regular expression.</td>
   </tr>
   <tr>
     <td><code>required</td>
     <td>A header and a cookie can either be optional or required. A value of <code>true</code> for this attribute
         will require the header or cookie to be present within the request (besides matching the regular expression.</td>
   </tr>
</table>
</div>

Obviously, the <code>Header</code> and <code>Cookie</code> elements do not provide the <code>scope</code> attribute as these
properties are header properties. 






<h2><a name="abstraction"></a>Abstraction</h2>
The concept of abstraction is a very important and powerful property of the <i>Web Profile Language</i>. It lets you define
request- or resource properties in an efficient and re-usable manner by allowing <code>Resource</code> elements to <i>extend</i>
pre-defined resource classes. 
<p/>
This is the same principle that you probably already know from object oriented languages like Java, objective PHP or the like,
which makes it even more attractive to be utilized by users. 


<div align="right" style="color:#404040; font-size:10pt;">
Written by  Christian Bockermann &lt;chris@jwall.org&gt;<br>Any feedback is welcome.
</div>


</div>
</body>
</html>
